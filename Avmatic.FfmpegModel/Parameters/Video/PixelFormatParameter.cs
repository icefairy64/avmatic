﻿namespace Avmatic.FfmpegModel.Parameters.Video;

public class PixelFormatParameter : IParameter
{
    public string Name => "pix_fmt";
    
    public required PixelFormat Format { get; set; }

    public string Value => Format.ToParameterValue();
}

public enum PixelFormat
{
    Yuv420,
    Yuv420P10Le
}

public static class PixelFormatExtensions
{
    public static string ToParameterValue(this PixelFormat format)
    {
        return format switch
        {
            PixelFormat.Yuv420 => "yuv420",
            PixelFormat.Yuv420P10Le => "yuv420p10le",
            _ => throw new ArgumentOutOfRangeException(nameof(format), format, null)
        };
    }
}