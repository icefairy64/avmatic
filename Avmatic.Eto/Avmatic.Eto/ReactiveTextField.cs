﻿using System.Reactive.Disposables;
using Avmatic.Model;
using Eto.Forms;

namespace Avmatic.Eto;

public sealed class ReactiveTextField : Panel
{
    public ReactiveFieldModel<string> ViewModel { get; }

    private readonly CompositeDisposable CompositeDisposable = new();

    public ReactiveTextField(ReactiveFieldModel<string> viewModel)
    {
        ViewModel = viewModel;
        Content = FieldUtil.CreateTextField(vm => vm.Name, viewModel, vm => vm.Value, CompositeDisposable);
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            CompositeDisposable.Dispose();
        }

        base.Dispose(disposing);
    }
}