﻿using System;

namespace Avmatic.Eto;

public class ApplicationConfigurator : IApplicationConfirurator
{
    private readonly Action<Application> ConfigurationAction;

    public ApplicationConfigurator(Action<Application> configurationAction)
    {
        ConfigurationAction = configurationAction;
    }

    public void ConfigureApplication(Application application)
    {
        ConfigurationAction(application);
    }
}