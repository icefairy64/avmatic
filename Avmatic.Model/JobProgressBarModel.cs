﻿using Avmatic.Jobs;
using ReactiveUI;

namespace Avmatic.Model;

public class JobProgressBarModel : ReactiveObject
{
    private Job FJob;
    public Job Job
    {
        get => FJob;
        set => this.RaiseAndSetIfChanged(ref FJob, value);
    }

    private float FProgress;
    public float Progress
    {
        get => FProgress;
        set => this.RaiseAndSetIfChanged(ref FProgress, value);
    }

    private JobState FState = JobState.Queued;
    public JobState State
    {
        get => FState;
        set => this.RaiseAndSetIfChanged(ref FState, value);
    }

    private string? FStage;
    public string? Stage
    {
        get => FStage;
        set => this.RaiseAndSetIfChanged(ref FStage, value);
    }
    
    public string? Filename => Job is IFileJob fileJob ? fileJob.OutputFile.Name : null;

    public JobProgressBarModel(Job job)
    {
        FJob = job;
    }
}