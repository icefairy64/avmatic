﻿namespace Avmatic.NodeModel;

public class EnumeratorNode<T> : Node, ITickableNode
{
    private readonly IEnumerator<T> Enumerator;
    
    public SimpleNodeSocket<T> Output { get; }

    public EnumeratorNode(IEnumerable<T> source)
    {
        Enumerator = source.GetEnumerator();
        Output = new SimpleNodeSocket<T> { Owner = this, Name = "Output" };
        FOutputs.Add(Output);
    }

    public bool Tick()
    {
        var result = Enumerator.MoveNext();
        Output.Value = Enumerator.Current;
        return result;
    }
}