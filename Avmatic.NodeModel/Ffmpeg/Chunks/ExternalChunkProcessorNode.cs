﻿using System.Diagnostics;

namespace Avmatic.NodeModel.Ffmpeg.Chunks;

[Node("External chunk processor", "FFmpeg")]
public class ExternalChunkProcessorNode : AbstractChunkProcessorNode
{
    private string FExecutable = "";

    [NodeParameter]
    public string Executable
    {
        get => FExecutable;
        set => SetAndUpdateIfChanged(ref FExecutable, value);
    }
    
    private string FArgumentsPattern = "$in $out";

    [NodeParameter]
    public string ArgumentsPattern
    {
        get => FArgumentsPattern;
        set => SetAndUpdateIfChanged(ref FArgumentsPattern, value);
    }

    protected override Task Process(DirectoryInfo sourceDirectory, DirectoryInfo targetDirectory, CancellationToken cancellationToken)
    {
        var startInfo = new ProcessStartInfo
        {
            FileName = Executable,
            CreateNoWindow = true,
            UseShellExecute = false,
            WorkingDirectory = new FileInfo(Executable).DirectoryName ?? ""
        };

        var args = ArgumentsPattern
            .Split(' ')
            .Select(arg => arg switch
            {
                "$in" => sourceDirectory.FullName,
                "$out" => targetDirectory.FullName,
                _ => arg
            });
        
        foreach (var arg in args)
            startInfo.ArgumentList.Add(arg);
        
        var process = new Process
        {
            StartInfo = startInfo
        };
        
        var processExited = false;
        var taskCompletionSource = new TaskCompletionSource();

        var ctRegistration = cancellationToken.Register(() =>
        {
            taskCompletionSource.TrySetCanceled();
            if (processExited)
                return;
            process.Kill(true);
            processExited = true;
        });
        
        process.EnableRaisingEvents = true;
        process.Exited += (_, _) =>
        {
            ctRegistration.Unregister();
            processExited = true;
            if (process.ExitCode != 0)
                taskCompletionSource.TrySetException(new Exception($"Process exited with code {process.ExitCode}"));
            else
                taskCompletionSource.TrySetResult();
        };
        
        process.Start();

        return taskCompletionSource.Task;
    }
}