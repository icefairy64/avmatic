﻿using System.Text;

namespace Avmatic.FfmpegModel.Parameters.Video.Aom;

public class AomExtraParameter : IParameter
{
    public string Name => "aom-params";
    
    public int? EnableDnlDenoising { get; set; }
    public string? Tune { get; set; }

    public string Value
    {
        get
        {
            var paramList = new List<string>();

            if (EnableDnlDenoising is { } enableDnlDenoising)
                paramList.Add($"enable-dnl-denoising={enableDnlDenoising}");
            
            if (Tune is { } tune)
                paramList.Add($"tune={tune}");

            return string.Join(':', paramList);
        }
    }
}