﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reactive;
using System.Windows.Input;
using Avmatic.NodeModel;
using Avmatic.NodeModel.Serialization;
using DynamicData;
using Microsoft.Extensions.Logging;
using ReactiveUI;

namespace Avmatic.Model.Nodes;

public class NodeTabModel : ReactiveObject
{
    public Interaction<Unit, IEnumerable<FileInfo>?> AddFilesInteraction { get; } = new();
    public Interaction<Unit, DirectoryInfo?> SelectOutputDirectoryInteraction { get; } = new();
    
    public ObservableCollection<FileInfo> SourceFiles { get; } = new();

    public ObservableCollection<ExternalValueFieldModel> ExternalValues { get; } = new();

    private DirectoryInfo? FOutputDirectory;
    public DirectoryInfo? OutputDirectory
    {
        get => FOutputDirectory;
        set => this.RaiseAndSetIfChanged(ref FOutputDirectory, value);
    }
    
    private int FJobCapacity = 8;
    public int JobCapacity
    {
        get => FJobCapacity;
        set => this.RaiseAndSetIfChanged(ref FJobCapacity, value);
    }
    
    private NodeGraph FGraph;

    public NodeGraph Graph
    {
        get => FGraph;
        set => this.RaiseAndSetIfChanged(ref FGraph, value);
    }
    
    public ICommand AddFilesCommand { get; }
    public ICommand SelectOutputDirectoryCommand { get; }
    public ICommand StartCommand { get; }

    private readonly ProgressTabModel ProgressTabModel;
    private readonly ILoggerFactory LoggerFactory;
    
    public NodeTabModel(ProgressTabModel progressTabModel, ILoggerFactory loggerFactory) : this(progressTabModel, new NodeGraph(), loggerFactory)
    {
        
    }

    public NodeTabModel(ProgressTabModel progressTabModel, NodeGraph graph, ILoggerFactory loggerFactory)
    {
        ProgressTabModel = progressTabModel;
        FGraph = graph;
        LoggerFactory = loggerFactory;

        AddFilesCommand = ReactiveCommand.Create(() =>
        {
            AddFilesInteraction.Handle(Unit.Default)
                .Subscribe(files =>
                {
                    if (files != null)
                        SourceFiles.AddRange(files);
                });
        });

        SelectOutputDirectoryCommand = ReactiveCommand.Create(() =>
        {
            SelectOutputDirectoryInteraction.Handle(Unit.Default)
                .Subscribe(path => OutputDirectory = path);
        });

        var startAvailability = this.WhenAnyValue(vm => vm.SourceFiles.Count, vm => vm.OutputDirectory,
                (inputFileCount, outputDir) => inputFileCount is > 0 && outputDir is not null);

        StartCommand = ReactiveCommand.Create(Start, startAvailability);
        
        foreach (var node in graph.Nodes.OfType<ExternalValueNode>())
            BindExternalValue(node);

        this.WhenAnyValue(vm => vm.Graph)
            .Subscribe(OnGraphChanged);
        
        graph.NodeAdded += OnNodeAdded;
        graph.NodeRemoved += OnNodeRemoved;
    }

    private void OnGraphChanged(NodeGraph graph)
    {
        ExternalValues.Clear();
        foreach (var node in graph.Nodes)
            OnNodeAdded(graph, new NodeGraphNodeEventArgs { Node = node });
    }

    private void BindExternalValue(ExternalValueNode node)
    {
        var valueProp = node.GetType().GetProperty("Value");
        var nameProp = node.GetType().GetProperty("Name");
        var field = new ExternalValueFieldModel(node);
        ExternalValues.Add(field);
        node.PropertyChanged += OnExternalValueNodePropertyChanged;
    }

    private void UnbindExternalValue(ExternalValueNode node)
    {
        var field = ExternalValues.First(f => f.Node == node);
        ExternalValues.Remove(field);
        node.PropertyChanged -= OnExternalValueNodePropertyChanged;
    }

    private void OnExternalValueNodePropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName != "Name")
            return;
        var field = ExternalValues.First(f => f.Node == sender);
        field.Name = field.Node.Name;
    }
    
    private void OnNodeAdded(object? sender, NodeGraphNodeEventArgs e)
    {
        if (e.Node is ExternalValueNode externalValueNode)
            BindExternalValue(externalValueNode);
    }
    
    private void OnNodeRemoved(object? sender, NodeGraphNodeEventArgs e)
    {
        if (e.Node is ExternalValueNode externalValueNode)
            UnbindExternalValue(externalValueNode);
    }

    private void Start()
    {
        if (OutputDirectory is not {} outputDirectory)
            return;

        var matrix = Zip(ExternalValues.Select(field => new KeyValuePair<string, string>(field.Name, field.Value)).ToList(),
            new List<IReadOnlyDictionary<string, string>> { new Dictionary<string, string>() })
            .ToList();
        
        var jobs = SourceFiles.SelectMany(file => matrix.Select(p => (File: file, Params: p)))
            .Select(tuple => new NodeGraphRunJob(CloneGraph(Graph, tuple.Params),
            tuple.File,
            new FileInfo(Path.Combine(outputDirectory.FullName,
                GetOutputFileName(tuple.File, tuple.Params))),
            LoggerFactory.CreateLogger<NodeGraphRunJob>()));
        ProgressTabModel.Start(jobs, JobCapacity);
    }

    private static NodeGraph CloneGraph(NodeGraph graph, IReadOnlyDictionary<string, string> externalValues)
    {
        var clone = NodeGraphDto.FromGraph(graph).Materialize();
        foreach (var (key, value) in externalValues)
        {
            var node = clone.Nodes.OfType<ExternalValueNode>().First(n => n.Name == key);
            node.Value = value;
        }
        return clone;
    }
    
    private static IEnumerable<IReadOnlyDictionary<string, string>> Zip(IReadOnlyList<KeyValuePair<string, string>> separatedProps, IEnumerable<IReadOnlyDictionary<string, string>> matrix)
    {
        if (separatedProps.Count == 0)
            return matrix;
        
        var firstPair = separatedProps[0];
        var restProps = separatedProps.Skip(1).ToList();
        var result = firstPair.Value.Split(',')
            .Select(x => x.Trim())
            .SelectMany(v => Multiply(firstPair.Key, v, matrix));
        
        return restProps.Count > 0 ? Zip(restProps, result) : result;
    }

    private static IEnumerable<IReadOnlyDictionary<string, string>> Multiply(
        string key,
        string value,
        IEnumerable<IReadOnlyDictionary<string, string>> matrix)
    {
        return matrix.Select(p => new Dictionary<string, string>(p) { [key] = value });
    }

    private static string GetOutputFileName(FileInfo inputFile, IReadOnlyDictionary<string, string> externalValues)
    {
        if (!externalValues.Any())
            return inputFile.Name;
        return Path.ChangeExtension(inputFile.Name, null) + "-" +
               string.Join('-', externalValues.Select(p => $"{p.Key}{p.Value.Replace(':', '-')}")) + inputFile.Extension;
    }
}