﻿using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.NodeModel.Ffmpeg;

[Node("Copy stream", "FFmpeg")]
public class FfmpegCopyStreamNode : FfmpegAbstractStreamNode
{
    protected override FfmpegStreamInfo? GetOutputStream()
    {
        return InputStream.Value switch
        {
            FfmpegStreamInfo info => info with
            {
                Parameters = info.Parameters.Append(new GenericParameter { Name = "c", Value = "copy" }).ToList()
            },
            null => null,
            _ => throw new InvalidOperationException("Unsupported type")
        };
    }
}