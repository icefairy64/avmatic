﻿namespace Avmatic.NodeModel.Ffmpeg.Chunks;

public class ChunkProcessingContext
{
    public required DirectoryInfo SourceDirectory { get; init; }
    public required ChunkPipelineContext Pipeline { get; init; }
    
    public Task<FileInfo>? Task => CompletionSource?.Task;

    private TaskCompletionSource<FileInfo>? CompletionSource;

    public EventHandler<ChunkStartedEventArgs>? ChunkStarted;

    public Task<FileInfo> BeginChunk(CancellationToken token)
    {
        CompletionSource = new TaskCompletionSource<FileInfo>();
        token.Register(() => CompletionSource.TrySetCanceled());
        ChunkStarted?.Invoke(this, new ChunkStartedEventArgs(token));
        return CompletionSource.Task;
    }

    public void EndChunk(FileInfo result)
    {
        CompletionSource?.SetResult(result);
        CompletionSource = null;
    }
}

public record ChunkStartedEventArgs(CancellationToken CancellationToken);