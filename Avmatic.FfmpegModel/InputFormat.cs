﻿namespace Avmatic.FfmpegModel;

public class InputFormat
{
    public required string FormatName { get; set; }
    public required TimeSpan? Duration { get; set; }

    public override string ToString()
    {
        return $"{nameof(FormatName)}: {FormatName}, {nameof(Duration)}: {Duration}";
    }
}