﻿using System;
using System.Linq;
using System.Reactive.Disposables;
using Avmatic.Model;
using Eto.Forms;
using ReactiveUI;

namespace Avmatic.Eto;

public sealed class StreamTranscodePanel : Panel
{
    public StreamTranscodePanelModel ViewModel { get; }
    private readonly CompositeDisposable CompositeDisposable = new();

    public StreamTranscodePanel(StreamTranscodePanelModel viewModel)
    {
        ViewModel = viewModel;

        var stack = new StackLayout
        {
            Spacing = 8,
            Items =
            {
                FieldUtil.CreateSelectorField("Target encoder", ViewModel, vm => vm.SelectedTarget, CompositeDisposable, ViewModel.AvailableTargets),
                new Label { Text = "No target selected" }
            }
        };
        
        var box = new GroupBox
        {
            Text = $"Stream {ViewModel.Stream.Index}",
            Padding = 8,
            Content = stack
        };

        Content = box;

        ViewModel.WhenAnyValue(vm => vm.StreamConfiguration)
            .Subscribe(c => 
            {
                var prevContent = stack.Items.Last()?.Control ?? throw new Exception();
                Control item = c == null ? new Label { Text = "No configuration available" } : new StreamConfigurationPanel(c);
                stack.Items.Remove(stack.Items.Last());
                stack.Items.Add(item);
                prevContent.Dispose();
            })
            .DisposeWith(CompositeDisposable);
    }
}