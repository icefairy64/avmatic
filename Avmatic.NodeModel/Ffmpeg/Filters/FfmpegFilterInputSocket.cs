﻿namespace Avmatic.NodeModel.Ffmpeg.Filters;

public class FfmpegFilterInputSocket : NodeSocket
{
    public IFfmpegFilterInputInfo? TypedValue => Value as IFfmpegFilterInputInfo;
    
    public FfmpegFilterInputSocket()
    {
        Type = typeof(IFfmpegFilterInputInfo);
    }

    public override bool MatchType(Type type)
    {
        return typeof(FfmpegFilterOutputInfo).IsAssignableFrom(type) || typeof(FfmpegStreamInfo).IsAssignableFrom(type);
    }

    protected override void OnValueChanged(NodeSocketValueChangedArgs args)
    {
        switch (args.Value)
        {
            case IFfmpegFilterInputInfo filterInputInfo:
                base.OnValueChanged(args);
                break;
            case FfmpegFilterOutputInfo filterOut:
                Value = new FfmpegFilterChainInputInfo(filterOut);
                break;
            case FfmpegStreamInfo stream:
                Value = new FfmpegFilterStreamInputInfo(stream);
                break;
            default:
                base.OnValueChanged(new NodeSocketValueChangedArgs { Value = null });
                break;
        }
    }
}

public class FfmpegFilterInputSocketBuilder : NodeSocketBuilder<FfmpegFilterInputSocket>
{
    public override FfmpegFilterInputSocket Build()
    {
        if (Owner is not { } owner)
            throw new Exception("Owner is not specified");
        if (Name is not { } name)
            throw new Exception("Name is not specified");
        return new FfmpegFilterInputSocket { Owner = owner, Name = name };
    }
}