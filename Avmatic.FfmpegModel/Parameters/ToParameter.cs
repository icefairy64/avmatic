﻿namespace Avmatic.FfmpegModel.Parameters;

public class ToParameter : IParameter
{
    public string Name => "to";
    
    public required string Duration { get; init; }

    public string Value => Duration;
}