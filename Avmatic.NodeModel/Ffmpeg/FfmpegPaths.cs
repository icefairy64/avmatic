﻿namespace Avmatic.NodeModel.Ffmpeg;

public static class FfmpegPaths
{
    public static FileInfo? FfmpegPath { get; set; }
    public static FileInfo? FfprobePath { get; set; }
}