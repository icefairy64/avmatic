﻿namespace Avmatic.FfmpegModel.Parameters.Video.Svt;

public class SvtExtraParameter : IParameter
{
    public string Name => "svtav1-params";
    
    public int? FilmGrain { get; set; }
    public int? FilmGrainDenoise { get; set; }
    public int? Tune { get; set; }

    public string Value
    {
        get
        {
            var paramList = new List<string>();
            
            if (FilmGrain is {} filmGrain)
                paramList.Add($"film-grain={filmGrain}");
            
            if (FilmGrainDenoise is {} filmGrainDenoise)
                paramList.Add($"film-grain-denoise={filmGrainDenoise}");
            
            if (Tune is {} tune)
                paramList.Add($"tune={tune}");

            return string.Join(':', paramList);
        }
    }
}