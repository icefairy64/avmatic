﻿namespace Avmatic.NodeModel.Serialization;

public record NodeGraphDto(IList<NodeDto> Nodes, IList<NodeConnectionDto> Connections)
{
    public NodeGraph Materialize()
    {
        var graph = new NodeGraph();

        foreach (var nodeDto in Nodes)
            graph.AddNode(nodeDto.Materialize());

        foreach (var connectionDto in Connections)
        {
            var sourceSocket = graph.Nodes[connectionDto.SourceNodeIndex].Outputs
                .First(s => s.Name == connectionDto.SourceSocket);
            var targetSocket = graph.Nodes[connectionDto.TargetNodeIndex].Inputs
                .First(s => s.Name == connectionDto.TargetSocket);
            graph.Connect(sourceSocket, targetSocket);
        }

        return graph;
    }

    public static NodeGraphDto FromGraph(NodeGraph graph)
    {
        var nodeDtos = graph.Nodes.Select(NodeDto.FromNode);
        var nodes = graph.Nodes.ToList();
        var connectionDtos = graph.Connections.Select(c =>
        {
            var sourceNodeIndex = nodes.IndexOf(c.Source.Owner);
            var targetNodeIndex = nodes.IndexOf(c.Target.Owner);
            return new NodeConnectionDto(sourceNodeIndex, c.Source.Name, targetNodeIndex, c.Target.Name);
        });

        return new NodeGraphDto(nodeDtos.ToList(), connectionDtos.ToList());
    }
}