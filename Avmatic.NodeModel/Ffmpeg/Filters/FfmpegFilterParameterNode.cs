﻿using System.Reflection;
using System.Text;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.NodeModel.Ffmpeg.Filters;

[Node("Filter to parameters", "FFmpeg/Filters")]
public class FfmpegFilterParameterNode : Node
{
    public FfmpegFilterGraphInputSocket Input { get; }
    public SimpleNodeSocket<IReadOnlyList<IParameter>> Parameter { get; }

    public FfmpegFilterParameterNode()
    {
        Input = new FfmpegFilterGraphInputSocket { Owner = this, Name = "Filter graph" };
        Parameter = new SimpleNodeSocket<IReadOnlyList<IParameter>> { Owner = this, Name = "Parameters" };
        
        Input.ValueChanged += OnInputValueChanged;
        
        FInputs.Add(Input);
        FOutputs.Add(Parameter);
    }

    private void OnInputValueChanged(object? sender, NodeSocketValueChangedArgs e)
    {
        if (Input.Graph is not { } graph)
        {
            Parameter.Value = null;
            return;
        }

        var atoms =
            new List<(IReadOnlyList<IFfmpegFilterInputInfo> Inputs, FfmpegFilterInfo Filter, List<FfmpegFilterOutputInfo> Outputs)>();
        
        foreach (var outputInfo in graph.Outputs)
            Fill(outputInfo, atoms);

        var pads = new Dictionary<object, string>();
        var subgraphs = new List<string>();

        var nodeCounter = 0;
        
        foreach (var (inputs, filter, outputs) in atoms)
        {
            nodeCounter++;
            var inputCounter = 0;
            var outputCounter = 0;
            var graphDescriptor = new StringBuilder();
            
            // Input pads
            
            foreach (var input in inputs)
            {
                inputCounter++;
                if (!pads.TryGetValue(input, out var inputName))
                {
                    inputName = input switch
                    {
                        FfmpegFilterStreamInputInfo streamInputInfo => streamInputInfo.StreamInfo.StreamSelector,
                        FfmpegFilterChainInputInfo chainInputInfo => chainInputInfo.SourceInfo.PadStreamSpecifier.PadName,
                        _ => $"in_${nodeCounter}_{inputCounter}"
                    };
                    pads.Add(input, inputName);
                }
                graphDescriptor.Append('[');
                graphDescriptor.Append(inputName);
                graphDescriptor.Append(']');
            }

            // Description
            
            graphDescriptor.Append(filter.Name);
            var paramString = string.Join(':', filter.Parameters.Select(p => $"{p.Key}={p.Value}"));
            if (paramString.Length > 0)
            {
                graphDescriptor.Append('=');
                graphDescriptor.Append(paramString);
            }
            
            // Output pads

            foreach (var output in outputs.OrderBy(o => o.Index))
            {
                outputCounter++;
                if (!pads.TryGetValue(output, out var outputName))
                {
                    outputName = output.PadStreamSpecifier.PadName;
                    pads.Add(output, outputName);
                }
                graphDescriptor.Append('[');
                graphDescriptor.Append(outputName);
                graphDescriptor.Append(']');
            }
            
            subgraphs.Add(graphDescriptor.ToString());
        }

        var parameters = new List<IParameter>
        {
            new GenericParameter { Name = "filter_complex", Value = string.Join(';', subgraphs) }
        };

        Parameter.Value = parameters;
    }

    private static void Fill(
        FfmpegFilterOutputInfo output,
        ICollection<(IReadOnlyList<IFfmpegFilterInputInfo> Inputs, FfmpegFilterInfo Filter, List<FfmpegFilterOutputInfo> Outputs)> entries)
    {
        var filter = output.Filter;
        var isNew = false;

        (IReadOnlyList<IFfmpegFilterInputInfo> Inputs, FfmpegFilterInfo Filter, List<FfmpegFilterOutputInfo> Outputs) entry;
        
        if (entries.All(x => x.Filter != filter))
        {
            isNew = true;
            entry = (Inputs: filter.Inputs, Filter: filter,
                Outputs: new List<FfmpegFilterOutputInfo>());
            entries.Add(entry);
        }
        else
        {
            entry = entries.First(x => x.Filter == filter);
        }
        
        if (!entry.Outputs.Contains(output))
            entry.Outputs.Add(output);

        if (isNew)
        {
            foreach (var chainInputInfo in filter.Inputs.OfType<FfmpegFilterChainInputInfo>())
                Fill(chainInputInfo.SourceInfo, entries);
        }
    }
}

public class FfmpegFilterGraphInputSocket : NodeSocket
{
    public FfmpegFilterMultipartGraphInfo? Graph => Value as FfmpegFilterMultipartGraphInfo;
    
    public FfmpegFilterGraphInputSocket()
    {
        Type = typeof(FfmpegFilterMultipartGraphInfo);
    }

    public override bool MatchType(Type type)
    {
        return typeof(FfmpegFilterMultipartGraphInfo).IsAssignableFrom(type) ||
               typeof(FfmpegFilterOutputInfo).IsAssignableFrom(type);
    }

    protected override void OnValueChanged(NodeSocketValueChangedArgs args)
    {
        switch (args.Value)
        {
            case FfmpegFilterMultipartGraphInfo:
                base.OnValueChanged(args);
                break;
            case FfmpegFilterOutputInfo outputInfo:
                Value = new FfmpegFilterMultipartGraphInfo(new List<FfmpegFilterOutputInfo> { outputInfo });
                break;
            default:
                Value = null;
                break;
        }
    }
}