﻿namespace Avmatic.FfmpegModel.Tests;

public static class EnumerableExtensions
{
    public static string Joined(this IEnumerable<string> enumerable, string separator) =>
        string.Join(separator, enumerable);
}