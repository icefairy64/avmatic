﻿namespace Avmatic.NodeModel;

public static class NodeImplementations
{
    private static List<Type>? ImplementationTypes;

    public static IEnumerable<Type> GetImplementations()
    {
        if (ImplementationTypes is null)
        {
            ImplementationTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(a =>
                {
                    try
                    {
                        return a.ExportedTypes;
                    }
                    catch (Exception)
                    {
                        return new List<Type>();
                    }
                })
                .Where(t => !t.IsAbstract && typeof(Node).IsAssignableFrom(t))
                .ToList();
        }

        return ImplementationTypes;
    }

    public static Type GetByName(string name)
    {
        return GetImplementations()
                   .FirstOrDefault(t => t.Name == name)
               ?? throw new ArgumentException($"Unknown node type {name}");
    }
}