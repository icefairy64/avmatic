﻿using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.Model;

public interface IScaleDescription
{
    public IParameter Parameter { get; }
}