﻿namespace Avmatic.FfmpegRunner;

public class PropertyOutputHandler : IOutputHandler
{
    public event EventHandler<PropertyEventArgs>? PropertyEmit;
    public Dictionary<string, string> Properties { get; } = new();

    public void HandleLine(string line)
    {
        var splitIndex = line.IndexOf('=');
        if (splitIndex < 0)
            return;
        OnPropertyEmit(new PropertyEventArgs
        {
            Key = line.Substring(0, splitIndex),
            Value = line.Substring(splitIndex + 1)
        });
    }

    protected virtual void OnPropertyEmit(PropertyEventArgs e)
    {
        Properties[e.Key] = e.Value;
        PropertyEmit?.Invoke(this, e);
    }
}

public class PropertyEventArgs : EventArgs
{
    public required string Key { get; set; }
    public required string Value { get; set; }
}