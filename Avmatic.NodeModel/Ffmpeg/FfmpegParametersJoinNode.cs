﻿using System.Collections;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.NodeModel.Ffmpeg;

[Node("Join parameters", "FFmpeg")]
public class FfmpegParametersJoinNode : Node
{
    public SimpleNodeSocket<IReadOnlyList<IParameter>> Output { get; }

    private bool FOptional;

    [NodeParameter]
    public bool Optional
    {
        get => FOptional;
        set
        {
            if (FOptional == value)
                return;
            FOptional = value;
            Update();
        }
    }

    private int FInputCount = 2;

    [NodeParameter]
    public int InputCount
    {
        get => FInputCount;
        set
        {
            if (FInputCount == value)
                return;
            var prevCount = FInputCount;
            FInputCount = value;
            UpdateInputs(value, prevCount);
        }
    }

    public FfmpegParametersJoinNode()
    {
        AddInput();
        AddInput();
        Output = new SimpleNodeSocket<IReadOnlyList<IParameter>> { Owner = this, Name = "Output" };
        FOutputs.Add(Output);
    }

    private void AddInput()
    {
        var input = new ResultNodeSocket<IReadOnlyList<IParameter>> { Owner = this, Name = $"Input {(char)('A' + Inputs.Count)}" };
        input.ValueChanged += (_, _) => Update();
        FInputs.Add(input);
    }
    
    private void PopInput()
    {
        var input = Inputs[^1];
        FInputs.Remove(input);
        input.Dispose();
    }
    
    private void UpdateInputs(int count, int prevCount)
    {
        var diff = count - prevCount;
        switch (diff)
        {
            case 0:
                return;
            case > 0:
                for (var i = 0; i < diff; i++)
                    AddInput();
                break;
            case < 0:
                for (var i = 0; i < -diff; i++)
                    PopInput();
                break;
        }
        Update();
    }

    private void Update()
    {
        if ((Optional && Inputs.All(i => i.Value is null)) || (!Optional && Inputs.Any(i => i.Value is null)))
        {
            Output.Value = null;
            return;
        }

        var list = Inputs.SelectMany(i =>
        {
            if (i.Value is IEnumerable<IParameter> enumerable)
                return enumerable;
            else
                return new List<IParameter>();
        }).ToList();

        Output.Value = list;
    }
}