﻿using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reflection;
using Avmatic.FfmpegModel;
using DynamicData;
using ReactiveUI;

namespace Avmatic.Model;

public class StreamMatrixPanelModel : ReactiveObject
{
    public required InputStream Stream { get; init; }

    private string FSelectedTarget = "none";
    public string SelectedTarget
    {
        get => FSelectedTarget;
        set => this.RaiseAndSetIfChanged(ref FSelectedTarget, value);
    }

    private readonly ObservableAsPropertyHelper<Type?> FSelectedConfigurationType;
    public Type? SelectedConfigurationType => FSelectedConfigurationType.Value;

    private SourceList<ReactiveFieldModel<string>> FConfigurationFields { get; } = new();
    public IObservableList<ReactiveFieldModel<string>> ConfigurationFields => FConfigurationFields.AsObservableList();
    
    public IEnumerable<(string, IStreamConfiguration)> StreamConfigurations =>
        Zip(ConfigurationFields.Items.Select(item => new KeyValuePair<string, string>(item.BackingMemberName ?? item.Name, item.Value)).ToList(),
                new List<IReadOnlyDictionary<string, string>> { new Dictionary<string, string>() })
            .Select(CreateStreamConfiguration);

    public string[] AvailableTargets => StreamConfigurationImplementations.Values
        .Select(t => t.Attribute)
        .OfType<StreamConfigurationAttribute>()
        .Where(a => a.StreamType is null || a.StreamType == Stream.CodecType.ToStreamType())
        .Select(a => a.Name)
        .Prepend("none")
        .ToArray();
    
    private static readonly Dictionary<string, (Type Type, StreamConfigurationAttribute? Attribute)> StreamConfigurationImplementations;

    static StreamMatrixPanelModel()
    {
        StreamConfigurationImplementations = IStreamConfiguration.GetImplementations()
            .Select(t => (Type: t, Attribute: t.GetCustomAttribute(typeof(StreamConfigurationAttribute)) as StreamConfigurationAttribute))
            .Where(t => t.Attribute is not null)
            .ToDictionary(x => x.Attribute?.Name ?? throw new Exception("No attribute"));
    }
    
    public StreamMatrixPanelModel()
    {
        this.WhenAnyValue(vm => vm.SelectedTarget)
            .Select(t => t switch
            {
                "none" => null,
                _ => StreamConfigurationImplementations[t].Type
            })
            .ToProperty(this, vm => vm.SelectedConfigurationType, out FSelectedConfigurationType);

        this.WhenAnyValue(vm => vm.SelectedConfigurationType)
            .Subscribe(ReplaceFields);
    }

    private static void SetPropertyValue(IStreamConfiguration streamConfiguration, PropertyInfo property, string value)
    {
        object convertedValue = value;
        if (property.PropertyType.IsAssignableFrom(typeof(double)))
            convertedValue = double.Parse(value);
        else if (property.PropertyType.IsAssignableFrom(typeof(int)))
            convertedValue = int.Parse(value);
        else if (property.PropertyType.IsAssignableFrom(typeof(bool)))
            convertedValue = bool.Parse(value);
        property.SetValue(streamConfiguration, convertedValue);
    }

    private (string, IStreamConfiguration) CreateStreamConfiguration(IReadOnlyDictionary<string, string> props)
    {
        var config = Activator.CreateInstance(SelectedConfigurationType) as IStreamConfiguration ?? throw new InvalidOperationException();
        config.InputStream = Stream;
        config.StreamIndex = Stream.Index;
        config.CodecType = Stream.CodecType;
        
        foreach (var pair in props)
        {
            var prop = SelectedConfigurationType.GetProperty(pair.Key) ?? throw new InvalidOperationException();
            SetPropertyValue(config, prop, pair.Value);
        }

        var key = string.Join('-', props.Select(p => $"{p.Key}{p.Value}"));

        return (key, config);
    }

    private void ReplaceFields(Type? type)
    {
        FConfigurationFields.Clear();

        if (type is null)
            return;

        var fields = type.GetProperties()
            .Select(p => (prop: p, attr: p.GetCustomAttribute(typeof(StreamConfigurationParameterAttribute))))
            .Select(t => t switch
            {
                (var prop, StreamConfigurationParameterAttribute attr) => new ReactiveFieldModel<string>(
                    attr.FieldName ?? prop.Name, prop.Name, attr.DefaultValue?.ToString() ?? ""),
                _ => null
            })
            .OfType<ReactiveFieldModel<string>>();
        
        FConfigurationFields.AddRange(fields);
    }

    private static IEnumerable<IReadOnlyDictionary<string, string>> Zip(IReadOnlyList<KeyValuePair<string, string>> separatedProps, IEnumerable<IReadOnlyDictionary<string, string>> matrix)
    {
        var firstPair = separatedProps[0];
        var restProps = separatedProps.Skip(1).ToList();
        var result = firstPair.Value.Split(',')
            .Select(x => x.Trim())
            .SelectMany(v => Multiply(firstPair.Key, v, matrix));
        
        return restProps.Count > 0 ? Zip(restProps, result) : result;
    }

    private static IEnumerable<IReadOnlyDictionary<string, string>> Multiply(
        string key,
        string value,
        IEnumerable<IReadOnlyDictionary<string, string>> matrix)
    {
        return matrix.Select(p => new Dictionary<string, string>(p) { [key] = value });
    }
}