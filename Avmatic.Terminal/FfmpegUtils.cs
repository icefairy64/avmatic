﻿namespace Avmatic.Terminal;

internal class FfmpegUtils
{
    public static FileInfo? GetFfToolPath(string tool)
    {
        if (Environment.GetEnvironmentVariable("FFMPEG_PATH") is { } path)
            return new FileInfo(Path.Combine(path, tool));
        return null;
    }
}