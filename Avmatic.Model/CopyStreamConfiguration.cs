﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.Model;

[StreamConfiguration("copy")]
public class CopyStreamConfiguration : IStreamConfiguration
{
    public required InputStream InputStream { get; set; }
    public int StreamIndex { get; set; }
    public required string CodecType { get; set; }
    public IList<IParameter> Parameters => new List<IParameter> { new GenericParameter { Name = "c", Value = "copy" } };
    
    public IStreamConfiguration Clone()
    {
        return new CopyStreamConfiguration
        {
            InputStream = InputStream,
            StreamIndex = StreamIndex,
            CodecType = CodecType
        };
    }
}