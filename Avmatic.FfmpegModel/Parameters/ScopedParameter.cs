﻿namespace Avmatic.FfmpegModel.Parameters;

public class ScopedParameter : IParameter
{
    public required IStreamSpecifier StreamSpecifier { get; set; }
    public required IParameter Parameter { get; set; }

    public string Name => $"{Parameter.Name}:{StreamSpecifier.String}";
    public string Value => Parameter.Value;
}