﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.NodeModel.Ffmpeg.Chunks;

[Node("FFmpeg chunk source", "FFmpeg")]
public sealed class FfmpegChunkSourceNode : SimpleNode, IDisposable
{
    public ResultNodeSocket<FileInfo> File { get; }
    public ResultNodeSocket<InputFile> FileInfo { get; }
    public ResultNodeSocket<IReadOnlyList<IParameter>> StreamParameters { get; }
    
    public SimpleNodeSocket<ChunkProcessingContext> Context { get; }
    public SimpleNodeSocket<IReadOnlyList<IParameter>> OutFileParameters { get; }
    public SimpleNodeSocket<FfmpegStreamInfo> StreamInfo { get; }

    private int FChunkLength = 5;

    [NodeParameter]
    public int ChunkLength
    {
        get => FChunkLength;
        set => SetAndUpdateIfChanged(ref FChunkLength, value);
    }

    private string FFramePattern = "f%06d.png";

    [NodeParameter]
    public string FramePattern
    {
        get => FFramePattern;
        set => SetAndUpdateIfChanged(ref FFramePattern, value);
    }

    private List<TempDirectory> ChunkDirectories;
    private CancellationTokenSource? CurrentTaskCancellationTokenSource;

    public FfmpegChunkSourceNode()
    {
        File = AddInput("File", () => new ResultNodeSocketBuilder<FileInfo>());
        FileInfo = AddInput("File info", () => new ResultNodeSocketBuilder<InputFile>());
        StreamParameters = AddInput("Stream parameters", () => new ResultNodeSocketBuilder<IReadOnlyList<IParameter>>());
        
        Context = AddOutput("Context", () => new SimpleNodeSocketBuilder<ChunkProcessingContext>());
        OutFileParameters = AddOutput("Input params", () => new SimpleNodeSocketBuilder<IReadOnlyList<IParameter>>());
        StreamInfo = AddOutput("Stream", () => new SimpleNodeSocketBuilder<FfmpegStreamInfo>());
        
        ChunkDirectories = new List<TempDirectory>();
    }

    protected override void Update()
    {
        if (File.Value is not FileInfo file || FileInfo.Value is not InputFile fileInfo)
        {
            Context.Value = null;
            return;
        }

        Slice(file, fileInfo, StreamParameters.Value as IReadOnlyList<IParameter>).ContinueWith(task =>
        {
            if (task is { Exception: { } e })
                RaiseException(e);
        });
    }

    private async Task Slice(FileInfo file, InputFile fileInfo, IReadOnlyList<IParameter>? streamParams)
    {
        using var tempDir = new TempDirectory();
        var chunkDir = new TempDirectory();
        ChunkDirectories.Add(chunkDir);

        var pipeline = new ChunkPipelineContext
        {
            FileInfo = fileInfo
        };
            
        var context = new ChunkProcessingContext
        {
            Pipeline = pipeline,
            SourceDirectory = tempDir.Directory
        };

        var cts = new CancellationTokenSource();
        CurrentTaskCancellationTokenSource = cts;
        Context.Value = context;

        var offset = 0;
        var counter = 0;

        do
        {
            var parameters = new List<IParameter>
            {
                new SeekParameter { Position = $"{TimeSpan.FromSeconds(offset):g}" },
                new ToParameter() { Duration = $"{(TimeSpan.FromSeconds(offset + ChunkLength) - TimeSpan.FromMilliseconds(1)):g}" },
                new GenericParameter { Name = "i", Value = file.FullName }
            };

            if (streamParams is not null)
                parameters.AddRange(streamParams);

            using var ffmpeg = new FfmpegRunner.FfmpegRunner(
                FfmpegRunnerNode.ResolveParameters(parameters),
                new FileInfo(Path.Combine(tempDir.Directory.FullName, FramePattern)),
                FfmpegPaths.FfmpegPath);

            await ffmpeg.Run();

            var frames = tempDir.FileCount;
            if (frames == 0)
                break;

            var chunkFile = await context.BeginChunk(cts.Token);
            chunkFile.MoveTo(Path.Combine(chunkDir.Directory.FullName, Path.ChangeExtension($"chunk{counter:0000}", chunkFile.Extension)));
            
            tempDir.Clear();
            offset += ChunkLength;
            counter++;
        } while (true);

        var indexFile = new FileInfo(Path.Combine(chunkDir.Directory.FullName, "index.txt"));
        await using var indexWriter = System.IO.File.CreateText(indexFile.FullName);

        foreach (var chunkFile in chunkDir.Directory.EnumerateFiles().Where(f => f.Name != "index.txt"))
            await indexWriter.WriteLineAsync($"file '{chunkFile.Name}'");

        await indexWriter.FlushAsync();
        
        Graph?.RunInTransaction(() =>
        {
            OutFileParameters.Value = new List<IParameter>
            {
                new GenericParameter { Name = "f", Value = "concat" },
                new GenericParameter { Name = "i", Value = indexFile.FullName }
            };
            StreamInfo.Value = new FfmpegStreamInfo(new InputStreamSpecifier { InputIndex = 0 }, new List<IParameter>());
        });

        cts = null;
    }

    public void Dispose()
    {
        if (CurrentTaskCancellationTokenSource is { } cts)
        {
            cts.Cancel();
            cts.Dispose();
        }
        
        foreach (var dir in ChunkDirectories)
            dir.Dispose();
    }
}