﻿namespace Avmatic.FfmpegModel.Parameters.Video.Aom;

public class AomGrainLevelParameter : IParameter
{
    public string Name => "denoise-noise-level";
    
    public required int GrainLevel { get; set; }

    public string Value => GrainLevel.ToString();
}