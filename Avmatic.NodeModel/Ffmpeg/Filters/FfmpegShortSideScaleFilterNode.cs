﻿namespace Avmatic.NodeModel.Ffmpeg.Filters;

[Node("Scale by short side", "FFmpeg/Filters")]
public class FfmpegShortSideScaleFilterNode : FfmpegAbstractFilterNode
{
    public FfmpegFilterInputSocket Input { get; }
    public SimpleNodeSocket<FfmpegFilterOutputInfo> Output { get; }

    private int FSideLength;

    [NodeParameter]
    public int SideLength
    {
        get => FSideLength;
        set => SetAndUpdateIfChanged(ref FSideLength, value);
    }

    public FfmpegShortSideScaleFilterNode()
    {
        Input = AddInput("Input", () => new FfmpegFilterInputSocketBuilder());
        Output = AddOutput("Output", () => new SimpleNodeSocketBuilder<FfmpegFilterOutputInfo>());
    }

    protected override void Update()
    {
        if (Input.TypedValue is not {} input)
        {
            Output.Value = null;
            return;
        }

        var filter = new FfmpegFilterInfo("scale", new Dictionary<string, string>
            {
                { "w", $"'if(gt(iw\\,ih)\\,-2\\,{SideLength})'" },
                { "h", $"'if(gt(iw\\,ih)\\,{SideLength}\\,-2)'" },
                { "flags", "lanczos" }
            },
            new List<IFfmpegFilterInputInfo> { input });
        
        Output.Value = new FfmpegFilterOutputInfo(filter, 0, GetOutputPadStreamSpecifier(0));
    }
}