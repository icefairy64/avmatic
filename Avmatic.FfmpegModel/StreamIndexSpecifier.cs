﻿using System.Text.Json.Serialization;

namespace Avmatic.FfmpegModel;

public class StreamIndexSpecifier : IStreamSpecifier
{
    public required int Index { get; set; }

    [JsonIgnore]
    public string String => Index.ToString();

    protected bool Equals(StreamIndexSpecifier other)
    {
        return Index == other.Index;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((StreamIndexSpecifier)obj);
    }

    public override int GetHashCode()
    {
        return Index;
    }
}