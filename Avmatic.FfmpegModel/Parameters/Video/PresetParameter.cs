﻿namespace Avmatic.FfmpegModel.Parameters.Video;

public class PresetParameter : IParameter
{
    public string Name => "preset";
    
    public required int Preset { get; set; }

    public string Value => Preset.ToString();
}