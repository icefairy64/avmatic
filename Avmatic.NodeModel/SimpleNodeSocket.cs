﻿namespace Avmatic.NodeModel;

public class SimpleNodeSocket<T> : NodeSocket
{
    public T? TypedValue => (T?) Value;

    public SimpleNodeSocket()
    {
        Type = typeof(T);
    }

    public override bool MatchType(Type type)
    {
        return typeof(T?).IsAssignableFrom(type);
    }
}

public class SimpleNodeSocketBuilder<T> : NodeSocketBuilder<SimpleNodeSocket<T>>
{
    public override SimpleNodeSocket<T> Build()
    {
        if (Owner is not { } owner)
            throw new ArgumentException("Owner is not specified");
        if (Name is not { } name)
            throw new ArgumentException("Name is not specified");
        return new SimpleNodeSocket<T> { Owner = owner, Name = name };
    }
}