﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.NodeModel.Ffmpeg;

[Node("File to parameters", "FFmpeg")]
public class FfmpegFileParameterNode : SimpleNode
{
    public ResultNodeSocket<FileInfo> File { get; }
    public SimpleNodeSocket<IReadOnlyList<IParameter>> Parameters { get; }

    public FfmpegFileParameterNode()
    {
        File = AddInput("File", () => new ResultNodeSocketBuilder<FileInfo>());
        Parameters = AddOutput("Parameters", () => new SimpleNodeSocketBuilder<IReadOnlyList<IParameter>>());
    }

    protected override void Update()
    {
        if (File.Value is not FileInfo file)
        {
            Parameters.Value = null;
            return;
        }

        Parameters.Value = new List<IParameter>
        {
            new GenericParameter { Name = "i", Value = file.FullName }
        };
    }
}