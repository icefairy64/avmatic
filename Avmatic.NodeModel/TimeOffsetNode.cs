﻿namespace Avmatic.NodeModel;

[Node("Time offset")]
public class TimeOffsetNode : SimpleNode
{
    public ResultNodeSocket<TimeSpan> Input { get; }
    public SimpleNodeSocket<TimeSpan> Output { get; }

    private string FOffset = "x0.5";

    [NodeParameter]
    public string Offset
    {
        get => FOffset;
        set => SetAndUpdateIfChanged(ref FOffset, value);
    }

    public TimeOffsetNode()
    {
        Input = AddInput("Input", () => new ResultNodeSocketBuilder<TimeSpan>());
        Output = AddOutput("Output", () => new SimpleNodeSocketBuilder<TimeSpan>());
    }

    protected override void Update()
    {
        if (Input.Value is not TimeSpan input)
        {
            Output.Value = null;
            return;
        }

        if (Offset.StartsWith('x') && double.TryParse(Offset[1..], out var multiplier))
        {
            Output.Value = input * multiplier;
            return;
        }
        
        Output.Value = null;
    }
}