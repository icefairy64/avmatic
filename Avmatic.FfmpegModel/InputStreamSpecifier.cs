﻿using System.Text.Json.Serialization;

namespace Avmatic.FfmpegModel;

public class InputStreamSpecifier : IStreamSpecifier
{
    public required int InputIndex { get; set; }
    public IStreamSpecifier? AdditionalSpecifier { get; set; }
    
    [JsonIgnore]
    public string String => AdditionalSpecifier != null
        ? $"{InputIndex}:{AdditionalSpecifier.String}"
        : InputIndex.ToString();

    protected bool Equals(InputStreamSpecifier other)
    {
        return InputIndex == other.InputIndex && Equals(AdditionalSpecifier, other.AdditionalSpecifier);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((InputStreamSpecifier)obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(InputIndex, AdditionalSpecifier);
    }
}