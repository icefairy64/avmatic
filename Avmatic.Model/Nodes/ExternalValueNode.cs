﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Avmatic.NodeModel;

namespace Avmatic.Model.Nodes;

public abstract class ExternalValueNode : SimpleNode, INotifyPropertyChanged
{
    public ExternalValueNodeOutputSocket Output { get; }

    private string? FValue;

    public string? Value
    {
        get => FValue;
        set => SetAndUpdateIfChanged(ref FValue, value);
    }

    private string FName = "Param";
    
    [NodeParameter]
    public string Name
    {
        get => FName;
        set
        {
            if (SetAndUpdateIfChanged(ref FName, value))
                OnPropertyChanged();
        }
    }
    
    public abstract Type ValueType { get; }

    public ExternalValueNode()
    {
        Output = AddOutput("Output", () => new ExternalValueNodeOutputSocketBuilder());
        Output.SetType(ValueType);
    }

    protected abstract object? ParseValue(string value);

    protected override void Update()
    {
        Output.Value = Value is null ? null : ParseValue(Value);
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}

public class ExternalValueNodeOutputSocket : NodeSocket
{
    internal void SetType(Type type)
    {
        Type = type;
    }
    
    public override bool MatchType(Type type)
    {
        throw new NotImplementedException();
    }
}

public class ExternalValueNodeOutputSocketBuilder : NodeSocketBuilder<ExternalValueNodeOutputSocket>
{
    public override ExternalValueNodeOutputSocket Build()
    {
        if (Owner is null)
            throw new Exception("No owner specified");
        if (Name is null)
            throw new Exception("No name specified");
        
        return new ExternalValueNodeOutputSocket { Owner = Owner, Name = Name };
    }
}