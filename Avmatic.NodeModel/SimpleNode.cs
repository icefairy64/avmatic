﻿namespace Avmatic.NodeModel;

public abstract class SimpleNode : Node
{
    protected abstract void Update();

    protected bool SetAndUpdateIfChanged<T>(ref T field, T value)
    {
        if ((field == null && value == null) || (field?.Equals(value) ?? false))
            return false;
        field = value;
        Update();
        return true;
    }

    protected T AddInput<T>(string name, Func<NodeSocketBuilder<T>> factory) where T : NodeSocket
    {
        var socket = factory()
            .WithName(name)
            .WithOwner(this)
            .Build();
        socket.ValueChanged += OnInputValueChanged;
        FInputs.Add(socket);
        return socket;
    }

    protected T AddOutput<T>(string name, Func<NodeSocketBuilder<T>> factory) where T : NodeSocket
    {
        var socket = factory()
            .WithName(name)
            .WithOwner(this)
            .Build();
        FOutputs.Add(socket);
        return socket;
    }

    protected override void OnGraphChanged(NodeGraph? graph)
    {
        if (graph != null)
            Update();
    }

    private void OnInputValueChanged(object? sender, NodeSocketValueChangedArgs e)
    {
        Update();
    }
}