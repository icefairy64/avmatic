﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.NodeModel.Ffmpeg;

public record FfmpegStreamInfo(IStreamSpecifier StreamSpecifier, IReadOnlyList<IParameter> Parameters)
{
    public string StreamSelector => StreamSpecifier.String;
}