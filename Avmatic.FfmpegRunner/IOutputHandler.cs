﻿namespace Avmatic.FfmpegRunner;

public interface IOutputHandler
{
    public void HandleLine(string line);
}