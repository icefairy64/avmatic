﻿namespace Avmatic.FfmpegModel.Parameters.Video.Aom;

public class CpuUsedParameter : IParameter
{
    public string Name => "cpu-used";
    
    public required int Usage { get; set; }

    public string Value => Usage.ToString();
}