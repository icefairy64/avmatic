﻿using System.Collections;
using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;
using Avmatic.FfmpegRunner;
using Avmatic.Jobs;
using Microsoft.Extensions.Logging;

namespace Avmatic.Model;

public class FfmpegJob : Job, ILogAwareJob<FfmpegJob>, IFileJob
{
    public ILogger<FfmpegJob> Logger { private get; set; }

    public required FileInfo InputFile { get; set; }
    public required FileInfo OutputFile { get; set; }
    public bool TwoPass { get; set; } = false;
    public FileInfo? FfprobePath { get; set; }
    public FileInfo? FfmpegPath { get; set; }
    public required IReadOnlyList<IStreamConfiguration> Streams { get; set; }
    public IList<IParameter>? InputParameters { get; set; }

    public override async Task Start()
    {
        Logger.LogInformation("{InputFile}: probing", InputFile.Name);
        
        EmitProgress(new JobProgressArgs
        {
            Job = this,
            Stage = "Probing",
            StageProgress = 0,
            TotalProgress = 0
        });

        using var probe = new FfProbeRunner(InputFile, FfprobePath);
        var input = await probe.Run();
        
        ThrowIfCancelled();
        
        Logger.LogInformation("{InputFile}: probe results: {Result}", InputFile.Name, input.ToString());
        
        var inputDuration = input.Format.Duration;
        var builder = new CommandLineBuilder()
            .WithInput(input);
        
        foreach (var streamConfig in Streams)
        {
            if (input.Streams.Count <= streamConfig.StreamIndex || input.Streams[streamConfig.StreamIndex].CodecType != streamConfig.CodecType)
                continue;
            var stream = input.Streams[streamConfig.StreamIndex];
            var specializedConfig = streamConfig.Clone();
            specializedConfig.InputStream = stream;
            builder.UseStream(stream)
                .ConfigureStream(stream, streamParams =>
                {
                    foreach (var param in streamConfig.Parameters)
                        streamParams.WithParameter(param);
                });
        }

        var streamParams = (InputParameters is null ? builder.BuildParameters() : InputParameters.Concat(builder.BuildParameters()))
            .Prepend(new GenericParameter { Name = "i", Value = InputFile.FullName })
            .ToList();

        EmitFfmpegProgress(1, 0);
        
        // First pass

        var firstPassParams = new List<IParameter>(streamParams);
        var passLogPath = TwoPass ? Path.GetTempFileName() : null;

        if (TwoPass)
        {
            firstPassParams.Add(new PassParameter { Pass = Pass.First });
            firstPassParams.Add(new GenericParameter { Name = "passlogfile", Value = passLogPath });
        }

        using var ffmpeg = new FfmpegRunner.FfmpegRunner(firstPassParams, OutputFile, FfmpegPath);
        ffmpeg.Progress += (_, args) => EmitFfmpegProgress(1,  inputDuration == null ? null : args.OutTime / inputDuration);
        
        Logger.LogInformation("{OutputFile}: first pass", OutputFile.Name);
        Logger.LogDebug("{OutputFile}: first pass command line: {command}", OutputFile.Name, string.Join(' ', CommandLineBuilder.BuildCommandLineArguments(firstPassParams)));

        try
        {
            await ffmpeg.Run(CancellationToken);
        }
        catch (Exception e)
        {
            Logger.LogError(e, "An error occured during first FFmpeg pass");
            throw;
        }
        
        ThrowIfCancelled();
        
        Logger.LogInformation("{OutputFile}: first pass complete", OutputFile.Name);
        
        // Second pass
        
        if (!TwoPass)
            return;
        
        var secondPassParams = new List<IParameter>(streamParams);

        secondPassParams.Add(new PassParameter { Pass = Pass.Second });
        secondPassParams.Add(new GenericParameter { Name = "passlogfile", Value = passLogPath });

        using var ffmpeg2 = new FfmpegRunner.FfmpegRunner(secondPassParams, OutputFile, FfmpegPath);
        ffmpeg2.Progress += (_, args) => EmitFfmpegProgress(2, args.OutTime / inputDuration);

        Logger.LogInformation("{OutputFile}: second pass", OutputFile.Name);
        Logger.LogDebug("{OutputFile}: second pass command line: {command}", OutputFile.Name, string.Join(' ', CommandLineBuilder.BuildCommandLineArguments(secondPassParams)));
        
        try
        {
            await ffmpeg2.Run(CancellationToken);
        }
        catch (Exception e)
        {
            Logger.LogError(e, "An error occured during second FFmpeg pass");
            throw;
        }
        
        ThrowIfCancelled();
        
        Logger.LogInformation("{OutputFile}: second pass complete", OutputFile.Name);
    }

    private void EmitFfmpegProgress(int pass, double? progress)
    {
        if (progress is null)
        {
            EmitProgress(new JobProgressArgs
            {
                Job = this,
                Stage = $"FFmpeg Pass {pass}"
            });
        }

        var totalProgress = progress;
        
        if (TwoPass)
            totalProgress = progress / 2 + (pass - 1) * 0.5d;
        
        EmitProgress(new JobProgressArgs
        {
            Job = this,
            Stage = $"FFmpeg Pass {pass}",
            StageProgress = progress,
            TotalProgress = totalProgress
        });
    }
}