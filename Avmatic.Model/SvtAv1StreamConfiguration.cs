﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;
using Avmatic.FfmpegModel.Parameters.Video;
using Avmatic.FfmpegModel.Parameters.Video.Svt;
using DynamicData;

namespace Avmatic.Model;

[StreamConfiguration("svtav1", StreamType.Video)]
public class SvtAv1StreamConfiguration : VideoStreamConfiguration
{
    [StreamConfigurationParameter("Preset", 4)]
    public int? Preset { get; set; }
    
    [StreamConfigurationParameter("CRF", 28)]
    public int? Crf { get; set; }
    
    [StreamConfigurationParameter("Film grain", 8)]
    public int? FilmGrain { get; set; }
    
    [StreamConfigurationParameter("Film grain denoise", 0)]
    public int? FilmGrainDenoise { get; set; }
    
    [StreamConfigurationParameter("Tune", 0)]
    public int? Tune { get; set; }
    
    public override IList<IParameter> Parameters
    {
        get
        {
            var parameters = base.Parameters;
            
            parameters.Add(new VideoCodecParameter { Codec = VideoCodec.SvtAv1 });
            
            if (Crf is {} crf)
                parameters.Add(new CrfParameter { Crf = crf });
            
            if (Preset is {} preset)
                parameters.Add(new PresetParameter { Preset = preset });

            if (FilmGrain is not null || FilmGrainDenoise is not null || Tune is not null)
            {
                parameters.Add(new SvtExtraParameter
                {
                    FilmGrain = FilmGrain,
                    FilmGrainDenoise = FilmGrainDenoise,
                    Tune = Tune
                });
            }

            return parameters;
        }
    }

    public override IStreamConfiguration Clone()
    {
        return new SvtAv1StreamConfiguration
        {
            StreamIndex = StreamIndex,
            InputStream = InputStream,
            PixelFormat = PixelFormat,
            Resolution = Resolution,
            CodecType = CodecType,
            KeyframeInterval = KeyframeInterval,
            FilmGrain = FilmGrain,
            FilmGrainDenoise = FilmGrainDenoise,
            Tune = Tune
        };
    }
}