﻿using Microsoft.Extensions.DependencyInjection;

namespace Avmatic.Terminal;

public interface IServiceVerb
{
    public void RegisterHostedService(IServiceCollection services);
}