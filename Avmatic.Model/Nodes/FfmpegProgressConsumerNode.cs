﻿using Avmatic.NodeModel;
using Avmatic.NodeModel.Ffmpeg;

namespace Avmatic.Model.Nodes;

[Node("FFmpeg progress consumer")]
public class FfmpegProgressConsumerNode : SimpleNode
{
    public ResultNodeSocket<FfmpegRunnerProgress> Progress { get; }
    public ResultNodeSocket<TimeSpan> Duration { get; }

    public event EventHandler<FfmpegProgressConsumedEventArgs>? ProgressConsumed; 

    public FfmpegProgressConsumerNode()
    {
        Progress = AddInput("Progress", () => new ResultNodeSocketBuilder<FfmpegRunnerProgress>());
        Duration = AddInput("Duration", () => new ResultNodeSocketBuilder<TimeSpan>());
    }
    
    protected override void Update()
    {
        if (Progress.Value is not FfmpegRunnerProgress progress)
            return;

        ProgressConsumed?.Invoke(this, new FfmpegProgressConsumedEventArgs
        {
            Progress = progress,
            TotalDuration = Duration.Value as TimeSpan?
        });
    }
}

public class FfmpegProgressConsumedEventArgs : EventArgs
{
    public required FfmpegRunnerProgress Progress { get; init; }
    public TimeSpan? TotalDuration { get; init; }
}