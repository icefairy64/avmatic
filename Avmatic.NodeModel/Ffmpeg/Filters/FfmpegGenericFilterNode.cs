﻿namespace Avmatic.NodeModel.Ffmpeg.Filters;

[Node("Generic filter", "FFmpeg/Filters")]
public class FfmpegGenericFilterNode : FfmpegAbstractFilterNode
{
    public FfmpegFilterInputSocket Input { get; }
    public SimpleNodeSocket<FfmpegFilterOutputInfo> Output { get; }

    private string FName = "";

    [NodeParameter]
    public string Name
    {
        get => FName;
        set => SetAndUpdateIfChanged(ref FName, value);
    }
    
    private string FParameters = "";

    [NodeParameter]
    public string Parameters
    {
        get => FParameters;
        set => SetAndUpdateIfChanged(ref FParameters, value);
    }

    public FfmpegGenericFilterNode()
    {
        Input = AddInput("Input", () => new FfmpegFilterInputSocketBuilder());
        Output = AddOutput("Output", () => new SimpleNodeSocketBuilder<FfmpegFilterOutputInfo>());
    }

    protected override void Update()
    {
        if (Input.TypedValue is not { } input)
        {
            Output.Value = null;
            return;
        }

        var paramDict = Parameters.Split(':')
            .Select(str => str.Split('='))
            .Where(pair => pair.Length == 2)
            .ToDictionary(pair => pair[0], pair => pair[1]);
        
        var filter = new FfmpegFilterInfo(Name, paramDict, new List<IFfmpegFilterInputInfo> { input });

        Output.Value = new FfmpegFilterOutputInfo(filter, 0, GetOutputPadStreamSpecifier(0));
    }
}