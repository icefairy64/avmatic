using System;
using System.Collections.Generic;
using Avmatic.Model;
using Avmatic.Model.Nodes;
using Avmatic.NodeModel;
using Avmatic.NodeModel.Ffmpeg;
using Eto.Forms;
using Eto.Drawing;
using Microsoft.Extensions.Logging;

namespace Avmatic.Eto;

public partial class MainForm : Form
{
    public MainForm(ILoggerFactory loggerFactory)
    {
        Title = "Avmatic";
        MinimumSize = new Size(640, 480);

        var progressTabViewModel = new ProgressTabModel(loggerFactory, loggerFactory.CreateLogger<ProgressTabModel>())
        {
            FfmpegPath = FfmpegUtils.GetFfToolPath("ffmpeg"),
            FfprobePath = FfmpegUtils.GetFfToolPath("ffprobe")
        };

        var matrixTabViewModel = new MatrixTabModel(progressTabViewModel)
        {
            FfprobePath = FfmpegUtils.GetFfToolPath("ffprobe")
        };

        var nodeTabViewModel = new NodeTabModel(progressTabViewModel, new NodeGraph(), loggerFactory);

        Content = new TabControl
        {
            Pages =
            {
                new TabPage
                {
                    Text = "Transcoding",
                    Content = new TranscodingTab(progressTabViewModel)
                },
                new TabPage
                {
                    Text = "Matrix",
                    Content = new MatrixTab(matrixTabViewModel)
                },
                new TabPage
                {
                    Text = "Progress",
                    Content = new ProgressTab(progressTabViewModel)
                },
                new TabPage
                {
                    Text = "Nodes",
                    Content = new NodeTab(nodeTabViewModel)
                }
            }
        };
    }
}