﻿namespace Avmatic.NodeModel.Tests;

public class MockSourceNode<T> : Node
{
    public SimpleNodeSocket<T> Output { get; }

    public MockSourceNode()
    {
        Output = new SimpleNodeSocket<T> { Owner = this, Name = "Output" };
        FOutputs.Add(Output);
    }

    public void Produce(T value)
    {
        Output.Value = value;
    }
}

public class MockSinkNode<T> : Node
{
    public ResultNodeSocket<T> Input { get; }

    public List<T?> Values { get; } = new();

    public MockSinkNode()
    {
        Input = new ResultNodeSocket<T> { Owner = this, Name = "Input" };
        
        Input.ValueChanged += OnInputValueChanged;
        
        FInputs.Add(Input);
    }

    private void OnInputValueChanged(object? sender, NodeSocketValueChangedArgs e)
    {
        if (e.Value is T value)
            Values.Add(value);
    }

    public void Reset()
    {
        Values.Clear();
    }
}

public class MockPassthroughNode<T> : Node
{
    public ResultNodeSocket<T> Input { get; }
    public SimpleNodeSocket<T> Output { get; }

    public MockPassthroughNode()
    {
        Input = new ResultNodeSocket<T> { Owner = this, Name = "Input" };
        Output = new SimpleNodeSocket<T> { Owner = this, Name = "Output" };
        
        Input.ValueChanged += OnInputValueChanged;
        
        FInputs.Add(Input);
        FOutputs.Add(Output);
    }

    private void OnInputValueChanged(object? sender, NodeSocketValueChangedArgs e)
    {
        Output.Value = Input.Value;
    }
}

public class MockManualPassthroughNode<T> : Node, ITickableNode
{
    public ResultNodeSocket<T> Input { get; }
    public SimpleNodeSocket<T> Output { get; }

    public List<T> Ticks { get; } = new();

    public event EventHandler? Ticking;
    public event EventHandler? Ticked;

    public Task? Task { get; private set; }
    
    private TaskCompletionSource? CompletionSource;

    public MockManualPassthroughNode()
    {
        Input = new ResultNodeSocket<T> { Owner = this, Name = "Input" };
        Output = new SimpleNodeSocket<T> { Owner = this, Name = "Output" };
        
        FInputs.Add(Input);
        FOutputs.Add(Output);
    }

    public bool Tick()
    {
        Ticking?.Invoke(this, EventArgs.Empty);
        
        if (Input.Value is T value)
            Ticks.Add(value);
        
        CompletionSource = new TaskCompletionSource();

        Task = CompletionSource.Task.ContinueWith(_ =>
        {
            Output.Value = Input.Value;
        });

        Ticked?.Invoke(this, EventArgs.Empty);
        
        return true;
    }
    
    public void Run()
    {
        CompletionSource?.TrySetResult();
        CompletionSource = null;
    }
}

public class MockManualPassthroughDoubleNode<T> : Node, ITickableNode
{
    public ResultNodeSocket<T> Input { get; }
    public SimpleNodeSocket<T> OutputA { get; }
    public SimpleNodeSocket<T> OutputB { get; }

    public List<T> Ticks { get; } = new();

    public event EventHandler? Ticking;
    public event EventHandler? Ticked;
    
    public Task? Task { get; private set; }

    private TaskCompletionSource? CompletionSource;

    public MockManualPassthroughDoubleNode()
    {
        Input = new ResultNodeSocket<T> { Owner = this, Name = "Input" };
        OutputA = new SimpleNodeSocket<T> { Owner = this, Name = "Output A" };
        OutputB = new SimpleNodeSocket<T> { Owner = this, Name = "Output B" };
        
        FInputs.Add(Input);
        FOutputs.Add(OutputA);
        FOutputs.Add(OutputB);
    }

    public bool Tick()
    {
        Ticking?.Invoke(this, EventArgs.Empty);
        
        if (Input.Value is T value)
            Ticks.Add(value);

        CompletionSource = new TaskCompletionSource();

        Task = CompletionSource.Task.ContinueWith(_ =>
        {
            if (Graph is { } graph)
            {
                graph.RunInTransaction(() =>
                {
                    OutputA.Value = Input.Value;
                    OutputB.Value = Input.Value;
                });
            }
        });

        Ticked?.Invoke(this, EventArgs.Empty);
        
        return true;
    }

    public void Run()
    {
        CompletionSource?.TrySetResult();
        CompletionSource = null;
    }
}