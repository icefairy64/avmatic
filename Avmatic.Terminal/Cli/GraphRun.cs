﻿using System.Text.Json;
using Avmatic.Model.Nodes;
using Avmatic.NodeModel.Serialization;
using CommandLine;
using DynamicData;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Avmatic.Terminal.Cli;

[Verb("rungraph", HelpText = "Runs a provided graph pipeline against provided input files")]
public class GraphRunOptions : IServiceVerb
{
    [Option('g',"graph", Required = true)]
    public required string GraphDescriptionPath { get; set; }
    
    [Option('i',"input", Required = true)]
    public required IEnumerable<string> InputFilePaths { get; set; }
    
    [Option('o',"outdir", Required = true)]
    public required string OutputDirPath { get; set; }
    
    [Option('j',"jobs", Default = 6)]
    public required int JobCapacity { get; set; }
    
    [Option("params")]
    public IEnumerable<string>? ExternalValues { get; set; }

    public void RegisterHostedService(IServiceCollection services)
    {
        services.AddSingleton(this)
            .AddHostedService<GraphRunService>();
    }
}

public class GraphRunService : BackgroundService
{
    private readonly GraphRunOptions Options;
    private readonly NodeTabModel NodeTabModel;
    private readonly ILogger<GraphRunService> Logger;

    public GraphRunService(GraphRunOptions options, NodeTabModel nodeTabModel, ILogger<GraphRunService> logger, IHostApplicationLifetime lifetime)
    {
        Options = options;
        NodeTabModel = nodeTabModel;
        Logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await using var graphJsonStream = File.Open(Options.GraphDescriptionPath, FileMode.Open);
        if (await JsonSerializer.DeserializeAsync<VisualNodeGraphDto>(graphJsonStream, cancellationToken: stoppingToken) is not {} graphDto)
            throw new Exception("Failed to read graph description");
        
        Logger.LogInformation("Graph loaded");

        NodeTabModel.Graph = graphDto.Graph.Materialize();
        
        Logger.LogInformation("Graph materialized");

        var overrideValues = Options.ExternalValues
            ?.Select(s => s.Split('='))
            .Where(pair => pair.Length == 2)
            .ToDictionary(pair => pair[0], pair => pair[1]);

        foreach (var valueVm in NodeTabModel.ExternalValues)
        {
            var value = graphDto.ExternalValues?[valueVm.Name];
            if (overrideValues?.TryGetValue(valueVm.Name, out var overridenValue) ?? false)
                value = overridenValue;
            
            if (value != null)
            {
                Logger.LogInformation("Setting external value {} to {}", valueVm.Name, value);
                valueVm.Value = value;
            }
        }

        NodeTabModel.JobCapacity = Options.JobCapacity;
        NodeTabModel.OutputDirectory = new DirectoryInfo(Options.OutputDirPath);
        NodeTabModel.SourceFiles.AddRange(Options.InputFilePaths.Select(path => new FileInfo(path)));
        
        Logger.LogInformation("Starting");
        
        NodeTabModel.StartCommand.Execute(null);
    }
}