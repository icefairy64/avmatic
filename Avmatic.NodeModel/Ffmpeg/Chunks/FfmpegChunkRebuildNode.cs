﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.NodeModel.Ffmpeg.Chunks;

[Node("Chunk rebuild", "FFmpeg")]
public class FfmpegChunkRebuildNode : SimpleNode
{
    public ResultNodeSocket<ChunkProcessingContext> Context { get; }
    public ResultNodeSocket<FfmpegRunnerProgress> Progress { get; }

    public SimpleNodeSocket<IReadOnlyList<IParameter>> OutputParameters;
    public SimpleNodeSocket<FileInfo> OutputFile;

    private string FFramePattern = "f%06d.png";

    [NodeParameter]
    public string FramePattern
    {
        get => FFramePattern;
        set => SetAndUpdateIfChanged(ref FFramePattern, value);
    }
    
    private string FFormat = ".webm";

    [NodeParameter]
    public string Format
    {
        get => FFormat;
        set => SetAndUpdateIfChanged(ref FFormat, value);
    }

    private ChunkProcessingContext? CurrentContext;
    private FileInfo? CurrentFile;

    public FfmpegChunkRebuildNode()
    {
        Context = AddInput("Context", () => new ResultNodeSocketBuilder<ChunkProcessingContext>());
        Progress = AddInput("Progress", () => new ResultNodeSocketBuilder<FfmpegRunnerProgress>());
        OutputParameters = AddOutput("Parameters", () => new SimpleNodeSocketBuilder<IReadOnlyList<IParameter>>());
        OutputFile = AddOutput("File", () => new SimpleNodeSocketBuilder<FileInfo>());
    }

    protected override void Update()
    {
        if (Context.Value is not ChunkProcessingContext context)
            return;
        
        if (context != CurrentContext)
            UpdateContext(context);

        if (Progress.Value is FfmpegRunnerProgress { Complete: true })
            context.EndChunk(CurrentFile ?? throw new Exception("No file"));
    }

    private void UpdateContext(ChunkProcessingContext context)
    {
        if (CurrentContext is { } oldContext)
            oldContext.ChunkStarted -= OnChunkStarted;
        
        CurrentContext = context;
        
        context.ChunkStarted += OnChunkStarted;
    }

    private void OnChunkStarted(object? sender, ChunkStartedEventArgs e)
    {
        if (sender is not ChunkProcessingContext context)
            throw new Exception();
        
        CurrentFile = new FileInfo(Path.GetTempFileName());
        CurrentFile.MoveTo(Path.Combine(CurrentFile.DirectoryName ?? "", Path.ChangeExtension(CurrentFile.Name, Format)));
        
        var (numerator, denominator) = context.Pipeline.FileInfo.Streams.OfType<VideoInputStream>().First().Framerate;
        
        Graph?.RunInTransaction(() =>
        {
            OutputFile.Value = CurrentFile;
            OutputParameters.Value = new List<IParameter>
            {
                new GenericParameter { Name = "framerate", Value = $"{(numerator / (double)denominator):F4}" },
                new GenericParameter { Name = "i", Value = Path.Combine(context.SourceDirectory.FullName, FramePattern) }
            };
        });
    }
}