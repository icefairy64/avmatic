﻿using Avmatic.FfmpegModel;

namespace Avmatic.Model;

[AttributeUsage(AttributeTargets.Class)]
public class StreamConfigurationAttribute : Attribute
{
    public string Name { get; }
    public StreamType? StreamType { get; }
    
    public StreamConfigurationAttribute(string name)
    {
        Name = name;
    }
    
    public StreamConfigurationAttribute(string name, StreamType streamType)
    {
        Name = name;
        StreamType = streamType;
    }
}