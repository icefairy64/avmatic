﻿using System.IO;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Avmatic.Model;
using Eto.Forms;
using ReactiveUI;

namespace Avmatic.Eto;

public sealed class MatrixTab : Panel
{
    public MatrixTabModel ViewModel { get; }

    private readonly CompositeDisposable CompositeDisposable = new();

    public MatrixTab(MatrixTabModel viewModel)
    {
        ViewModel = viewModel;
        
        viewModel.OutputDirectorySelectInteraction
            .RegisterHandler(ctx => ctx.SetOutput(OpenSelectOutputDirectoryDialog()))
            .DisposeWith(CompositeDisposable);
        
        viewModel.FileOpenInteraction
            .RegisterHandler(ctx => ctx.SetOutput(OpenSelectFileDialog()))
            .DisposeWith(CompositeDisposable);
        
        var fileSelectorButton = new Button
        {
            Text = "Select a file",
            Command = new Command((_, _) => viewModel.SelectInputFile())
        };
        
        var directorySelectorButton = new Button
        {
            Text = "Select output directory",
            Command = new Command((_, _) => viewModel.SelectOutputDirectory())
        };
        
        var startButton = new Button
        {
            Text = "Start",
            Command = viewModel.StartCommand
        };
        
        var fileInfoLabel = new Label();
        viewModel.WhenAnyValue(vm => vm.InputFile)
            .Select(file => file?.ToString() ?? "No file selected")
            .BindToControl(fileInfoLabel, label => label.Text)
            .DisposeWith(CompositeDisposable);
        
        var outputDirLabel = new Label();
        viewModel.WhenAnyValue(vm => vm.OutputDirectory)
            .Select(dir => dir?.FullName ?? "No directory selected")
            .BindToControl(outputDirLabel, label => label.Text);

        var streamStack = new StackLayout
        {
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Spacing = 8
        };
        viewModel.StreamModels
            .Connect()
            .BindToStackLayout(streamStack, (vm, control) => control.ViewModel == vm,
                vm => new StreamMatrixConfigurationPanel(vm))
            .DisposeWith(CompositeDisposable);

        Content = new StackLayout
        {
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Spacing = 8,
            Padding = 8,
            Items =
            {
                fileSelectorButton,
                fileInfoLabel,
                directorySelectorButton,
                outputDirLabel,
                FieldUtil.CreateNumericField("Jobs", viewModel, vm => vm.JobCapacity, CompositeDisposable),
                FieldUtil.CreateTextField("Output name", viewModel, vm => vm.OutputName, CompositeDisposable),
                FieldUtil.CreateTextField("Starting position", viewModel, vm => vm.TimeOffset, CompositeDisposable),
                FieldUtil.CreateNumericField("Duration (seconds)", viewModel, vm => vm.DurationSeconds, CompositeDisposable),
                new StackLayoutItem
                {
                    Control = new Scrollable { Content = streamStack },
                    Expand = true
                },
                startButton
            }
        };
    }

    private DirectoryInfo? OpenSelectOutputDirectoryDialog()
    {
        using var dialog = new SelectFolderDialog();
        return dialog.ShowDialog(this) != DialogResult.Ok ? null : new DirectoryInfo(dialog.Directory);
    }

    private FileInfo? OpenSelectFileDialog()
    {
        using var dialog = new OpenFileDialog
        {
            Filters = { new FileFilter("Video files", ".mp4", ".mkv", ".webm", ".avi") }
        };

        return dialog.ShowDialog(this) != DialogResult.Ok ? null : new FileInfo(dialog.FileName);
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            CompositeDisposable.Dispose();
        }

        base.Dispose(disposing);
    }
}