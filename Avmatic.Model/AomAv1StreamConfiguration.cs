﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;
using Avmatic.FfmpegModel.Parameters.Video;
using Avmatic.FfmpegModel.Parameters.Video.Aom;

namespace Avmatic.Model;

[StreamConfiguration("aom-av1", StreamType.Video)]
public class AomAv1StreamConfiguration : VideoStreamConfiguration
{
    [StreamConfigurationParameter("CRF", 32)]
    public int? Crf { get; set; }
    
    [StreamConfigurationParameter("Grain level", 8)]
    public int? GrainLevel { get; set; }
    
    [StreamConfigurationParameter("CPU used", 4)]
    public int? CpuUsed { get; set; }
    
    [StreamConfigurationParameter("Lag in frames", 48)]
    public int? LagInFrames { get; set; }

    [StreamConfigurationParameter("Enable DNL denoising")]
    public int? EnableDnlDenoising { get; set; }
    
    [StreamConfigurationParameter("Tune")]
    public string? Tune { get; set; }

    public override IList<IParameter> Parameters
    {
        get
        {
            var result = base.Parameters;
            
            result.Add(new VideoCodecParameter { Codec = VideoCodec.AomAv1 });

            if (Crf is { } crf)
            {
                result.Add(new CrfParameter { Crf = crf });
                result.Add(new BitrateParameter { Bitrate = 0, Unit = BitrateUnit.Bit });
            }

            if (GrainLevel is { } value)
                result.Add(new AomGrainLevelParameter { GrainLevel = value });

            if (CpuUsed is { } cpuUsed)
                result.Add(new CpuUsedParameter { Usage = cpuUsed });
            
            if (LagInFrames is { } lag)
                result.Add(new LagInFramesParameter { Frames = lag });

            if (EnableDnlDenoising is not null || Tune is not null)
            {
                result.Add(new AomExtraParameter
                {
                    EnableDnlDenoising = EnableDnlDenoising,
                    Tune = Tune
                });
            }

            if (InputStream is not VideoInputStream videoStream)
                throw new Exception("Not a video stream");

            if (videoStream.Framerate.Denominator > 1000000)
            {
                const int denominator = 900000;
                var numerator = (long)Math.Truncate((double)videoStream.Framerate.Numerator /
                    videoStream.Framerate.Denominator * denominator);
                result.Add(new GenericParameter { Name = "r", Value = $"{numerator}/{denominator}" });
            }

            return result;
        }
    }

    public override IStreamConfiguration Clone()
    {
        return new AomAv1StreamConfiguration
        {
            InputStream = InputStream,
            StreamIndex = StreamIndex,
            CodecType = CodecType,
            CpuUsed = CpuUsed,
            Crf = Crf,
            GrainLevel = GrainLevel,
            KeyframeInterval = KeyframeInterval,
            EnableDnlDenoising = EnableDnlDenoising,
            Tune = Tune,
            LagInFrames = LagInFrames,
            PixelFormat = PixelFormat,
            Resolution = Resolution
        };
    }
}