﻿namespace Avmatic.NodeModel.Ffmpeg.Filters;

public record FfmpegFilterMultipartGraphInfo(IReadOnlyList<FfmpegFilterOutputInfo> Outputs);