﻿namespace Avmatic.FfmpegModel;

public record AudioInputStream(int Index, string CodecName, string CodecType, IDictionary<string, string> Tags, int SampleRate) : InputStream(Index, CodecName, CodecType, Tags);