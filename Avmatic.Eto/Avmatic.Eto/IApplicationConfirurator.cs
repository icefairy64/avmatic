﻿namespace Avmatic.Eto;

public interface IApplicationConfirurator
{
    public void ConfigureApplication(Application application);
}