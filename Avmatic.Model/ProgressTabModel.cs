﻿using System.Reactive;
using System.Reactive.Linq;
using Avmatic.FfmpegModel.Parameters;
using Avmatic.Jobs;
using DynamicData;
using DynamicData.Binding;
using Microsoft.Extensions.Logging;
using ReactiveUI;

namespace Avmatic.Model;

public sealed class ProgressTabModel : ReactiveObject, IDisposable
{
    public FileInfo? FfprobePath { get; set; }
    public FileInfo? FfmpegPath { get; set; }
    
    public readonly IObservableList<JobProgressBarModel> JobsInProgress;
    public readonly IObservableList<JobProgressBarModel> JobsPending;
    public SourceList<JobProgressBarModel> Jobs { get; } = new();

    private readonly ObservableAsPropertyHelper<float> FTotalProgress;
    public float TotalProgress => FTotalProgress.Value;

    private DateTime? StartTime;

    private TimeSpan? FElapsed;
    public TimeSpan? Elapsed
    {
        get => FElapsed;
        private set => this.RaiseAndSetIfChanged(ref FElapsed, value);
    }
    
    private readonly ObservableAsPropertyHelper<TimeSpan?> FEta;
    public TimeSpan? Eta => FEta.Value;

    public ReactiveCommand<Unit, Unit> CancelAllJobsCommand { get; }

    private JobExecutor? Executor;
    
    private readonly ILoggerFactory LoggerFactory;
    private readonly ILogger<ProgressTabModel> Logger;

    public ProgressTabModel(ILoggerFactory loggerFactory, ILogger<ProgressTabModel> logger)
    {
        LoggerFactory = loggerFactory;
        Logger = logger;
        
        Jobs.Connect()
            .FilterOnObservable(jm => jm.WhenAnyValue(j => j.State).Select(s => s == JobState.Running))
            .BindToObservableList(out JobsInProgress)
            .Subscribe();
        
        Jobs.Connect()
            .FilterOnObservable(jm => jm.WhenAnyValue(j => j.State).Select(s => s == JobState.Queued))
            .BindToObservableList(out JobsPending)
            .Subscribe();

        Jobs.Connect()
            .AutoRefreshOnObservable(jm => jm.WhenAnyValue(j => j.State).CombineLatest(jm.WhenAnyValue(j => j.Progress)))
            .ToCollection()
            .Select(c => c.Sum(x => x.State switch
            {
                JobState.Running => x.Progress,
                JobState.Complete or JobState.Failed => 1f,
                _ => 0f
            }) / Jobs.Count)
            .Sample(TimeSpan.FromSeconds(1))
            .ToProperty(this, vm => vm.TotalProgress, out FTotalProgress);

        Observable.Interval(TimeSpan.FromSeconds(1))
            .Subscribe(x => Elapsed = StartTime == null ? null : DateTime.Now - StartTime);

        this.WhenAnyValue(vm => vm.TotalProgress)
            .DistinctUntilChanged()
            .CombineLatest(this.WhenAnyValue(vm => vm.Elapsed))
            .Sample(TimeSpan.FromSeconds(1))
            .Select(t => t switch
            {
                (<= 0f, _) => null as TimeSpan?,
                (var progress, { } elapsed) => elapsed / progress - elapsed,
                (_, null) => null
            })
            .ToProperty(this, vm => vm.Eta, out FEta);

        CancelAllJobsCommand = ReactiveCommand.Create(() => Executor?.CancelAll(), Jobs.CountChanged.Select(c => c > 0));
    }
    
    public void Start(
        IReadOnlyList<FileInfo> inputFiles,
        DirectoryInfo outputDirectory,
        IReadOnlyList<IStreamConfiguration> streams,
        string format,
        int jobCapacity,
        bool twoPass)
    {
        Setup(jobCapacity);
        
        if (Executor is null)
            return;
        
        Logger.LogInformation("Starting a batch of {JobCount} transcoding jobs over {JobCapacity} executors", inputFiles.Count, jobCapacity);

        foreach (var inputFile in inputFiles)
        {
            var outputFilename = Path.ChangeExtension(inputFile.Name, format);
            var outputFilePath = new FileInfo(Path.Combine(outputDirectory.FullName, outputFilename));
            var job = new FfmpegJob
            {
                Streams = streams,
                TwoPass = twoPass,
                InputFile = inputFile,
                OutputFile = outputFilePath,
                FfmpegPath = FfmpegPath,
                FfprobePath = FfprobePath
            };
            AddJob(job);
        }
    }
    
    public void StartMatrix(
        FileInfo file,
        DirectoryInfo outputDirectory,
        IDictionary<string, IStreamConfiguration> configurations,
        string format,
        string offset,
        double duration,
        int jobCapacity,
        bool twoPass)
    {
        Setup(jobCapacity);
        
        if (Executor is null)
            return;
        
        Logger.LogInformation("Starting a batch of {JobCount} matrix jobs over {JobCapacity} executors", configurations.Count, jobCapacity);
        
        foreach (var pair in configurations)
        {
            var outputFilename = Path.ChangeExtension(pair.Key, format);
            var outputFilePath = new FileInfo(Path.Combine(outputDirectory.FullName, outputFilename));
            var job = new FfmpegJob
            {
                Streams = new List<IStreamConfiguration> { pair.Value },
                TwoPass = twoPass,
                InputFile = file,
                OutputFile = outputFilePath,
                FfmpegPath = FfmpegPath,
                FfprobePath = FfprobePath,
                InputParameters = new List<IParameter>
                {
                    new GenericParameter { Name = "ss", Value = offset },
                    new GenericParameter { Name = "t", Value = $"{duration}s" }
                }
            };
            AddJob(job);
        }
    }

    public void Start(IEnumerable<Job> jobs, int jobCapacity)
    {
        Setup(jobCapacity);
        
        if (Executor is null)
            return;
        
        Logger.LogInformation("Starting a batch of {JobCount} jobs over {JobCapacity} executors", jobs, jobCapacity);
        
        foreach (var job in jobs)
            AddJob(job);
    }

    private void AddJob(Job job)
    {
        if (Executor is not {} executor)
            return;
        Jobs.Add(new JobProgressBarModel(job));
        executor.Enqueue(job);
    }

    private void Setup(int jobCapacity)
    {
        Executor = new JobExecutor(LoggerFactory)
        {
            JobCapacity = jobCapacity
        };

        Executor.JobProgress += OnJobProgress;
        Executor.JobStateChanged += OnJobStateChanged;
        
        Jobs.Clear();
        StartTime = DateTime.Now;
        Elapsed = TimeSpan.Zero;
    }

    private void OnJobStateChanged(object? sender, JobStateChangeArgs e)
    {
        var job = Jobs.Items.First(j => j.Job == e.Job);
        job.State = e.State;
        
        if (job.State is JobState.Complete or JobState.Failed or JobState.Canceled)
            job.Progress = 1f;
        
        if (job.State is JobState.Complete or JobState.Failed or JobState.Running or JobState.Canceled && job.Job is FfmpegJob ffmpegJob)
            Logger.LogInformation("Job for {File} is {State}", ffmpegJob.OutputFile.Name, job.State);
    }

    private void OnJobProgress(object? sender, JobProgressArgs e)
    {
        var job = Jobs.Items.First(j => j.Job == e.Job);
        job.Progress = (float)e.TotalProgress;
        job.Stage = e.Stage;
    }

    public void Dispose()
    {
        Executor?.Dispose();
    }
}