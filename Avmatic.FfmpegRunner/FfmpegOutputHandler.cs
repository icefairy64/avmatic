﻿namespace Avmatic.FfmpegRunner;

public class FfmpegOutputHandler : IOutputHandler
{
    public event EventHandler<FfmpegProgressArgs>? Progress;

    private TimeSpan OutTime;
    private long TotalBytes;
    private float Fps;
    private int Frame;
    
    public void HandleLine(string line)
    {
        var key = line[..line.IndexOf('=')];
        var value = line[(key.Length + 1)..];

        switch (key)
        {
            case "frame":
            {
                Frame = int.Parse(value);
                break;
            }
            case "fps":
            {
                Fps = float.Parse(value);
                break;
            }
            case "total_size":
            {
                if (long.TryParse(value, out var bytes))
                    TotalBytes = bytes;
                break;
            }
            case "out_time_us":
            {
                var us = long.Parse(value);
                if (us < 0) 
                    us = 0;
                OutTime = TimeSpan.FromMicroseconds(us);
                break;
            }
            case "progress":
            {
                RaiseProgress(new FfmpegProgressArgs
                {
                    Frame = Frame,
                    Fps = Fps,
                    TotalBytes = TotalBytes,
                    OutTime = OutTime,
                    Done = value == "end"
                });
                break;
            }
        }
    }

    protected virtual void RaiseProgress(FfmpegProgressArgs e)
    {
        Progress?.Invoke(this, e);
    }
}

public class FfmpegProgressArgs : EventArgs
{
    public required TimeSpan OutTime { get; set; }
    public required long TotalBytes { get; set; }
    public required float Fps { get; set; }
    public required int Frame { get; set; }
    public required bool Done { get; set; }

    public override string ToString()
    {
        return $"{nameof(OutTime)}: {OutTime}, {nameof(TotalBytes)}: {TotalBytes}, {nameof(Fps)}: {Fps}, {nameof(Frame)}: {Frame}, {nameof(Done)}: {Done}";
    }
}