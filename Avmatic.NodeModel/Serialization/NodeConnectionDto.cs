﻿namespace Avmatic.NodeModel.Serialization;

public record NodeConnectionDto(int SourceNodeIndex, string SourceSocket, int TargetNodeIndex, string TargetSocket);