﻿using Avmatic.FfmpegRunner;

namespace AvMatic.FfmpegRunner.Tests.Ffmpeg;

public class FfmpegOutputTests
{
    private Dictionary<string, FileInfo> Dumps;
    
    [SetUp]
    public void Setup()
    {
        var dir = new DirectoryInfo(Path.Join(TestContext.CurrentContext.TestDirectory, "Ffmpeg", "Dumps"));
        Dumps = dir.EnumerateFiles().ToDictionary(f => f.Name);
    }

    [Test]
    public void Test1()
    {
        var handler = new FfmpegOutputHandler();
        
        FfmpegProgressArgs? args = null;
        var entries = 0;
        handler.Progress += (sender, progressArgs) =>
        {
            entries++;
            args = progressArgs;
        };
        
        var file = Dumps["ffmpeg_r0td.txt"];
        var lines = File.ReadLines(file.FullName);
        
        foreach (var line in lines) 
            handler.HandleLine(line);
        
        Assert.That(args, Is.Not.Null);
        Assert.That(args.Done, Is.True);
        Assert.That(args.Frame, Is.EqualTo(90));
        Assert.That(args.Fps, Is.EqualTo(12.39f));
        Assert.That(args.OutTime, Is.EqualTo(TimeSpan.FromMilliseconds(3001)));
        Assert.That(args.TotalBytes, Is.EqualTo(1122967));
        Assert.That(entries, Is.EqualTo(12));
    }
}