﻿namespace Avmatic.NodeModel.Ffmpeg.Filters;

[Node("Box blur", "FFmpeg/Filters")]
public class FfmpegBoxBlurFilterNode : FfmpegAbstractFilterNode
{
    public FfmpegFilterInputSocket Input { get; }
    public SimpleNodeSocket<FfmpegFilterOutputInfo> Output { get; }

    private int FLumaRadius = 2;

    [NodeParameter]
    public int LumaRadius
    {
        get => FLumaRadius;
        set => SetAndUpdateIfChanged(ref FLumaRadius, value);
    }
    
    private int FLumaPower = 2;

    [NodeParameter]
    public int LumaPower
    {
        get => FLumaPower;
        set => SetAndUpdateIfChanged(ref FLumaPower, value);
    }
    
    private int FChromaRadius = 2;

    [NodeParameter]
    public int ChromaRadius
    {
        get => FChromaRadius;
        set => SetAndUpdateIfChanged(ref FChromaRadius, value);
    }
    
    private int FChromaPower = 2;

    [NodeParameter]
    public int ChromaPower
    {
        get => FChromaPower;
        set => SetAndUpdateIfChanged(ref FChromaPower, value);
    }
    
    private int FAlphaRadius = 2;

    [NodeParameter]
    public int AlphaRadius
    {
        get => FAlphaRadius;
        set => SetAndUpdateIfChanged(ref FAlphaRadius, value);
    }
    
    private int FAlphaPower = 2;

    [NodeParameter]
    public int AlphaPower
    {
        get => FAlphaPower;
        set => SetAndUpdateIfChanged(ref FAlphaPower, value);
    }

    public FfmpegBoxBlurFilterNode()
    {
        Input = AddInput("Input", () => new FfmpegFilterInputSocketBuilder());
        Output = AddOutput("Output", () => new SimpleNodeSocketBuilder<FfmpegFilterOutputInfo>());
    }

    protected override void Update()
    {
        if (Input.TypedValue is not { } input)
        {
            Output.Value = null;
            return;
        }

        var filter = new FfmpegFilterInfo("boxblur", new Dictionary<string, string>
        {
            { "lr", LumaRadius.ToString() },
            { "lp", LumaPower.ToString() },
            { "cr", ChromaRadius.ToString() },
            { "cp", ChromaPower.ToString() },
            { "ar", AlphaRadius.ToString() },
            { "ap", AlphaPower.ToString() },
        }, new List<IFfmpegFilterInputInfo> { input });

        Output.Value = new FfmpegFilterOutputInfo(filter, 0, GetOutputPadStreamSpecifier(0));
    }
}