﻿using System;

namespace Avmatic.Eto.Mac
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Startup.Run();
        }
    }
}