﻿namespace Avmatic.FfmpegModel.Parameters.Video;

public class CrfParameter : IParameter
{
    public string Name => "crf";
    
    public required int Crf { get; set; }

    public string Value => Crf.ToString();
}