﻿using ReactiveUI;

namespace Avmatic.Model;

public class ReactiveFieldModel<T> : ReactiveObject
{
    private string FName;
    public string Name
    {
        get => FName;
        set => this.RaiseAndSetIfChanged(ref FName, value);
    }
    
    public string? BackingMemberName { get; }
    
    private T FValue;
    public T Value
    {
        get => FValue;
        set => this.RaiseAndSetIfChanged(ref FValue, value);
    }
    
    public ReactiveFieldModel(string name, string? backingMemberName, T value)
    {
        Name = name;
        BackingMemberName = backingMemberName;
        FValue = value;
    }
}