﻿namespace Avmatic.FfmpegModel.Parameters;

public class GenericParameter : IParameter
{
    public required string Name { get; set; }
    public required string Value { get; set; }
}