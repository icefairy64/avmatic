﻿namespace Avmatic.NodeModel.Ffmpeg;

public class FfmpegTimeConverterNode : SimpleNode
{
    public ResultNodeSocket<TimeSpan> Input { get; }
    public SimpleNodeSocket<string> Output { get; }

    public FfmpegTimeConverterNode()
    {
        Input = AddInput("Input", () => new ResultNodeSocketBuilder<TimeSpan>());
        Output = AddOutput("Output", () => new SimpleNodeSocketBuilder<string>());
    }

    protected override void Update()
    {
        if (Input.Value is not TimeSpan input)
        {
            Output.Value = null;
            return;
        }

        Output.Value = $"{input.TotalMilliseconds}ms";
    }
}