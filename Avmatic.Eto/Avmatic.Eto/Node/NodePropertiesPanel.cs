﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using DynamicData;
using Eto.Drawing;
using Eto.Forms;

namespace Avmatic.Eto.Node;

public class NodePropertiesPanel : Panel
{
    public VisualNode Node { get; }

    public NodePropertiesPanel(VisualNode node) : base()
    {
        var fields = new List<StackLayout>();

        foreach (var parameterName in node.Parameters)
        {
            if (node.GetParameterType(parameterName) is not {} type)
                continue;
            
            if (typeof(int).IsAssignableFrom(type))
            {
                var binding = new VisualNodeParameterBinding<int>(node, parameterName);
                fields.Add(FieldUtil.CreateNumericField(parameterName, binding));
            }
            else if (typeof(int?).IsAssignableFrom(type))
            {
                var binding = new VisualNodeNullableNumericParameterBinding<int>(node, parameterName);
                fields.Add(FieldUtil.CreateTextField(parameterName, binding));
            }
            if (typeof(bool).IsAssignableFrom(type))
            {
                var binding = new VisualNodeParameterBinding<bool>(node, parameterName);
                fields.Add(FieldUtil.CreateCheckboxField(parameterName, binding));
            }
            else if (typeof(string).IsAssignableFrom(type))
            {
                var binding = new VisualNodeParameterBinding<string>(node, parameterName);
                fields.Add(FieldUtil.CreateTextField(parameterName, binding));
            }
            else if (type.IsEnum)
            {
                var binding = new VisualNodeEnumParameterBinding(node, parameterName);
                var values = type.GetEnumNames();
                fields.Add(FieldUtil.CreateSelectorField(parameterName, binding, values));
            }
            else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>) && (Nullable.GetUnderlyingType(type)?.IsEnum ?? false))
            {
                var binding = new VisualNodeEnumParameterBinding(node, parameterName);
                var values = type.GetGenericArguments()[0].GetEnumNames().Prepend("").ToArray();
                fields.Add(FieldUtil.CreateSelectorField(parameterName, binding, values));
            }
            else if (typeof(FileInfo).IsAssignableFrom(type))
            {
                var binding = new VisualNodeParameterBinding<FileInfo?>(node, parameterName);
                fields.Add(FieldUtil.CreateFileSelectorField(parameterName, binding));
            }
        }

        Node = node;

        var fieldStack = new StackLayout(fields.Select(i => new StackLayoutItem(i)).ToArray())
        {
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Padding = new Padding(10),
            Spacing = 8
        };

        Content = fieldStack;
    }
}

public class NodePropertiesWindow : Form
{
    public NodePropertiesWindow(VisualNode node) : base()
    {
        Title = $"Properties for {node.EffectiveName}";
        Content = new NodePropertiesPanel(node);
    }
}