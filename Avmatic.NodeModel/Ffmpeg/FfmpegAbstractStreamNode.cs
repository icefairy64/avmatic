﻿namespace Avmatic.NodeModel.Ffmpeg;

public abstract class FfmpegAbstractStreamNode : SimpleNode
{
    public FfmpegInputStreamSocket InputStream { get; }
    public SimpleNodeSocket<FfmpegStreamInfo> OutputStream { get; }

    protected FfmpegAbstractStreamNode()
    {
        InputStream = AddInput("Input", () => new FfmpegInputStreamSocketBuilder());
        OutputStream = AddOutput("Output", () => new SimpleNodeSocketBuilder<FfmpegStreamInfo>());
    }

    protected override void Update()
    {
        OutputStream.Value = GetOutputStream();
    }
    
    protected abstract FfmpegStreamInfo? GetOutputStream();
}