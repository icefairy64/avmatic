﻿using System.Globalization;

namespace Avmatic.NodeModel.Ffmpeg.Filters;

[Node("LagFun")]
public class FfmpegLagFunFilterNode : FfmpegAbstractFilterNode
{
    public FfmpegFilterInputSocket Input { get; }
    public SimpleNodeSocket<FfmpegFilterOutputInfo> Output { get; }

    private float? FDecay;

    [NodeParameter]
    public float? Decay
    {
        get => FDecay;
        set => SetAndUpdateIfChanged(ref FDecay, value);
    }

    public FfmpegLagFunFilterNode()
    {
        Input = AddInput("Input", () => new FfmpegFilterInputSocketBuilder());
        Output = AddOutput("Output", () => new SimpleNodeSocketBuilder<FfmpegFilterOutputInfo>());
    }

    protected override void Update()
    {
        if (Input.TypedValue is not { } input)
        {
            Output.Value = null;
            return;
        }
        
        var parameters = new Dictionary<string, string>();
        
        if (Decay is {} decay)
            parameters.Add("decay", decay.ToString(CultureInfo.InvariantCulture));

        var filter = new FfmpegFilterInfo("lagfun", parameters, new List<IFfmpegFilterInputInfo> { input });

        Output.Value = new FfmpegFilterOutputInfo(filter, 0, GetOutputPadStreamSpecifier(0));
    }
}