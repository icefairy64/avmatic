﻿using Avmatic.NodeModel;

namespace Avmatic.Model.Nodes;

[Node("External int value")]
public class ExternalIntValueNode : ExternalValueNode
{
    public override Type ValueType => typeof(int);
    
    protected override object? ParseValue(string value)
    {
        if (int.TryParse(value, out var result))
            return result;
        else
            return null;
    }
}