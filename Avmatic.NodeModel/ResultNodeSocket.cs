﻿namespace Avmatic.NodeModel;

public class ResultNodeSocket<T> : NodeSocket
{
    public ResultNodeSocket()
    {
        Type = typeof(Result<T>);
    }

    public override bool MatchType(Type type)
    {
        return typeof(T?).IsAssignableFrom(type) || typeof(Task<T>).IsAssignableFrom(type) || typeof(Task<T?>).IsAssignableFrom(type);
    }

    protected override void OnValueChanged(NodeSocketValueChangedArgs args)
    {
        if (args.Value is T or null)
            base.OnValueChanged(args);

        if (args.Value is Task<T> task)
        {
            task.ContinueWith(t =>
            {
                if (t.IsCompletedSuccessfully)
                {
                    Value = t.Result;
                }
                else if (t.Exception is { } exception)
                {
                    Value = null;
                    Owner.Graph?.MarkException(Owner, exception);
                }
            });
        }
        
        if (args.Value is Task<T?> nullableTask)
        {
            nullableTask.ContinueWith(t =>
            {
                if (t.IsCompletedSuccessfully)
                {
                    Value = t.Result;
                }
                else if (t.Exception is { } exception)
                {
                    Value = null;
                    Owner.Graph?.MarkException(Owner, exception);
                }
            });
        }
    }
}

public abstract class Result<T>
{
    public abstract T? Unwrap();
}

public class Ok<T> : Result<T>
{
    public T Value { get; }

    public Ok(T value)
    {
        Value = value;
    }

    public override T? Unwrap()
    {
        return Value;
    }
}

public class Err<T> : Result<T>
{
    public Exception Exception { get; }

    public Err(Exception exception)
    {
        Exception = exception;
    }

    public override T? Unwrap()
    {
        throw Exception;
    }
}

public class ResultNodeSocketBuilder<T> : NodeSocketBuilder<ResultNodeSocket<T>>
{
    public override ResultNodeSocket<T> Build()
    {
        if (Owner is not { } owner)
            throw new Exception("Owner is not specified");

        if (Name is not { } name)
            throw new Exception("Name is not specified");

        return new ResultNodeSocket<T> { Owner = owner, Name = name };
    }
}