﻿namespace Avmatic.NodeModel;

[Node("File")]
public class FileNode : Node
{
    public SimpleNodeSocket<FileInfo> FileSocket;

    public FileInfo? FFile;

    [NodeParameter]
    public FileInfo? File
    {
        get => FFile;
        set
        {
            if (FFile == value)
                return;
            FFile = value;
            Update();
        }
    }

    public FileNode()
    {
        FileSocket = new SimpleNodeSocket<FileInfo> { Owner = this, Name = "File" };
        FOutputs.Add(FileSocket);
    }

    private void Update()
    {
        FileSocket.Value = File;
    }
}