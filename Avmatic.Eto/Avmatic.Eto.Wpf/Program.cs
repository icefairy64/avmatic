﻿using System;

namespace Avmatic.Eto.Wpf
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Startup.Run();
        }
    }
}