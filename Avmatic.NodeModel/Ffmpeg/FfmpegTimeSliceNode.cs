﻿using System.Text.RegularExpressions;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.NodeModel.Ffmpeg;

[Node("Time slice", "FFmpeg")]
public partial class FfmpegTimeSliceNode : SimpleNode
{
    public SimpleNodeSocket<IReadOnlyList<IParameter>> Parameters { get; }
    public SimpleNodeSocket<TimeSpan> DurationSocket { get; }

    private string? FFrom;

    [NodeParameter]
    public string? From
    {
        get => FFrom;
        set
        {
            if (FFrom == value)
                return;
            FFrom = value;
            Update();
        }
    }
    
    private string? FDuration;

    [NodeParameter]
    public string? Duration
    {
        get => FDuration;
        set
        {
            if (FDuration == value)
                return;
            FDuration = value;
            Update();
        }
    }

    public FfmpegTimeSliceNode()
    {
        Parameters = AddOutput("Parameters", () => new SimpleNodeSocketBuilder<IReadOnlyList<IParameter>>());
        DurationSocket = AddOutput("Duration", () => new SimpleNodeSocketBuilder<TimeSpan>());
    }

    protected override void Update()
    {
        var parameters = new List<IParameter>();

        if (From is {} from)
            parameters.Add(new SeekParameter { Position = from });
        
        if (Duration is {} duration)
            parameters.Add(new DurationParameter { Duration = duration });
        
        Graph?.RunInTransaction(() =>
        {
            Parameters.Value = parameters;
            DurationSocket.Value = ParseDuration(Duration);
        });
    }

    private static TimeSpan? ParseDuration(string? duration)
    {
        if (duration is null)
            return null;
        
        if (TimeSpan.TryParse(duration, out var timeSpan))
            return timeSpan;
        
        if (FfmpegUnitTimeStringRegex.Match(duration) is { } match)
        {
            var groups = match.Groups.Values.Skip(1).Select(g => g.Value).ToList();
            return (groups[0], groups[1]) switch
            {
                (var value, "ms") => TimeSpan.FromMilliseconds(int.Parse(value)),
                (var value, "s") => TimeSpan.FromSeconds(int.Parse(value)),
                (var value, "m") => TimeSpan.FromMinutes(int.Parse(value)),
                (var value, "h") => TimeSpan.FromHours(int.Parse(value)),
                _ => null
            };
        }

        return null;
    }

    private static readonly Regex FfmpegUnitTimeStringRegex = UnitTimeStringRegex();
    
    [GeneratedRegex("^(\\d+)(ms|s|m|h)$")]
    private static partial Regex UnitTimeStringRegex();
}