﻿namespace Avmatic.Model;

public interface IFileJob
{
    public FileInfo OutputFile { get; }
}