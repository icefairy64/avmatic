﻿using System.Diagnostics;
using Avmatic.FfmpegModel;

namespace Avmatic.FfmpegRunner;

public class FfProbeRunner : IDisposable
{
    private readonly FileInfo File;
    private Process Process;
    private readonly FfProbeOutputHandler OutputHandler = new FfProbeOutputHandler();
    private readonly string FfProbePath;

    public FfProbeRunner(FileInfo file, FileInfo? ffProbePath = null)
    {
        File = file;

        FfProbePath = ffProbePath?.FullName ?? "ffprobe";
    }

    public void Dispose()
    {
        Process.Dispose();
    }

    private void OnOutputDataReceived(object sender, DataReceivedEventArgs e)
    {
        if (e.Data is {})
            OutputHandler.HandleLine(e.Data);
    }

    public async Task<InputFile> Run()
    {
        var task = new TaskCompletionSource();
        
        Process = new Process {
            StartInfo = new ProcessStartInfo
            {
                FileName = FfProbePath,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                ArgumentList =
                {
                    "-hide_banner",
                    "-v",
                    "quiet",
                    "-show_streams",
                    "-show_format",
                    File.FullName
                }
            }
        };
        Process.EnableRaisingEvents = true;
        Process.OutputDataReceived += OnOutputDataReceived;
        
        Process.Start();
        
        Process.BeginOutputReadLine();

        OutputHandler.Finished += (sender, args) => task.SetResult();

        await task.Task;

        return OutputHandler.Input;
    }
}