﻿using Avmatic.Model;
using Avmatic.Model.Nodes;
using Avmatic.NodeModel.Ffmpeg;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using LogLevel = NLog.LogLevel;

namespace Avmatic.Terminal;

public static class Startup
{
    private static object? Options;
    
    public static void Run(object options)
    {
        Options = options;

        ConfigureNLog();

        var builder = Host.CreateDefaultBuilder()
            .ConfigureLogging(logging =>
            {
                logging
                    .ClearProviders()
                    .AddNLog();
            })
            .ConfigureServices(ConfigureServices);

        var host = builder.Build();

        FfmpegPaths.FfmpegPath = FfmpegUtils.GetFfToolPath("ffmpeg");
        FfmpegPaths.FfprobePath = FfmpegUtils.GetFfToolPath("ffprobe");

        host.RunAsync();
    }

    private static void ConfigureNLog()
    {
        NLog.LogManager.Setup().LoadConfiguration(builder =>
        {
            builder.ForLogger().FilterMinLevel(LogLevel.Debug).WriteToFile("debug.log");
        });
    }

    private static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
    {
        services.AddHostedService<ApplicationService>()
            .AddScoped<ProgressTabModel>()
            .AddScoped<NodeTabModel>();
        
        if (Options is IServiceVerb verb)
            verb.RegisterHostedService(services);
    }
}