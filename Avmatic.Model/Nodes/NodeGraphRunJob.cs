﻿using Avmatic.Jobs;
using Avmatic.NodeModel;
using Microsoft.Extensions.Logging;

namespace Avmatic.Model.Nodes;

public class NodeGraphRunJob : Job, IFileJob
{
    private readonly NodeGraph Graph;
    private readonly FileInfo InputFile;
    private readonly InputFileNode InputFileNode;
    private readonly FileInfo FOutputFile;
    private readonly ILogger<NodeGraphRunJob> Logger;
    private readonly OutputFileNode OutputFileNode;
    private readonly TaskCompletionSource CompletionSource;

    public FileInfo OutputFile => FOutputFile;

    public NodeGraphRunJob(NodeGraph graph, FileInfo inputFile, FileInfo outputFile, ILogger<NodeGraphRunJob> logger)
    {
        Graph = graph;
        InputFile = inputFile;
        FOutputFile = outputFile;
        Logger = logger;
        
        graph.PhaseChanged += OnGraphPhaseChanged;

        InputFileNode = graph.Nodes.OfType<InputFileNode>().FirstOrDefault() ?? throw new Exception("No input file node");
        OutputFileNode = graph.Nodes.OfType<OutputFileNode>().FirstOrDefault() ?? throw new Exception("No output file node");
        
        var progressConsumerNode = graph.Nodes.OfType<FfmpegProgressConsumerNode>().FirstOrDefault() ?? throw new Exception("No progress consumer node");
        progressConsumerNode.ProgressConsumed += OnProgressConsumed;
        
        CompletionSource = new TaskCompletionSource();
    }

    private void OnGraphPhaseChanged(object? sender, NodeGraphPhaseChangedEventArgs e)
    {
        if (e.Phase is NodeGraphPhase.Idle)
        {
            var nodesWithMissingOutputs = Graph.Nodes
                .Where(n => n.Outputs.Any(s => s.Value is null))
                .Select(n =>
                    $"{n.GetType().Name}: ${string.Join(", ", n.Outputs.Where(s => s.Value is null).Select(s => s.Name))}")
                .ToList();
            if (nodesWithMissingOutputs.Any())
                Logger.LogDebug("The following nodes are missing their outputs: {}", string.Join("; ", nodesWithMissingOutputs));
        }
        
        if (e.Phase is not NodeGraphPhase.Error)
            return;
        
        Logger.LogError("Phase changed to {}", e.Phase);
        foreach (var (node, exception) in Graph.RaisedExceptions)
            Logger.LogError(exception, "Error in node {}", node?.GetType().Name ?? "null");
    }

    private void OnProgressConsumed(object? sender, FfmpegProgressConsumedEventArgs e)
    {
        if (e.Progress.Complete)
            CompletionSource.TrySetResult();
        else
        {
            var progress = e.TotalDuration is null
                ? 0
                : e.Progress.Position / e.TotalDuration;
            
            EmitProgress(new JobProgressArgs
            {
                Job = this,
                Stage = $"Pass {e.Progress.Pass} ({e.Progress.Position} done)",
                StageProgress = progress,
                TotalProgress = 0.5d * (e.Progress.Pass - 1) + (progress / 2)
            });
        }
    }

    public override Task Start()
    {
        Logger.LogDebug("Starting");
        Graph.RunInTransaction(() =>
        {
            Logger.LogDebug("Input file: {}", InputFile);
            InputFileNode.File.Value = InputFile;
            Logger.LogDebug("Output file: {}", OutputFile);
            OutputFileNode.File = OutputFile;
        });
        return CompletionSource.Task;
    }
}