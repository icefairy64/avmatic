﻿using System;
using System.Linq;
using System.Reflection;
using Avmatic.FfmpegModel;
using Avmatic.Model;
using Eto.Forms;

namespace Avmatic.Eto;

public class StreamConfigurationPanel : Panel, IStreamConfigPanel
{
    public IStreamConfiguration StreamConfiguration { get; }

    public StreamConfigurationPanel(IStreamConfiguration streamConfiguration)
    {
        StreamConfiguration = streamConfiguration;
        
        var paramAttributes = streamConfiguration.GetType().GetProperties()
            .Select(p => (Property: p, Attribute: p.GetCustomAttribute(typeof(StreamConfigurationParameterAttribute)) as StreamConfigurationParameterAttribute))
            .Where(t => t.Attribute is not null);

        var stack = new StackLayout
        {
            Orientation = Orientation.Vertical,
            Spacing = 8
        };
        
        Content = stack;

        foreach (var tuple in paramAttributes)
        {
            var attr = tuple.Attribute ?? throw new Exception("No attribute");
            tuple.Property.SetValue(StreamConfiguration, attr.DefaultValue);
            stack.Items.Add(CreateParameterField(tuple.Property, attr));
        }
    }

    private Control CreateParameterField(PropertyInfo property, StreamConfigurationParameterAttribute attribute)
    {
        if (property.PropertyType.IsAssignableFrom(typeof(double)))
            return FieldUtil.CreateNumericField(attribute.FieldName ?? property.Name,
                new ObjectBinding<double>(StreamConfiguration, property.Name));
        
        if (property.PropertyType.IsAssignableFrom(typeof(int)))
            return FieldUtil.CreateNumericField(attribute.FieldName ?? property.Name,
                new ObjectBinding<int>(StreamConfiguration, property.Name));
        
        if (property.PropertyType.IsAssignableFrom(typeof(string)))
            return FieldUtil.CreateTextField(attribute.FieldName ?? property.Name,
                new ObjectBinding<string>(StreamConfiguration, property.Name));
        
        if (property.PropertyType.IsAssignableFrom(typeof(bool)))
            return FieldUtil.CreateCheckboxField(attribute.FieldName ?? property.Name,
                new ObjectBinding<bool>(StreamConfiguration, property.Name));

        throw new Exception($"Unsupported parameter type {property.PropertyType.Name}");
    }
}