﻿namespace Avmatic.NodeModel.Ffmpeg.Filters;

[Node("Test source filter", "FFmpeg/Filters")]
public class FfmpegTestSrcFilterNode : FfmpegAbstractFilterNode
{
    public ResultNodeSocket<TimeSpan> Duration { get; }
    public SimpleNodeSocket<FfmpegFilterOutputInfo> Output { get; }

    public FfmpegTestSrcFilterNode()
    {
        Duration = AddInput("Duration", () => new ResultNodeSocketBuilder<TimeSpan>());
        Output = AddOutput("Output", () => new SimpleNodeSocketBuilder<FfmpegFilterOutputInfo>());
    }

    protected override void OnGraphChanged(NodeGraph? graph)
    {
        base.OnGraphChanged(graph);
        if (graph != null)
            Update();
    }

    protected override void Update()
    {
        var parameters = new Dictionary<string, string>();
        
        if (Duration.Value is TimeSpan duration)
            parameters.Add("duration", $"{duration.TotalMilliseconds}ms");
        
        Output.Value = new FfmpegFilterOutputInfo(
            new FfmpegFilterInfo("testsrc", parameters, new List<IFfmpegFilterInputInfo>()),
            0,
            GetOutputPadStreamSpecifier(0)
        );
    }
}