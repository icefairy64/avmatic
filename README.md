﻿# Avmatic
A small utility to simplify parallel media encoding and encoding parameter experimentation.

## Features
* Build a complex processing pipeline from nodes
* Parametrize a pipeline by exposing arbitrary parameters as nodes
* Specify multiple values for an exposed parameter to run pipeline for a parameter matrix
* Run a pipeline against multiple files (and parameter configurations) over multiple workers in parallel
* Save / load configured pipeline as a file

## Requirements
* .NET 7
* FFmpeg binaries (`ffmpeg`, `ffprobe`)

## How to run
* Go to `Avmatic.Eto/Avmatic.Eto.<PLATFORM>`
* Run `dotnet run` (if you don't have FFmpeg binaries available in `PATH` then provide a path to them using `FFMPEG_PATH` environment variable)

## Graph-based pipeline
* Open Nodes tab
* Add nodes to represent your desired pipeline from context menu and connect them together
* Alternatively, drop a media file onto graph editor to create a basic node setup from it
* Select one or more files to transcode
* Select an output directory
* Start processing - progress will be shown in the Progress tab

## Transcoding (deprecated)
* Open Transcoding tab
* Select one or more files to transcode
* Select an output directory
* For each stream in an example file, configure a template or exclude it

## Matrix encoding (deprecated, parameter experimentation)
* Open Matrix tab
* Select an input file
* Select an output directory
* Configure output file suffix, sample start time and duration
* Configure a matrix of stream parameters (using a comma-separated list for each parameter)

## Supported frontends
* [Eto.Forms](https://github.com/picoe/Eto) (Windows, Linux, Mac)
* [Terminal.Gui](https://github.com/gui-cs/Terminal.Gui) (Windows, Linux, Mac, only for running preconfigured pipelines)

## Supported encoding backends
* [FFmpeg](https://ffmpeg.org/)