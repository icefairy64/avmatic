namespace Avmatic.FfmpegModel.Tests;

public class GenericTests
{
    private VideoInputStream VideoStream11 = new(
        Index: 0,
        CodecName: "libaom-av1",
        CodecType: "video",
        Width: 640,
        Height: 480,
        Framerate: (24000, 1001),
        Tags: new Dictionary<string, string>());
    
    private VideoInputStream VideoStream12 = new(
        Index: 1,
        CodecName: "libaom-av1",
        CodecType: "video",
        Width: 640,
        Height: 400,
        Framerate: (24000, 1001),
        Tags: new Dictionary<string, string>());
    
    private VideoInputStream VideoStream21 = new(
        Index: 0,
        CodecName: "libaom-av1",
        CodecType: "video",
        Width: 600,
        Height: 480,
        Framerate: (24000, 1001),
        Tags: new Dictionary<string, string>());
    
    private AudioInputStream AudioStream11 = new(
        Index: 0,
        CodecName: "libopus",
        CodecType: "audio",
        SampleRate: 48000,
        Tags: new Dictionary<string, string>());
    
    private AudioInputStream AudioStream12 = new(
        Index: 1,
        CodecName: "libopus",
        CodecType: "audio",
        SampleRate: 48000,
        Tags: new Dictionary<string, string>());
    
    private AudioInputStream AudioStream21 = new(
        Index: 0,
        CodecName: "libopus",
        CodecType: "audio",
        SampleRate: 48000,
        Tags: new Dictionary<string, string>());

    private InputFormat Format = new()
    {
        Duration = TimeSpan.FromMinutes(1),
        FormatName = "webm"
    };
    
    [Test]
    public void TestSingleFileSingleStreamMapping()
    {
        var input = new InputFile
        {
            Index = 0,
            Streams = new List<InputStream> { VideoStream11 },
            Format = Format
        };

        var line = new CommandLineBuilder()
            .WithInput(input)
            .UseStream(VideoStream11)
            .BuildCommandLineArguments()
            .Joined(" ");
        
        Assert.That(line, Is.EqualTo("-map 0:v:0"));
    }
    
    [Test]
    public void TestSingleFileSingleAudioSingleVideoStreamMapping()
    {
        var input = new InputFile
        {
            Index = 0,
            Streams = new List<InputStream> { VideoStream11, AudioStream11 },
            Format = Format
        };

        var line = new CommandLineBuilder()
            .WithInput(input)
            .UseStream(VideoStream11)
            .UseStream(AudioStream11)
            .BuildCommandLineArguments()
            .Joined(" ");
        
        Assert.That(line, Is.EqualTo("-map 0:v:0 -map 0:a:0"));
    }
    
    [Test]
    public void TestSingleFileSkipAudioStreamMapping()
    {
        var input = new InputFile
        {
            Index = 0,
            Streams = new List<InputStream> { VideoStream11, AudioStream11 },
            Format = Format
        };

        var line = new CommandLineBuilder()
            .WithInput(input)
            .UseStream(VideoStream11)
            .BuildCommandLineArguments()
            .Joined(" ");
        
        Assert.That(line, Is.EqualTo("-map 0:v:0"));
    }
    
    [Test]
    public void TestSingleFileMultipleAudioSingleVideoStreamMapping()
    {
        var input = new InputFile
        {
            Index = 0,
            Streams = new List<InputStream> { VideoStream11, AudioStream11, AudioStream12 },
            Format = Format
        };

        var line = new CommandLineBuilder()
            .WithInput(input)
            .UseStream(VideoStream11)
            .UseStream(AudioStream11)
            .UseStream(AudioStream12)
            .BuildCommandLineArguments()
            .Joined(" ");
        
        Assert.That(line, Is.EqualTo("-map 0:v:0 -map 0:a:0 -map 0:a:1"));
    }
    
    [Test]
    public void TestMultipleFileSingleAudioSingleVideoStreamMapping()
    {
        var input1 = new InputFile
        {
            Index = 0,
            Streams = new List<InputStream> { VideoStream11, AudioStream11 },
            Format = Format
        };
        
        var input2 = new InputFile
        {
            Index = 1,
            Streams = new List<InputStream> { VideoStream21, AudioStream21 },
            Format = Format
        };

        var line = new CommandLineBuilder()
            .WithInput(input1)
            .WithInput(input2)
            .UseStream(VideoStream11)
            .UseStream(AudioStream21)
            .BuildCommandLineArguments()
            .Joined(" ");
        
        Assert.That(line, Is.EqualTo("-map 0:v:0 -map 1:a:0"));
    }
    
    [Test]
    public void TestMultipleFileMultipleAudioSingleVideoStreamMapping()
    {
        var input1 = new InputFile
        {
            Index = 0,
            Streams = new List<InputStream> { VideoStream11, AudioStream11 },
            Format = Format
        };
        
        var input2 = new InputFile
        {
            Index = 1,
            Streams = new List<InputStream> { VideoStream21, AudioStream21 },
            Format = Format
        };

        var line = new CommandLineBuilder()
            .WithInput(input1)
            .WithInput(input2)
            .UseStream(VideoStream11)
            .UseStream(AudioStream21)
            .UseStream(AudioStream11)
            .BuildCommandLineArguments()
            .Joined(" ");
        
        Assert.That(line, Is.EqualTo("-map 0:v:0 -map 1:a:0 -map 0:a:0"));
    }
}