﻿using Avmatic.FfmpegModel.Parameters;
using Avmatic.FfmpegModel.Parameters.Video;
using Avmatic.FfmpegModel.Parameters.Video.Aom;

namespace Avmatic.NodeModel.Ffmpeg.Encoders;

[Node("VPX-VP9 encoder", "FFmpeg/Encoders")]
public class FfmpegVpxVp9EncoderNode : FfmpegAbstractVideoEncoderNode
{
    private int FCrf = 36;

    [NodeParameter]
    public int Crf
    {
        get => FCrf;
        set => SetAndUpdateIfChanged(ref FCrf, value);
    }
    
    private int FCpuUsed = 4;

    [NodeParameter]
    public int CpuUsed
    {
        get => FCpuUsed;
        set => SetAndUpdateIfChanged(ref FCpuUsed, value);
    }
    
    private int FLagInFrames = 25;

    [NodeParameter]
    public int LagInFrames
    {
        get => FLagInFrames;
        set => SetAndUpdateIfChanged(ref FLagInFrames, value);
    }
    
    protected override FfmpegStreamInfo? GetOutputStream()
    {
        return InputStream.Value switch
        {
            FfmpegStreamInfo info => info with
            {
                Parameters = info.Parameters
                    .Append(new VideoCodecParameter { Codec = VideoCodec.VpxVp9 })
                    .Append(new PixelFormatParameter { Format = PixelFormat })
                    .Append(new CrfParameter { Crf = Crf })
                    .Append(new BitrateParameter { Bitrate = 0, Unit = BitrateUnit.Bit })
                    .Append(new CpuUsedParameter { Usage = CpuUsed })
                    .Append(new LagInFramesParameter { Frames = LagInFrames })
                    .ToList()
            },
            null => null,
            _ => throw new Exception("Unsupported value")
        };
    }
}