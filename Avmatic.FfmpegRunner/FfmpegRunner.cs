﻿using System.Diagnostics;
using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.FfmpegRunner;

public class FfmpegRunner : IDisposable
{
    private readonly FileSystemInfo File;
    public event EventHandler<FfmpegProgressArgs>? Progress; 

    private readonly List<IParameter> Parameters;
    private Process Process;
    private readonly FfmpegOutputHandler OutputHandler = new();
    private readonly AccumulatorOutputHandler ErrorHandler = new();
    private readonly string FfmpegPath;
    private TaskCompletionSource? TaskCompletionSource;

    public FfmpegRunner(IEnumerable<IParameter> parameters, FileSystemInfo file, FileInfo? ffmpegPath = null)
    {
        File = file;
        Parameters = parameters.ToList();

        FfmpegPath = ffmpegPath?.FullName ?? "ffmpeg";

        OutputHandler.Progress += OnProgress;
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
        TaskCompletionSource?.TrySetCanceled();
        OutputHandler.Progress -= OnProgress;
        Process.Dispose();
    }

    private void OnOutputDataReceived(object sender, DataReceivedEventArgs e)
    {
        if (e.Data is {})
            OutputHandler.HandleLine(e.Data);
    }
    
    private void OnErrorDataReceived(object sender, DataReceivedEventArgs e)
    {
        if (e.Data is {} data)
            ErrorHandler.HandleLine(data);
    }
    
    private void OnProgress(object? sender, FfmpegProgressArgs e)
    {
        Progress?.Invoke(this, e);
    }

    public Task Run(CancellationToken? cancellationToken = null)
    {
        TaskCompletionSource = new TaskCompletionSource();
        
        var startInfo = new ProcessStartInfo
        {
            FileName = FfmpegPath,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            CreateNoWindow = true,
            UseShellExecute = false
        };
        
        foreach (var arg in CommandLineBuilder.BuildCommandLineArguments(Parameters))
            startInfo.ArgumentList.Add(arg);

        startInfo.ArgumentList.Add("-hide_banner");
        startInfo.ArgumentList.Add("-v");
        startInfo.ArgumentList.Add("error");
        startInfo.ArgumentList.Add("-y");
        startInfo.ArgumentList.Add("-progress");
        startInfo.ArgumentList.Add("pipe:1");
        
        startInfo.ArgumentList.Add(File.FullName);

        Process = new Process
        {
            StartInfo = startInfo
        };

        var processExited = false;
        
        var ctRegistration = cancellationToken?.Register(() =>
        {
            TaskCompletionSource.TrySetCanceled();
            if (processExited)
                return;
            Process.CancelOutputRead();
            Process.CancelErrorRead();
            Process.Kill(true);
            processExited = true;
        });
        
        Process.EnableRaisingEvents = true;
        Process.OutputDataReceived += OnOutputDataReceived;
        Process.ErrorDataReceived += OnErrorDataReceived;
        Process.Exited += (_, _) =>
        {
            ctRegistration?.Unregister();
            processExited = true;
            if (Process.ExitCode != 0)
                TaskCompletionSource.TrySetException(new FfmpegException(Process.ExitCode, ErrorHandler.Lines));
            else
                TaskCompletionSource.TrySetResult();
        };

        OutputHandler.Progress += (_, args) =>
        {
            if (args.Done)
            {
                TaskCompletionSource.TrySetResult();
                processExited = true;
            }
        };
        
        Process.Start();
        
        Process.BeginOutputReadLine();
        Process.BeginErrorReadLine();

        return TaskCompletionSource.Task;
    }
}