﻿namespace Avmatic.NodeModel;

public abstract class NodeSocketBuilder<T> where T : NodeSocket
{
    protected Node? Owner;
    protected string? Name;
    
    public abstract T Build();

    public NodeSocketBuilder<T> WithOwner(Node node)
    {
        Owner = node;
        return this;
    }
    
    public NodeSocketBuilder<T> WithName(string name)
    {
        Name = name;
        return this;
    }
}