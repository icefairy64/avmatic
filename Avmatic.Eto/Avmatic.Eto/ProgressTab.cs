﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Avmatic.Jobs;
using Avmatic.Model;
using DynamicData;
using DynamicData.Alias;
using Eto.Forms;
using ReactiveUI;

namespace Avmatic.Eto;

public sealed class ProgressTab : Panel
{
    private readonly ProgressTabModel ViewModel;
    
    private readonly CompositeDisposable CompositeDisposable = new();

    public ProgressTab(ProgressTabModel viewModel)
    {
        ViewModel = viewModel;
        
        var progressBarStack = new StackLayout
        {
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Spacing = 8
        };
        ViewModel.JobsInProgress.Connect()
            .BindToStackLayout(progressBarStack, (jvm, pb) => pb.ViewModel == jvm, jvm => new JobProgressBar(jvm));
        
        var pendingStack = new StackLayout
        {
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Spacing = 8
        };
        ViewModel.JobsPending.Connect()
            .BindToStackLayout(pendingStack, (jvm, pb) => pb.ViewModel == jvm, jvm => new JobProgressBar(jvm));

        var jobScrollable = new Scrollable
        {
            Content = new StackLayout
            {
                Orientation = Orientation.Vertical,
                HorizontalContentAlignment = HorizontalAlignment.Stretch,
                Items =
                {
                    progressBarStack,
                    pendingStack
                }
            }
        };

        var itemsLeftLabel = new Label();
        ViewModel.Jobs.Connect()
            .AutoRefresh()
            .Where(j => j.State is JobState.Queued or JobState.Running)
            .Count()
            .Select(c => c == 0 ? "No items left" : $"Items left: {c}")
            .BindToControl(itemsLeftLabel, l => l.Text)
            .DisposeWith(CompositeDisposable);

        var etaLabel = new Label();
        ViewModel.WhenAnyValue(vm => vm.Elapsed)
            .CombineLatest(ViewModel.WhenAnyValue(vm => vm.Eta))
            .DistinctUntilChanged(t => t.Second)
            .Select(t => t switch
            {
                (null, _) or (_, null) => "No ETA",
                (var elapsed, { TotalMilliseconds: 0 }) => $"Finished in {elapsed:g}",
                var (elapsed, eta) => $"ETA: {eta:g}, elapsed: {elapsed:g}"
            })
            .BindToControl(etaLabel, l => l.Text);

        var totalProgressBar = new ProgressBar();
        ViewModel.WhenAnyValue(vm => vm.TotalProgress)
            .Select(p => (int)(p * 100))
            .BindToControl(totalProgressBar, b => b.Value)
            .DisposeWith(CompositeDisposable);

        var cancelAllButton = new Button
        {
            Text = "Cancel all jobs",
            Command = viewModel.CancelAllJobsCommand
        };

        Content = new StackLayout
        {
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Padding = 8,
            Spacing = 8,
            Items =
            {
                new StackLayoutItem
                {
                    Control = jobScrollable,
                    Expand = true
                },
                itemsLeftLabel,
                totalProgressBar,
                etaLabel,
                cancelAllButton
            }
        };
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
            CompositeDisposable.Dispose();

        base.Dispose(disposing);
    }

    protected override void OnUnLoad(EventArgs e)
    {
        ViewModel.Dispose();
        base.OnUnLoad(e);
    }
}

public sealed class JobProgressBar : StackLayout
{
    public readonly JobProgressBarModel ViewModel;

    private readonly CompositeDisposable CompositeDisposable = new();

    public JobProgressBar(JobProgressBarModel viewModel)
    {
        ViewModel = viewModel;

        var progressBar = new ProgressBar();
        ViewModel.WhenAnyValue(vm => vm.Progress)
            .Select(p => (int)(p * 100))
            .BindToControl(progressBar, p => p.Value)
            .DisposeWith(CompositeDisposable);

        var stageLabel = new Label();
        ViewModel.WhenAnyValue(vm => vm.Stage)
            .Select(s => s ?? "Unknown stage")
            .BindToControl(stageLabel, l => l.Text)
            .DisposeWith(CompositeDisposable);

        var filenameLabel = new Label();
        ViewModel.WhenAnyValue(vm => vm.Filename)
            .Select(s => s ?? "No filename")
            .BindToControl(filenameLabel, l => l.Text)
            .DisposeWith(CompositeDisposable);

        Orientation = Orientation.Vertical;
        HorizontalContentAlignment = HorizontalAlignment.Stretch;
        
        Items.Add(progressBar);
        Items.Add(filenameLabel);
        Items.Add(stageLabel);
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
            CompositeDisposable.Dispose();

        base.Dispose(disposing);
    }
}