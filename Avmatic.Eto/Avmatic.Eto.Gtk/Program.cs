﻿using System;
using Microsoft.Extensions.DependencyInjection;
using ReactiveUI;

namespace Avmatic.Eto.Gtk
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Startup.Run(builder => builder.ConfigureServices((_, services) => services.AddSingleton<IApplicationConfirurator>(new ApplicationConfigurator(
                app =>
                {
                    RxApp.MainThreadScheduler = new EtoScheduler(app);
                }))));
        }
    }
}