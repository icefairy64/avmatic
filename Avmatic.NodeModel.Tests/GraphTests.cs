namespace Avmatic.NodeModel.Tests;

public class GraphTests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void TestPhases1()
    {
        var graph = new NodeGraph();
        var source = new MockSourceNode<int>();
        var sink = new MockSinkNode<int>();
        
        graph.Setup(new List<(NodeSocket source, NodeSocket target)>
        {
            (source.Output, sink.Input)
        });
        
        Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Idle));

        source.Output.ValueChanging +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        sink.Input.ValueChanging +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        sink.Input.ValueChanged +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        source.Output.ValueChanged +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Idle));
        
        source.Produce(1);
        
        Assert.That(sink.Input.Value, Is.EqualTo(1));
    }
    
    [Test]
    public void TestPhases2()
    {
        var graph = new NodeGraph();
        var source = new MockSourceNode<int>();
        var passthrough = new MockPassthroughNode<int>();
        var sink = new MockSinkNode<int>();

        graph.Setup(new List<(NodeSocket source, NodeSocket target)>
        {
            (source.Output, passthrough.Input),
            (passthrough.Output, sink.Input)
        });
        
        Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Idle));

        source.Output.ValueChanging +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        passthrough.Input.ValueChanging +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        sink.Input.ValueChanging +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        sink.Input.ValueChanged +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        passthrough.Input.ValueChanging +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        source.Output.ValueChanged +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Idle));
        
        source.Produce(1);
        
        Assert.Multiple(() =>
        {
            Assert.That(passthrough.Input.Value, Is.EqualTo(1));
            Assert.That(sink.Input.Value, Is.EqualTo(1));
            Assert.That(sink.Values, Has.Count.EqualTo(1));
        });
    }
    
    [Test]
    public async Task TestPhasesWithTick()
    {
        var graph = new NodeGraph();
        var source = new MockSourceNode<int?>();
        var passthrough = new MockManualPassthroughNode<int?>();
        var sink = new MockSinkNode<int?>();

        graph.Setup(new List<(NodeSocket source, NodeSocket target)>
        {
            (source.Output, passthrough.Input),
            (passthrough.Output, sink.Input)
        });
        
        Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Idle));

        source.Output.ValueChanging +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        passthrough.Input.ValueChanging +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));

        passthrough.Input.ValueChanging +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        source.Output.ValueChanged +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Idle));
        
        passthrough.Ticking +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Execution));
        
        passthrough.Ticked +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Execution));
        
        sink.Input.ValueChanging +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        sink.Input.ValueChanged +=
            (_, _) => Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Propagation));
        
        source.Produce(1);
        
        Assert.Multiple(() =>
        {
            Assert.That(passthrough.Input.Value, Is.EqualTo(1));
            Assert.That(passthrough.Output.Value, Is.Null);
            Assert.That(sink.Input.Value, Is.Null);
            Assert.That(sink.Values, Is.Empty);
            Assert.That(passthrough.Ticks, Has.Count.EqualTo(1));
            Assert.That(passthrough.Task, Is.Not.Null);
        });

        passthrough.Run();
        
        if (passthrough.Task is {} task)
            await task;
        
        Assert.Multiple(() =>
        {
            Assert.That(sink.Input.Value, Is.EqualTo(1));
            Assert.That(sink.Values, Has.Count.EqualTo(1));
        });
    }
    
    [Test]
    public void TestTickProduceOutputInExecutionPhase()
    {
        var graph = new NodeGraph();
        var source = new MockSourceNode<int?>();
        var passthrough = new MockManualPassthroughNode<int?>();
        var sink = new MockSinkNode<int?>();

        graph.Setup(new List<(NodeSocket source, NodeSocket target)>
        {
            (source.Output, passthrough.Input),
            (passthrough.Output, sink.Input)
        });
        
        passthrough.Ticking +=
            (_, _) => passthrough.Output.Value = 2;

        source.Produce(1);
        
        Assert.Multiple(() =>
        {
            Assert.That(graph.Phase, Is.EqualTo(NodeGraphPhase.Error));
            Assert.That(graph.RaisedExceptions, Has.One.Matches<(Node?, Exception)>(tuple => tuple.Item1 == passthrough && tuple.Item2 is InvalidOperationException));
        });
    }
    
    [Test]
    public async Task TestTickTransaction()
    {
        var graph = new NodeGraph();
        var source = new MockSourceNode<int?>();
        var passthrough = new MockManualPassthroughDoubleNode<int?>();
        var sinkA = new MockSinkNode<int?>();
        var sinkB = new MockSinkNode<int?>();

        graph.Setup(new List<(NodeSocket source, NodeSocket target)>
        {
            (source.Output, passthrough.Input),
            (passthrough.OutputA, sinkA.Input),
            (passthrough.OutputB, sinkB.Input)
        });

        var propagatePhaseCounter = 0;
        graph.PhaseChanged += (_, e) =>
        {
            if (e.Phase == NodeGraphPhase.Propagation)
                propagatePhaseCounter++;
        };
        
        source.Produce(1);
        
        Assert.Multiple(() =>
        {
            Assert.That(propagatePhaseCounter, Is.EqualTo(1));
            Assert.That(passthrough.Ticks, Has.Count.EqualTo(1));
            Assert.That(sinkA.Input.Value, Is.Null);
            Assert.That(sinkB.Input.Value, Is.Null);
        });

        passthrough.Run();
        
        if (passthrough.Task is {} task)
            await task;
        
        Assert.Multiple(() =>
        {
            Assert.That(propagatePhaseCounter, Is.EqualTo(2));
            Assert.That(sinkA.Input.Value, Is.EqualTo(1));
            Assert.That(sinkB.Input.Value, Is.EqualTo(1));
        });
    }
}