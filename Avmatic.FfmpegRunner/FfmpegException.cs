﻿namespace Avmatic.FfmpegRunner;

public class FfmpegException : Exception
{
    public int ExitCode { get; }
    public IReadOnlyList<string> StandardOutputLines { get; }
    
    public FfmpegException(int exitCode, IReadOnlyList<string> standardOutputLines) : base($"Process exited with non-zero code {exitCode}\nStandard error output:\n{string.Join("\n", standardOutputLines)}")
    {
        ExitCode = exitCode;
        StandardOutputLines = standardOutputLines;
    }
}