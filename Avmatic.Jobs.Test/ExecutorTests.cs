﻿using Microsoft.Extensions.Logging.Abstractions;

namespace Avmatic.Jobs.Test;

public class ExecutorTests
{
    [Test]
    public void TestSingleDepthSingleTaskSuccess()
    {
        var executor = new JobExecutor(new NullLoggerFactory()) { JobCapacity = 1 };
        
        var job1 = new MockJob();
        Assert.That(job1.State, Is.EqualTo(JobState.Queued));

        JobStateChangeArgs? stateChangeArgs = null;
        executor.JobStateChanged += (sender, args) => stateChangeArgs = args;
        
        JobFailureArgs? failureArgs = null;
        executor.JobFailed += (sender, args) => failureArgs = args;
        
        executor.Enqueue(job1);
        Thread.Sleep(10);
        Assert.That(job1.State, Is.EqualTo(JobState.Running));
        Assert.That(stateChangeArgs.Job, Is.EqualTo(job1));
        Assert.That(stateChangeArgs.State, Is.EqualTo(JobState.Running));
        
        job1.Complete();
        Thread.Sleep(10);
        Assert.That(stateChangeArgs.Job, Is.EqualTo(job1));
        Assert.That(stateChangeArgs.State, Is.EqualTo(JobState.Complete));
        Assert.That(failureArgs, Is.Null);
    }
    
    [Test]
    public void TestSingleDepthSingleTaskFailure()
    {
        var executor = new JobExecutor(new NullLoggerFactory())  { JobCapacity = 1 };
        
        var job1 = new MockJob();
        Assert.That(job1.State, Is.EqualTo(JobState.Queued));

        JobStateChangeArgs? stateChangeArgs = null;
        executor.JobStateChanged += (sender, args) => stateChangeArgs = args;
        
        JobFailureArgs? failureArgs = null;
        executor.JobFailed += (sender, args) => failureArgs = args;
        
        executor.Enqueue(job1);
        Thread.Sleep(10);
        Assert.That(job1.State, Is.EqualTo(JobState.Running));
        Assert.That(stateChangeArgs.Job, Is.EqualTo(job1));
        Assert.That(stateChangeArgs.State, Is.EqualTo(JobState.Running));
        
        job1.Fail(new Exception("qwe"));
        Thread.Sleep(10);
        Assert.That(stateChangeArgs.Job, Is.EqualTo(job1));
        Assert.That(stateChangeArgs.State, Is.EqualTo(JobState.Failed));
        Assert.That(failureArgs.Job, Is.EqualTo(job1));
        Assert.That(failureArgs.Exception.Message, Does.Contain("qwe"));
    }
    
    [Test]
    public void TestSingleDepthMultipleTasksSuccess()
    {
        var executor = new JobExecutor(new NullLoggerFactory())  { JobCapacity = 1 };
        
        var job1 = new MockJob();
        Assert.That(job1.State, Is.EqualTo(JobState.Queued));
        var job2 = new MockJob();
        Assert.That(job2.State, Is.EqualTo(JobState.Queued));

        List<JobStateChangeArgs> stateChangeArgs = new();
        executor.JobStateChanged += (sender, args) => stateChangeArgs.Add(args);
        
        List<JobFailureArgs> failureArgs = new();
        executor.JobFailed += (sender, args) => failureArgs.Add(args);
        
        executor.Enqueue(job1);
        Thread.Sleep(10);
        Assert.That(job1.State, Is.EqualTo(JobState.Running));
        Assert.That(job2.State, Is.EqualTo(JobState.Queued));
        Assert.That(stateChangeArgs, Has.Count.EqualTo(2));
        Assert.That(stateChangeArgs[0].Job, Is.EqualTo(job1));
        Assert.That(stateChangeArgs[0].State, Is.EqualTo(JobState.Queued));
        Assert.That(stateChangeArgs[1].Job, Is.EqualTo(job1));
        Assert.That(stateChangeArgs[1].State, Is.EqualTo(JobState.Running));
        Assert.That(failureArgs, Is.Empty);
        
        stateChangeArgs.Clear();
        
        executor.Enqueue(job2);
        Thread.Sleep(10);
        Assert.That(job1.State, Is.EqualTo(JobState.Running));
        Assert.That(job2.State, Is.EqualTo(JobState.Queued));
        Assert.That(stateChangeArgs, Has.Count.EqualTo(1));
        Assert.That(stateChangeArgs[0].Job, Is.EqualTo(job2));
        Assert.That(stateChangeArgs[0].State, Is.EqualTo(JobState.Queued));
        Assert.That(failureArgs, Is.Empty);
        
        stateChangeArgs.Clear();
        
        job1.Complete();
        Thread.Sleep(10);
        Assert.That(stateChangeArgs, Has.Count.EqualTo(2));
        Assert.That(stateChangeArgs[0].Job, Is.EqualTo(job1));
        Assert.That(stateChangeArgs[0].State, Is.EqualTo(JobState.Complete));
        Assert.That(stateChangeArgs[1].Job, Is.EqualTo(job2));
        Assert.That(stateChangeArgs[1].State, Is.EqualTo(JobState.Running));
        Assert.That(failureArgs, Is.Empty);
        
        stateChangeArgs.Clear();
        
        job2.Complete();
        Thread.Sleep(10);
        Assert.That(stateChangeArgs, Has.Count.EqualTo(1));
        Assert.That(stateChangeArgs[0].Job, Is.EqualTo(job2));
        Assert.That(stateChangeArgs[0].State, Is.EqualTo(JobState.Complete));
        Assert.That(failureArgs, Is.Empty);
    }
}

internal class MockJob : Job
{
    private TaskCompletionSource CompletionSource = new();
    private bool Running = false;

    public override Task Start()
    {
        Running = true;
        return CompletionSource.Task;
    }

    public new void EmitProgress(JobProgressArgs args)
    {
        base.EmitProgress(args);
    }

    public void Complete()
    {
        CompletionSource.SetResult();
    }

    public void Fail(Exception e)
    {
        CompletionSource.SetException(e);
    }

    public JobState State =>
        CompletionSource.Task switch
        {
            { IsCompletedSuccessfully: true } => JobState.Complete,
            { IsCanceled: true } => JobState.Complete,
            { IsFaulted: true } => JobState.Failed,
            _ => Running ? JobState.Running : JobState.Queued
        };
}