﻿using Avmatic.NodeModel.Serialization;

namespace Avmatic.Model.Nodes;

public record VisualNodeDto(int NodeIndex, int X, int Y, int Width, int Height, string Name);

public record VisualNodeGraphDto(NodeGraphDto Graph, IList<VisualNodeDto> VisualNodes, IDictionary<string, string>? ExternalValues);