﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.NodeModel.Ffmpeg;

public class StreamIndexUnresolvedParameter : IParameter
{
    public string Name => "unused";
    public string Value => "unused";
    
    public required IStreamSpecifier StreamSpecifier { get; init; }
    public required IParameter Parameter { get; init; }
}