﻿namespace Avmatic.FfmpegModel;

public record VideoInputStream(
    int Index,
    string CodecName,
    string CodecType,
    IDictionary<string, string> Tags,
    (long Numerator, long Denominator) Framerate,
    int Width,
    int Height) : InputStream(Index: Index, CodecName: CodecName, CodecType: CodecType, Tags: Tags);