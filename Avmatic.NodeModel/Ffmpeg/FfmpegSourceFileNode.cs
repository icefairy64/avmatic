﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegRunner;

namespace Avmatic.NodeModel.Ffmpeg;

[Node("Source file", "FFmpeg")]
public class FfmpegSourceFileNode : SimpleNode
{
    public ResultNodeSocket<FileInfo> Filename { get; }
    public SimpleNodeSocket<InputFile> File { get; }
    public SimpleNodeSocket<TimeSpan> Duration { get; }

    private int FFileIndex;

    [NodeParameter]
    public int FileIndex
    {
        get => FFileIndex;
        set => SetAndUpdateIfChanged(ref FFileIndex, value);
    }

    public FfmpegSourceFileNode()
    {
        Filename = AddInput("Filename", () => new ResultNodeSocketBuilder<FileInfo>());
        File = AddOutput("File", () => new SimpleNodeSocketBuilder<InputFile>());
        Duration = AddOutput("Duration", () => new SimpleNodeSocketBuilder<TimeSpan>());
    }

    protected override void Update()
    {
        if (Filename.Value is not FileInfo file)
        {
            Graph?.RunInTransaction(() =>
            {
                File.Value = null;
                Duration.Value = null; 
            });
            return;
        }

        var runner = new FfProbeRunner(file, FfmpegPaths.FfprobePath);

        runner.Run().ContinueWith(t =>
        {
            if (t is { IsFaulted: true, Exception: { } exception })
            {
                Graph?.MarkException(this, exception);
                File.Value = null;
                Duration.Value = null;
                return;
            }

            var value = t.Result;
            value.Index = FileIndex;

            Graph?.RunInTransaction(() =>
            {
                File.Value = value;
                Duration.Value = value.Format.Duration; 
            });
        });
    }
}