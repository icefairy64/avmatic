﻿using Avmatic.FfmpegModel.Parameters;
using Avmatic.FfmpegModel.Parameters.Video;
using Avmatic.FfmpegModel.Parameters.Video.Aom;

namespace Avmatic.NodeModel.Ffmpeg.Encoders;

[Node("AOM AV1 encoder", "FFmpeg/Encoders")]
public class FfmpegAomAv1EncoderNode : FfmpegAbstractVideoEncoderNode
{
    private int FCrf = 36;

    [NodeParameter]
    public int Crf
    {
        get => FCrf;
        set => SetAndUpdateIfChanged(ref FCrf, value);
    }
    
    private int FCpuUsed = 6;

    [NodeParameter]
    public int CpuUsed
    {
        get => FCpuUsed;
        set => SetAndUpdateIfChanged(ref FCpuUsed, value);
    }
    
    private int FGrainLevel = 8;

    [NodeParameter]
    public int GrainLevel
    {
        get => FGrainLevel;
        set => SetAndUpdateIfChanged(ref FGrainLevel, value);
    }
    
    private int FLagInFrames = 48;

    [NodeParameter]
    public int LagInFrames
    {
        get => FLagInFrames;
        set => SetAndUpdateIfChanged(ref FLagInFrames, value);
    }
    
    private bool FEnableDnlDenoising = true;

    [NodeParameter]
    public bool EnableDnlDenoising
    {
        get => FEnableDnlDenoising;
        set => SetAndUpdateIfChanged(ref FEnableDnlDenoising, value);
    }
    
    private string? FTune;

    [NodeParameter]
    public string? Tune
    {
        get => FTune;
        set => SetAndUpdateIfChanged(ref FTune, value);
    }

    protected override FfmpegStreamInfo? GetOutputStream()
    {
        return InputStream.Value switch
        {
            FfmpegStreamInfo info => info with
            {
                Parameters = info.Parameters
                    .Concat(Parameters)
                    .ToList()
            },
            null => null,
            _ => throw new Exception("Unsupported value")
        };
    }
    
    private IEnumerable<IParameter> Parameters
    {
        get
        {
            yield return new VideoCodecParameter { Codec = VideoCodec.AomAv1 };
            yield return new PixelFormatParameter { Format = PixelFormat };
            yield return new CrfParameter { Crf = Crf };
            yield return new BitrateParameter { Bitrate = 0, Unit = BitrateUnit.Bit };
            yield return new CpuUsedParameter { Usage = CpuUsed };
            yield return new LagInFramesParameter { Frames = LagInFrames };
            yield return new AomGrainLevelParameter { GrainLevel = GrainLevel };
            yield return new AomExtraParameter
            {
                EnableDnlDenoising = EnableDnlDenoising ? 1 : 0,
                Tune = Tune
            };
        }
    }
}