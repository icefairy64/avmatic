﻿namespace Avmatic.FfmpegModel.Parameters.Video;

public class VideoCodecParameter : IParameter
{
    public string Name => "c";
    
    public required VideoCodec Codec { get; set; }

    public string Value => Codec switch
    {
        VideoCodec.X264 => "libx264",
        VideoCodec.X265 => "libx265",
        VideoCodec.VpxVp9 => "libvpx-vp9",
        VideoCodec.AomAv1 => "libaom-av1",
        VideoCodec.SvtAv1 => "libsvtav1",
        VideoCodec.Copy => "copy",
        _ => throw new ArgumentOutOfRangeException()
    };
}

public enum VideoCodec
{
    X264,
    X265,
    VpxVp9,
    AomAv1,
    SvtAv1,
    Copy
}