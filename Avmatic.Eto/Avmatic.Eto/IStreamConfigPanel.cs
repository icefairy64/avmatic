﻿using Avmatic.Model;

namespace Avmatic.Eto;

public interface IStreamConfigPanel
{
    public IStreamConfiguration StreamConfiguration { get; }
}