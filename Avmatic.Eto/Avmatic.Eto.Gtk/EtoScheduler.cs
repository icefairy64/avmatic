﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using Application = Eto.Forms.Application;
using Task = System.Threading.Tasks.Task;

namespace Avmatic.Eto.Gtk;

public class EtoScheduler : IScheduler
{
    private readonly Application EtoApplication;

    public EtoScheduler(Application etoApplication)
    {
        EtoApplication = etoApplication;
    }

    public IDisposable Schedule<TState>(TState state, Func<IScheduler, TState, IDisposable> action)
    {
        var disposable = new SingleAssignmentDisposable();
        EtoApplication.InvokeAsync(() =>
        {
            if (disposable.IsDisposed)
                return;
            disposable.Disposable = action(this, state);
        });
        return disposable;
    }

    public IDisposable Schedule<TState>(TState state, TimeSpan dueTime, Func<IScheduler, TState, IDisposable> action)
    {
        var disposable = new SingleAssignmentDisposable();
        Task.Run(async () =>
        {
            await Task.Delay(Scheduler.Normalize(dueTime));
            await EtoApplication.InvokeAsync(() =>
            {
                if (disposable.IsDisposed)
                    return;
                disposable.Disposable = action(this, state);
            });
        });
        return disposable;
    }

    public IDisposable Schedule<TState>(TState state, DateTimeOffset dueTime, Func<IScheduler, TState, IDisposable> action)
    {
        return Schedule(state, dueTime - Now, action);
    }

    public DateTimeOffset Now => EtoApplication.Invoke(() => DateTimeOffset.Now);
}