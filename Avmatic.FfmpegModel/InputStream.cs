﻿namespace Avmatic.FfmpegModel;

public record InputStream(int Index, string CodecName, string CodecType, IDictionary<string, string> Tags);