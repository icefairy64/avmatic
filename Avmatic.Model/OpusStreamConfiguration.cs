﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;
using Avmatic.FfmpegModel.Parameters.Audio;

namespace Avmatic.Model;

[StreamConfiguration("opus", StreamType.Audio)]
public class OpusStreamConfiguration : IStreamConfiguration
{
    public InputStream? InputStream { get; set; }
    public int StreamIndex { get; set; }
    public required string CodecType { get; set; }

    private int BitrateValue;
    private BitrateUnit BitrateUnit;

    private string FBitrate = "";
    
    [StreamConfigurationParameter("Bitrate", "112k")]
    public required string Bitrate
    {
        get => FBitrate;
        set
        {
            FBitrate = value;
            BitrateUnit = value.ToLowerInvariant().LastOrDefault('-') switch
            {
                'k' => BitrateUnit.Kilobit,
                'm' => BitrateUnit.Megabit,
                'g' => BitrateUnit.Gigabit,
                _ => BitrateUnit.Bit
            };
            if (int.TryParse(BitrateUnit == BitrateUnit.Bit ? value : value[..^1], out var bitrate))
                BitrateValue = bitrate;
        }
    }

    public IList<IParameter> Parameters
    {
        get
        {
            var result = new List<IParameter>
            {
                new AudioCodecParameter { Codec = AudioCodec.Opus },
                new BitrateParameter { Bitrate = BitrateValue, Unit = BitrateUnit }
            };

            return result;
        }
    }
    
    public IStreamConfiguration Clone()
    {
        return new OpusStreamConfiguration
        {
            InputStream = InputStream,
            StreamIndex = StreamIndex,
            CodecType = CodecType,
            Bitrate = Bitrate
        };
    }
}