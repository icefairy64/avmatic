﻿using Avmatic.FfmpegModel;

namespace Avmatic.NodeModel.Ffmpeg.Filters;

public class FilterOutputPadStreamSpecifier : IStreamSpecifier
{
    public required string PadName { get; init; }
    public string String => $"[{PadName}]";

    protected bool Equals(FilterOutputPadStreamSpecifier other)
    {
        return PadName == other.PadName;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((FilterOutputPadStreamSpecifier)obj);
    }

    public override int GetHashCode()
    {
        return PadName.GetHashCode();
    }
}