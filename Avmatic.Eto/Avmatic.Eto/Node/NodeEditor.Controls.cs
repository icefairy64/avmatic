﻿using System;
using System.Collections.Generic;
using System.Linq;
using Avmatic.NodeModel;
using DynamicData;
using Eto.Drawing;
using Eto.Forms;

namespace Avmatic.Eto.Node;

public partial class NodeEditor
{
    private HashSet<VisualNode> SelectedNodes = new();

    private VisualNode? SourceNode;
    private NodeSocket? SourceSocket;
    private int? SourceSocketIndex;

    private VisualNode? TargetNode;
    private NodeSocket? TargetSocket;
    private int? TargetSocketIndex;

    private NodeSocketConnection? MovingConnection;

    private IEnumerable<(NodeSocket, PointF)> GetNodeSocketLocations(VisualNode node)
    {
        var socketOffset = 48;
        var result = new List<(NodeSocket, PointF)>();
        
        foreach (var input in node.Node.Inputs)
        {
            result.Add((input, new PointF(node.X, node.Y + socketOffset)));
            socketOffset += 24;
        }

        socketOffset = 48;
        
        foreach (var output in node.Node.Outputs)
        {
            result.Add((output, new PointF(node.X + node.Width, node.Y + socketOffset)));
            socketOffset += 24;
        }

        return result;
    }

    private VisualNode? GetNodeAtScreenLocation(float x, float y)
    {
        var localPoint = ScreenToLocal(x, y);
        return Nodes.Find(n => localPoint.X >= n.X && localPoint.X <= n.X + n.Width && localPoint.Y >= n.Y && localPoint.Y <= n.Y + n.Height);
    }

    private (VisualNode, NodeSocket)? GetSocketAtScreenLocation(float x, float y)
    {
        var localPoint = ScreenToLocal(x, y);
        
        foreach (var node in Nodes)
        {
            foreach (var (socket, location) in GetNodeSocketLocations(node))
            {
                if (localPoint.Distance(location) <= 12)
                    return (node, socket);
            }
        }

        return null;
    }

    private void OnNodeMouseDown(MouseEventArgs e, VisualNode node)
    {
        if (!e.Modifiers.HasFlag(Keys.Control) && !SelectedNodes.Contains(node))
            SelectedNodes.Clear();
        SelectedNodes.Add(node);
        
        MousePreviousPosition = e.Location;
        
        var bottomRightCorner = new PointF(node.X + node.Width, node.Y + node.Height);
        var curLocalPos = ScreenToLocal(e.Location.X, e.Location.Y);
        if (curLocalPos.Distance(bottomRightCorner) <= 16 / CameraZoom)
        {
            OnNodeResizeMouseDown(e, node);
            return;
        }
        
        State = NodeEditorState.MovingNode;
        Invalidate();
    }

    private void OnNodeResizeMouseDown(MouseEventArgs e, VisualNode node)
    {
        State = NodeEditorState.ResizingNode;
    }

    private void OnNodeMove(MouseEventArgs e)
    {
        var prevLocalPos = ScreenToLocal(MousePreviousPosition.X, MousePreviousPosition.Y);
        var curLocalPos = ScreenToLocal(e.Location.X, e.Location.Y);
        var dx = curLocalPos.X - prevLocalPos.X;
        var dy = curLocalPos.Y - prevLocalPos.Y;
        if (State is NodeEditorState.MovingNode)
        {
            foreach (var node in SelectedNodes)
            {
                node.X += dx;
                node.Y += dy;
            }
        }
        else if (State is NodeEditorState.ResizingNode)
        {
            foreach (var node in SelectedNodes)
            {
                node.Width += dx;
                node.Height += dy;
            }
        }
        else
        {
            throw new InvalidOperationException($"Could not handle node mouse movement in state {State}");
        }
        MousePreviousPosition = e.Location;
        Invalidate();
    }

    private void OnSocketMouseDown(MouseEventArgs e, NodeSocket socket, VisualNode node)
    {
        if (socket.Owner.Inputs.Contains(socket))
            OnInputSocketMouseDown(e, socket, node);
        else
            OnOutputSocketMouseDown(e, socket, node);
    }
    
    private void OnInputSocketMouseDown(MouseEventArgs e, NodeSocket socket, VisualNode node)
    {
        if (Graph.Connections.FirstOrDefault(c => c.Target == socket) is not { } connection)
            return;
        
        State = NodeEditorState.Disconnecting;
        MovingConnection = connection;
        SourceNode = Nodes.Find(n => n.Node.Outputs.Contains(connection.Source));
        SourceSocket = connection.Source;
        SourceSocketIndex = SourceNode?.Node.Outputs.IndexOf(connection.Source);
        Invalidate();
    }
    
    private void OnOutputSocketMouseDown(MouseEventArgs e, NodeSocket socket, VisualNode node)
    {
        State = NodeEditorState.CreatingConnection;
        SourceNode = node;
        SourceSocket = socket;
        SourceSocketIndex = socket.Owner.Outputs.IndexOf(socket);
        Invalidate();
    }

    private void OnCreatingConnectionMouseMove(MouseEventArgs e)
    {
        if (GetSocketAtScreenLocation(e.Location.X, e.Location.Y) is { Item1: { } targetNode, Item2: { } targetSocket }
            && targetNode.Node.Inputs.Contains(targetSocket)
            && SourceSocket is {} sourceSocket
            && targetSocket.MatchType(sourceSocket.Type)
            && !Graph.Connections.Any(c => c.Source == sourceSocket && c.Target == targetSocket))
        {
            TargetNode = targetNode;
            TargetSocket = targetSocket;
            TargetSocketIndex = targetNode.Node.Inputs.IndexOf(targetSocket);
        }
        else
        {
            TargetNode = null;
            TargetSocket = null;
            TargetSocketIndex = null;
        }
        
        MousePreviousPosition = e.Location;
        Invalidate();
    }

    private void OnCreatingConnectionMouseUp(MouseEventArgs e)
    {
        if (SourceNode is not {} sourceNode || SourceSocket is not {} sourceSocket)
            return;

        if (TargetNode is { } targetNode && TargetSocket is { } targetSocket)
            Graph.Connect(sourceSocket, targetSocket);

        SourceNode = null;
        SourceSocket = null;
        SourceSocketIndex = null;
        TargetNode = null;
        TargetSocket = null;
        TargetSocketIndex = null;
        
        Invalidate();
    }

    private void OnDisconnectingMouseUp(MouseEventArgs e)
    {
        if (SourceNode is not {} sourceNode || SourceSocket is not {} sourceSocket || MovingConnection is not {} connection)
            return;
        
        Graph.Disconnect(connection);

        if (TargetNode is { } targetNode && TargetSocket is { } targetSocket)
            Graph.Connect(sourceSocket, targetSocket);

        SourceNode = null;
        SourceSocket = null;
        SourceSocketIndex = null;
        TargetNode = null;
        TargetSocket = null;
        TargetSocketIndex = null;
        MovingConnection = null;
        
        Invalidate();
    }

    private void OnNodeAlternateMouseDown(MouseEventArgs e, VisualNode node)
    {
        NodeContextMenuRequested?.Invoke(this, new NodeContextMenuEventArgs { MouseEventArgs = e, Node = node });
    }

    private void OnFieldAlternateMouseDown(MouseEventArgs e)
    {
        FieldContextMenuRequested?.Invoke(this, e);
    }
}