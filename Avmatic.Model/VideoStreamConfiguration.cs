﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;
using Avmatic.FfmpegModel.Parameters.Video;

namespace Avmatic.Model;

public abstract class VideoStreamConfiguration : IStreamConfiguration
{
    public InputStream? InputStream { get; set; }
    public int StreamIndex { get; set; }
    public required string CodecType { get; set; }

    public PixelFormat? PixelFormat { get; set; } = FfmpegModel.Parameters.Video.PixelFormat.Yuv420P10Le;

    private IScaleDescription? FScale;
    
    [StreamConfigurationParameter("Resolution", "")]
    public string? Resolution
    {
        get => FScale is ShortSideScaleDescription scale ? scale.SideLength.ToString() : "";
        set => FScale = value is { Length: > 0 }
            ? new ShortSideScaleDescription { SideLength = int.Parse(value) }
            : null;
    }

    [StreamConfigurationParameter("Keyframe interval", 9999)]
    public int? KeyframeInterval { get; set; }

    public virtual IList<IParameter> Parameters
    {
        get
        {
            var result = new List<IParameter>();
            
            if (PixelFormat is {} pixelFormat)
                result.Add(new PixelFormatParameter { Format = pixelFormat });
            
            if (FScale is {} scale)
                result.Add(scale.Parameter);
            
            if (KeyframeInterval is {} kf)
                result.Add(new KeyframeIntervalParameter { Interval = kf });

            return result;
        }
    }

    public abstract IStreamConfiguration Clone();
}