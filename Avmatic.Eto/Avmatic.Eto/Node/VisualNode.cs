﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Reflection;
using Avmatic.NodeModel;
using Eto.Forms;

namespace Avmatic.Eto.Node;

public class VisualNode
{
    public NodeModel.Node Node { get; }
    
    public string? Name { get; set; }
    
    public float X { get; set; }
    public float Y { get; set; }
    
    public float Width { get; set; }
    public float Height { get; set; }

    public string EffectiveName => Name ?? Node.GetType().Name;

    private readonly List<string> FParameters;
    public IEnumerable<string> Parameters => FParameters;

    public IEnumerable<string> SocketizableParameters => Parameters.Except(Node.SocketizedParameters);
    public IEnumerable<string> UnsocketizableParameters => Node.SocketizedParameters;

    public VisualNode(NodeModel.Node node)
    {
        Node = node;
        FParameters = CollectParameterProperties(node.GetType())
            .Select(p => p.Name)
            .ToList();
    }

    public Type? GetParameterType(string parameterName)
    {
        return Node.GetType().GetProperty(parameterName)?.PropertyType;
    }

    public object? GetParameterValue(string parameterName)
    {
        return Node.GetType().GetProperty(parameterName)?.GetValue(Node);
    }
    
    public void SetParameterValue(string parameterName, object? value)
    {
        Node.GetType().GetProperty(parameterName)?.SetValue(Node, value);
    }

    private static IEnumerable<PropertyInfo> CollectParameterProperties(Type nodeType)
    {
        return nodeType.GetProperties().Where(p => p.GetCustomAttribute<NodeParameterAttribute>() is not null);
    }
}

public class VisualNodeParameterBinding<T> : DirectBinding<T>
{
    private readonly VisualNode Node;
    private readonly string ParameterName;

    public VisualNodeParameterBinding(VisualNode node, string parameterName)
    {
        Node = node;
        ParameterName = parameterName;
    }

    public override T DataValue
    {
        get => (T)Node.GetParameterValue(ParameterName);
        set => Node.SetParameterValue(ParameterName, value);
    }
}

public class VisualNodeNullableNumericParameterBinding<T> : DirectBinding<string> where T : INumber<T>
{
    private readonly VisualNode Node;
    private readonly string ParameterName;

    public VisualNodeNullableNumericParameterBinding(VisualNode node, string parameterName)
    {
        Node = node;
        ParameterName = parameterName;
    }

    public override string DataValue
    {
        get => Node.GetParameterValue(ParameterName)?.ToString() ?? "";
        set
        {
            if (T.TryParse(value, null, out var result))
                Node.SetParameterValue(ParameterName, result);
            else
                Node.SetParameterValue(ParameterName, null);
        }
    }
}

public class VisualNodeEnumParameterBinding : DirectBinding<string>
{
    private readonly VisualNode Node;
    private readonly string ParameterName;
    private readonly Type EnumType;

    public VisualNodeEnumParameterBinding(VisualNode node, string parameterName)
    {
        Node = node;
        ParameterName = parameterName;
        EnumType = node.GetParameterType(parameterName) ?? throw new Exception();
        if (EnumType.IsGenericType && EnumType.GetGenericTypeDefinition() == typeof(Nullable<>))
            EnumType = Nullable.GetUnderlyingType(EnumType) ?? throw new Exception();
    }

    public override string DataValue
    {
        get => Node.GetParameterValue(ParameterName)?.ToString() ?? "";
        set
        {
            if (value == "")
                Node.SetParameterValue(ParameterName, null);
            else if (Enum.TryParse(EnumType, value, out var result))
                Node.SetParameterValue(ParameterName, result);
            else
                Node.SetParameterValue(ParameterName, null);
        }
    }
}