﻿using System;
using System.Linq.Expressions;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reflection;
using Eto.Forms;
using ReactiveUI;

namespace Avmatic.Eto;

public static class ReactiveControlExtensions
{
    public static IDisposable BindToObservable<TO, TV>(this BindableBinding<TO, TV> binding, IObservable<TV> observable, Action<TV> onNext) where TO : IBindable
    {
        var disposable = new CompositeDisposable();

        Observable.FromEventPattern<EventArgs>(d => binding.DataValueChanged += d, d => binding.DataValueChanged -= d)
            .ObserveOn(RxApp.MainThreadScheduler)
            .Subscribe(pattern => onNext(binding.DataValue))
            .DisposeWith(disposable);
        
        observable.ObserveOn(RxApp.MainThreadScheduler)
            .Subscribe(v => binding.DataValue = v)
            .DisposeWith(disposable);
        
        return disposable;
    }
    
    public static IDisposable BindToObservable<TO, TV>(this TO obj, Expression<Func<TO, TV>> expression, IObservable<TV> observable) where TO : Control
    {
        if (expression is not LambdaExpression { Body: MemberExpression memberExpression })
            throw new Exception("Not a member expression");
        
        if (memberExpression.Member is not PropertyInfo property)
            throw new Exception("Not a property");

        return observable.ObserveOn(RxApp.MainThreadScheduler).Subscribe(v => property.SetValue(obj, v));
    }
    
    public static IDisposable BindToControl<TO, TV>(this IObservable<TV> observable, TO obj, Expression<Func<TO, TV>> expression) where TO : Control
    {
        return obj.BindToObservable(expression, observable);
    }

    public static IDisposable BindToAction<TV>(this IObservable<TV> observable, Action<TV> action)
    {
        return observable.ObserveOn(RxApp.MainThreadScheduler).Subscribe(action);
    }
}