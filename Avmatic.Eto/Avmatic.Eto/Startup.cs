﻿using System;
using System.Threading;
using Avmatic.NodeModel.Ffmpeg;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Avmatic.Eto;

public static class Startup
{
    public static void Run(Action<IHostBuilder>? hostConfigurator = null)
    {
        var builder = Host.CreateDefaultBuilder()
            .ConfigureLogging(logging => 
                logging.AddConsole(options => options.LogToStandardErrorThreshold = LogLevel.Information)
                    .AddDebug())
            .ConfigureServices((_, services) =>
                services.AddHostedService<Application>());
        
        hostConfigurator?.Invoke(builder);

        var host = builder.Build();

        FfmpegPaths.FfmpegPath = FfmpegUtils.GetFfToolPath("ffmpeg");
        FfmpegPaths.FfprobePath = FfmpegUtils.GetFfToolPath("ffprobe");
        
        var task = host.RunAsync();
        Application.Instance.Run();
    }
}