﻿namespace Avmatic.FfmpegModel.Parameters.Video.Aom;

public class LagInFramesParameter : IParameter
{
    public string Name => "lag-in-frames";
    
    public required int Frames { get; set; }

    public string Value => Frames.ToString();
}