﻿namespace Avmatic.NodeModel;

public interface ITickableNode
{
    public bool Tick();
}