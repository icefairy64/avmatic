﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using Avmatic.Eto.Node;
using Avmatic.Model.Nodes;
using Avmatic.NodeModel;
using DynamicData.Binding;
using Eto.Forms;
using ReactiveUI;

namespace Avmatic.Eto;

public class NodeTab : Panel
{
    private readonly NodeEditor Editor;
    private readonly NodeTabModel ViewModel;

    private readonly CompositeDisposable Disposable = new();
    
    public NodeTab(NodeTabModel viewModel)
    {
        ViewModel = viewModel;
        Editor = new NodeEditor(viewModel.Graph);

        Editor.FieldContextMenuRequested += OnFieldContextMenuRequested;
        Editor.NodeContextMenuRequested += OnNodeContextMenuRequested;
        Editor.FileDropped += OnEditorFileDropped;

        ViewModel.AddFilesInteraction.RegisterHandler(OpenAddFilesDialog)
            .DisposeWith(Disposable);
        
        ViewModel.SelectOutputDirectoryInteraction.RegisterHandler(SelectOutputDirectory)
            .DisposeWith(Disposable);

        var fileAddButton = new Button
        {
            Command = ViewModel.AddFilesCommand, 
            Text = "Add files"
        };
        
        var outputDirectorySelectButton = new Button
        {
            Command = ViewModel.SelectOutputDirectoryCommand, 
            Text = "Select output directory"
        };
        
        var startButton = new Button
        {
            Command = ViewModel.StartCommand, 
            Text = "Start"
        };

        var outputDirectoryLabel = new Label();
        ViewModel.WhenAnyValue(vm => vm.OutputDirectory)
            .Select(d => d?.FullName ?? "No directory selected")
            .BindToControl(outputDirectoryLabel, l => l.Text)
            .DisposeWith(Disposable);

        var capacityField = FieldUtil.CreateNumericField("Job capacity", viewModel, vm => vm.JobCapacity, Disposable);

        var externalValuesStack = new StackLayout
        {
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Padding = 10,
            Spacing = 8
        };

        viewModel.ExternalValues.ToObservableChangeSet()
            .BindToStackLayout(externalValuesStack,
                (fieldVm, control) => control.ViewModel == fieldVm,
                fieldVm => new ReactiveTextField(fieldVm))
            .DisposeWith(Disposable);

        var contentStack = new StackLayout
        {
            Orientation = Orientation.Horizontal,
            VerticalContentAlignment = VerticalAlignment.Stretch,
            Spacing = 8,
            Items =
            {
                new StackLayoutItem(Editor, true)
            }
        };

        viewModel.ExternalValues.ToObservableChangeSet()
            .Select((_) => viewModel.ExternalValues.Count)
            .BindToAction(count =>
            {
                var externalValuesItem = contentStack.Items.FirstOrDefault(i => i.Control == externalValuesStack);
                if (count == 0 && externalValuesItem is not null)
                    contentStack.Items.Remove(externalValuesItem);
                else if (count > 0 && externalValuesItem is null)
                    contentStack.Items.Add(new StackLayoutItem { Control = externalValuesStack });
            })
            .DisposeWith(Disposable);

        Content = new StackLayout
        {
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Padding = 10,
            Spacing = 8,
            Items =
            {
                fileAddButton,
                outputDirectorySelectButton,
                outputDirectoryLabel,
                capacityField,
                startButton,
                new StackLayoutItem(contentStack, true)
            }
        };
    }

    private void OnEditorFileDropped(object? sender, NodeEditorFileDropEventArgs e)
    {
        if (e.File.Extension?.ToLower() == ".json")
            ImportFromFile(e.File);
        else
            Editor.LoadFromExample(e.File);
    }

    private void OpenAddFilesDialog(IInteractionContext<Unit, IEnumerable<FileInfo>?> context)
    {
        using var dialog = new OpenFileDialog
        {
            Filters = { new FileFilter("Video files", ".mp4", ".mkv", ".webm", ".avi", ".wmv", ".ts") },
            MultiSelect = true
        };

        var files = dialog.ShowDialog(this) != DialogResult.Ok
            ? null
            : dialog.Filenames.Select(f => new FileInfo(f));
        
        context.SetOutput(files);
    }

    private void SelectOutputDirectory(IInteractionContext<Unit, DirectoryInfo?> context)
    {
        using var dialog = new SelectFolderDialog();
        var dir = dialog.ShowDialog(this) != DialogResult.Ok ? null : new DirectoryInfo(dialog.Directory);
        context.SetOutput(dir);
    }

    private void OnNodeContextMenuRequested(object? sender, NodeContextMenuEventArgs e)
    {
        var socketizeSubMenu = new SubMenuItem { Text = "Expose parameter..." };
        
        foreach (var parameter in e.Node.SocketizableParameters)
        {
            var command = new Command((_, _) =>
            {
                e.Node.Node.ExposeParameterAsSocket(parameter);
                Editor.Invalidate();
            }) { MenuText = parameter };
            socketizeSubMenu.Items.Add(command);
        }
        
        var unsocketizeSubMenu = new SubMenuItem { Text = "Remove exposed parameter..." };
        
        foreach (var parameter in e.Node.UnsocketizableParameters)
        {
            var command = new Command((_, _) =>
            {
                e.Node.Node.RemoveExposedParameter(parameter);
                Editor.Invalidate();
            }) { MenuText = parameter };
            unsocketizeSubMenu.Items.Add(command);
        }
        
        var menu = new ContextMenu
        {
            Items =
            {
                new Command((_, _) => OpenNodeEditor(e.Node)) { MenuText = "Edit properties" },
                new Command((_, _) => Editor.RemoveNode(e.Node) ) { MenuText = "Remove" },
                socketizeSubMenu,
                unsocketizeSubMenu
            }
        };
        menu.Closed += (_, _) => menu.Dispose();
        menu.Show();
    }

    private void OpenNodeEditor(VisualNode node)
    {
        var propsWindow = new NodePropertiesWindow(node);
        propsWindow.Show();
    }

    private void OnFieldContextMenuRequested(object? sender, MouseEventArgs args)
    {
        var nodeSubMenu = new SubMenuItem { Text = "Add node..." };

        foreach (var type in NodeImplementations.GetImplementations())
        {
            var attr = type.GetCustomAttribute<NodeAttribute>();
            var subMenu = GetTargetSubMenu(nodeSubMenu, attr?.Category?.Split('/') ?? Array.Empty<string>());
            var command = new Command((_, _) =>
            {
                if (Activator.CreateInstance(type) is not NodeModel.Node node)
                    return;
                Editor.AddNode(node, args.Location.X, args.Location.Y, attr?.Name);
            }) { MenuText = attr?.Name ?? type.Name };
            subMenu.Items.Add(command);
        }

        var menu = new ContextMenu
        {
            Items =
            {
                nodeSubMenu,
                new Command(ImportFromFile) { MenuText = "Import graph..." },
                new Command(ExportToFile) { MenuText = "Export graph..." },
                new Command((_, _) => Editor.Reset()) { MenuText = "Reset" }
            }
        };
        menu.Closed += (_, _) => menu.Dispose();
        menu.Show();
    }

    private void ImportFromFile(object? sender, EventArgs e)
    {
        using var dialog = new OpenFileDialog
        {
            Filters =
            {
                new FileFilter { Name = "JSON", Extensions = new string[] {".json" } }
            }
        };
        if (dialog.ShowDialog(this) is not DialogResult.Ok)
            return;

        ImportFromFile(new FileInfo(dialog.FileName));
    }

    private async Task ImportFromFile(FileInfo fileInfo)
    {
        await using var file = File.Open(fileInfo.FullName, FileMode.Open);
        if (await JsonSerializer.DeserializeAsync<VisualNodeGraphDto>(file) is not { } graphDto)
            throw new Exception("Could not read graph description");
        
        await Editor.LoadFromDto(graphDto);
        
        if (graphDto.ExternalValues is not {} externalValues)
            return;
        
        await global::Eto.Forms.Application.Instance.InvokeAsync(() =>
        {
            foreach (var (key, value) in externalValues)
                ViewModel.ExternalValues.First(field => field.Name == key).Value = value;
        });
    }

    private void ExportToFile(object? sender, EventArgs e)
    {
        using var dialog = new SaveFileDialog()
        {
            Filters =
            {
                new FileFilter { Name = "JSON", Extensions = new string[] {".json" } }
            }
        };
        
        if (dialog.ShowDialog(this) is not DialogResult.Ok)
            return;

        ExportToFile(new FileInfo(dialog.FileName));
    }

    private async Task ExportToFile(FileInfo fileInfo)
    {
        var graphDto = Editor.SaveToDto() with
        {
            ExternalValues = ViewModel.ExternalValues.ToDictionary(field => field.Name, field => field.Value)
        };
        
        await using var file = File.Create(fileInfo.FullName);
        await JsonSerializer.SerializeAsync(file, graphDto);
    }

    private static SubMenuItem GetTargetSubMenu(SubMenuItem root, string[] categoryPath)
    {
        if (categoryPath.Length == 0)
            return root;
        
        if (root.Items.FirstOrDefault(i => i.Text == categoryPath[0]) is SubMenuItem menuItem)
            return GetTargetSubMenu(menuItem, categoryPath.Skip(1).ToArray());
        
        var newItem = new SubMenuItem { Text = categoryPath[0] };
        root.Items.Add(newItem);
        return GetTargetSubMenu(newItem, categoryPath.Skip(1).ToArray());
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            Disposable.Dispose();
        }

        base.Dispose(disposing);
    }
}