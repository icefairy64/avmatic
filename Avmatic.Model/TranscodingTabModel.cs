﻿using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Avmatic.FfmpegModel;
using Avmatic.FfmpegRunner;
using ReactiveUI;

namespace Avmatic.Model;

public sealed class TranscodingTabModel : ReactiveObject, IDisposable
{
    public FileInfo? FfprobePath { get; init; }
    public FileInfo? FfmpegPath { get; init; }
    
    public Action<int, IReadOnlyList<FileInfo>, DirectoryInfo, string, IReadOnlyList<IStreamConfiguration>>? StartAction { get; set; }

    public ObservableCollection<FileInfo> SourceFiles { get; } = new();

    private DirectoryInfo? FOutputDirectory;
    public DirectoryInfo? OutputDirectory
    {
        get => FOutputDirectory;
        set => this.RaiseAndSetIfChanged(ref FOutputDirectory, value);
    }

    private FileInfo? FSampleFile;
    public FileInfo? SampleFile
    {
        get => FSampleFile;
        set => this.RaiseAndSetIfChanged(ref FSampleFile, value);
    }

    private int FJobCapacity;
    public int JobCapacity
    {
        get => FJobCapacity;
        set => this.RaiseAndSetIfChanged(ref FJobCapacity, value);
    }

    private string FFormat;
    public required string Format
    {
        get => FFormat;
        set => this.RaiseAndSetIfChanged(ref FFormat, value);
    }

    public ObservableCollection<StreamTranscodePanelModel> StreamPanels { get; } = new();

    public readonly ReactiveCommand<Unit, Unit> StartCommand;

    private readonly CompositeDisposable CompositeDisposable = new();

    public TranscodingTabModel()
    {
        StartCommand = ReactiveCommand.Create(OnStart,
            this.WhenAnyValue(m => m.OutputDirectory)
                .Select(d => d != null)
                .Zip(this.WhenAnyValue(vm => vm.SampleFile).Select(f => f != null))
                .Select(tuple => tuple is { First: true, Second: true }));

        this.WhenAnyValue(vm => vm.SampleFile)
            .SelectMany(ProbeFile)
            .ObserveOn(RxApp.MainThreadScheduler)
            .Subscribe(OnSampleFileChange)
            .DisposeWith(CompositeDisposable);
    }

    private async Task<InputFile?> ProbeFile(FileInfo? file, CancellationToken cancellationToken)
    {
        if (file is null)
            return null;

        using var probe = new FfProbeRunner(file, FfprobePath);
        return await probe.Run();
    }

    private void OnSampleFileChange(InputFile? file)
    {
        StreamPanels.Clear();
        
        if (file is null)
            return;
        
        foreach (var stream in file.Streams)
            StreamPanels.Add(new StreamTranscodePanelModel { Stream = stream });
    }

    private void OnStart()
    {
        StartAction?.Invoke(JobCapacity, SourceFiles, OutputDirectory ?? throw new Exception("No directory"), Format, StreamPanels.Select(m => m.StreamConfiguration).OfType<IStreamConfiguration>().ToList());
    }

    public void Dispose()
    {
        StartCommand.Dispose();
        CompositeDisposable.Dispose();
    }
}