﻿using Avmatic.FfmpegModel.Parameters;
using Avmatic.FfmpegModel.Parameters.Video;
using Avmatic.FfmpegModel.Parameters.Video.Aom;
using Avmatic.FfmpegModel.Parameters.Video.Svt;

namespace Avmatic.NodeModel.Ffmpeg.Encoders;

[Node("SVT-AV1 encoder", "FFmpeg/Encoders")]
public class FfmpegSvtAv1EncoderNode : FfmpegAbstractVideoEncoderNode
{
    private int FCrf = 36;

    [NodeParameter]
    public int Crf
    {
        get => FCrf;
        set => SetAndUpdateIfChanged(ref FCrf, value);
    }
    
    private int FPreset = 6;

    [NodeParameter]
    public int Preset
    {
        get => FPreset;
        set => SetAndUpdateIfChanged(ref FPreset, value);
    }
    
    private int FFilmGrain = 8;

    [NodeParameter]
    public int FilmGrain
    {
        get => FFilmGrain;
        set => SetAndUpdateIfChanged(ref FFilmGrain, value);
    }
    
    private bool FFilmGrainDenoise = true;

    [NodeParameter]
    public bool FilmGrainDenoise
    {
        get => FFilmGrainDenoise;
        set => SetAndUpdateIfChanged(ref FFilmGrainDenoise, value);
    }
    
    private int? FTune = 0;

    [NodeParameter]
    public int? Tune
    {
        get => FTune;
        set => SetAndUpdateIfChanged(ref FTune, value);
    }

    protected override FfmpegStreamInfo? GetOutputStream()
    {
        return InputStream.Value switch
        {
            FfmpegStreamInfo info => info with
            {
                Parameters = info.Parameters
                    .Concat(Parameters)
                    .ToList()
            },
            null => null,
            _ => throw new Exception("Unsupported value")
        };
    }
    
    private IEnumerable<IParameter> Parameters
    {
        get
        {
            yield return new VideoCodecParameter { Codec = VideoCodec.SvtAv1 };
            yield return new PixelFormatParameter { Format = PixelFormat };
            yield return new CrfParameter { Crf = Crf };
            yield return new BitrateParameter { Bitrate = 0, Unit = BitrateUnit.Bit };
            yield return new PresetParameter { Preset = Preset };
            yield return new SvtExtraParameter
            {
                FilmGrain = FilmGrain,
                FilmGrainDenoise = FilmGrainDenoise ? 1 : 0,
                Tune = Tune
            };
        }
    }
}