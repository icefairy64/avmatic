﻿using Avmatic.FfmpegModel.Parameters.Video;

namespace Avmatic.NodeModel.Ffmpeg;

public abstract class FfmpegAbstractVideoEncoderNode : FfmpegAbstractStreamNode
{
    private PixelFormat FPixelFormat = PixelFormat.Yuv420P10Le;

    [NodeParameter]
    public PixelFormat PixelFormat
    {
        get => FPixelFormat;
        set => SetAndUpdateIfChanged(ref FPixelFormat, value);
    }
}