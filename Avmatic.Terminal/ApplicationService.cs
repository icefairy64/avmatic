﻿using Avmatic.NodeModel.Ffmpeg;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ReactiveUI;
using Terminal.Gui;

namespace Avmatic.Terminal;

public class ApplicationService : BackgroundService
{
    private TaskCompletionSource? RunSource;
    
    private readonly IHost Host;
    private readonly ILogger<ApplicationService> Logger;

    public ApplicationService(IHost host, ILogger<ApplicationService> logger)
    {
        Host = host;
        Logger = logger;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        RunSource = new TaskCompletionSource();
        
        var thread = new Thread(Run)
        {
            Name = "Application thread"
        };
        
        thread.Start(stoppingToken);
        
        return RunSource.Task;
    }

    private void Run(object? state)
    {
        if (state is not CancellationToken stoppingToken)
        {
            RunSource?.TrySetResult();
            return;
        }
        
        if (stoppingToken.IsCancellationRequested)
        {
            RunSource?.TrySetResult();
            return;
        }

        var shutDown = false;
        using var registration = stoppingToken.Register(() =>
        {
            shutDown = true;
            Application.Shutdown();
        });
        
        Logger.LogInformation("FFmpeg path is {}", FfmpegPaths.FfmpegPath);
        Logger.LogInformation("FFprobe path is {}", FfmpegPaths.FfprobePath);
        Logger.LogInformation("Starting application");

        RxApp.MainThreadScheduler = new TerminalScheduler();

        Application.QuitKey = Key.q;
        Application.UseSystemConsole = true;
        Application.Init();
        Application.Run(new MainWindow(Host));
        
        if (!shutDown)
            Application.Shutdown();
        
        RunSource?.TrySetResult();
    }
}