﻿namespace Avmatic.FfmpegModel.Parameters;

public class BitrateParameter : IParameter
{
    public string Name => "b";
    
    public required int Bitrate { get; set; }
    public required BitrateUnit Unit { get; set; }

    private string FormattedUnit => Unit.ToUnitChar();

    public string Value => $"{Bitrate}{FormattedUnit}";
}

public enum BitrateUnit
{
    Bit,
    Kilobit,
    Megabit,
    Gigabit
}

public static class BitrateUnitExtensions
{
    public static string ToUnitChar(this BitrateUnit unit) =>
        unit switch
        {
            BitrateUnit.Bit => "",
            BitrateUnit.Kilobit => "k",
            BitrateUnit.Megabit => "m",
            BitrateUnit.Gigabit => "g",
            _ => throw new ArgumentOutOfRangeException()
        };
}