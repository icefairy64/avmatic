﻿namespace Avmatic.NodeModel.Tests;

public class FileNeighborNodeTests
{
    private NodeGraph Graph;
    private MockSourceNode<FileInfo> Source;
    private FileNeighborNode Replacer;
    private MockSinkNode<FileInfo> Sink;

    [SetUp]
    public void Setup()
    {
        Graph = new NodeGraph();
        Source = new MockSourceNode<FileInfo>();
        Replacer = new FileNeighborNode();
        Sink = new MockSinkNode<FileInfo>();
        
        Graph.Setup(new List<(NodeSocket source, NodeSocket target)>
        {
            (Source.Output, Replacer.File),
            (Replacer.NeighborFile, Sink.Input)
        });
    }

    [Test]
    public void TestSingleReplace()
    {
        Source.Produce(new FileInfo("localFile.txt"));
        Replacer.Match = @"(?<name>.+?)\..+?";
        Replacer.Replace = "$name.gif";
        Assert.Multiple(() =>
        {
            Assert.That(Graph.RaisedExceptions, Is.Empty);
            Assert.That(Sink.Values.FirstOrDefault()?.Name, Is.EqualTo("localFile.gif"));
        });
    }
    
    [Test]
    public void TestMultiReplace()
    {
        Source.Produce(new FileInfo("localFile.txt"));
        Replacer.Match = @"(?<name>.+?)\.(?<ext>.+)";
        Replacer.Replace = "$name.gif.$ext";
        Assert.Multiple(() =>
        {
            Assert.That(Graph.RaisedExceptions, Is.Empty);
            Assert.That(Sink.Values.FirstOrDefault()?.Name, Is.EqualTo("localFile.gif.txt"));
        });
    }
    
    [Test]
    public void TestSingleReplaceWithDirectory()
    {
        Source.Produce(new FileInfo("/tmp/localFile.txt"));
        Replacer.Match = @"(?<name>.+?)\..+?";
        Replacer.Replace = "$name.gif";
        Assert.Multiple(() =>
        {
            Assert.That(Graph.RaisedExceptions, Is.Empty);
            Assert.That(Sink.Values.FirstOrDefault()?.Name, Is.EqualTo("localFile.gif"));
            Assert.That(Sink.Values.FirstOrDefault()?.DirectoryName, Is.EqualTo(new DirectoryInfo("/tmp").FullName));
        });
    }
}