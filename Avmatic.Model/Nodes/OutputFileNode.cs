﻿using Avmatic.NodeModel;

namespace Avmatic.Model.Nodes;

[Node("Output file")]
public class OutputFileNode : SimpleNode
{
    private SimpleNodeSocket<FileInfo> FileSocket { get; }

    private string? FExtension;

    [NodeParameter]
    public string? Extension
    {
        get => FExtension;
        set => SetAndUpdateIfChanged(ref FExtension, value);
    }
    
    private FileInfo? FFile;

    public FileInfo? File
    {
        get => FFile;
        set => SetAndUpdateIfChanged(ref FFile, value);
    }

    public OutputFileNode()
    {
        FileSocket = AddOutput("File", () => new SimpleNodeSocketBuilder<FileInfo>());
    }

    protected override void Update()
    {
        if (File is not { } file)
        {
            FileSocket.Value = null;
            return;
        }

        if (string.IsNullOrWhiteSpace(Extension))
        {
            FileSocket.Value = File;
            return;
        }

        FileSocket.Value = new FileInfo(Path.ChangeExtension(file.FullName, Extension));
    }
}