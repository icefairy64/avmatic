﻿namespace Avmatic.Model.Nodes;

public class ExternalValueFieldModel : ReactiveFieldModel<string>
{
    public ExternalValueNode Node { get; }

    public ExternalValueFieldModel(ExternalValueNode node) : base(node.Name, null, node.Value ?? "")
    {
        Node = node;
    }
}