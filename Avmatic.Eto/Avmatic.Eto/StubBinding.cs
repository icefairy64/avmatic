﻿using System;
using Eto.Forms;

namespace Avmatic.Eto;

public class StubBinding<T> : DirectBinding<T>
{
    private T Value;

    public override T DataValue
    {
        get => Value;
        set
        {
            Value = value;
            OnChanged(new BindingChangedEventArgs(value));
            OnDataValueChanged(EventArgs.Empty);
        }
    }
}