﻿using System.Reflection;
using Avmatic.FfmpegModel.Parameters;
using Avmatic.NodeModel.Ffmpeg.Filters;

namespace Avmatic.NodeModel.Ffmpeg;

public class FfmpegInputStreamSocket : NodeSocket
{
    public FfmpegInputStreamSocket()
    {
        Type = typeof(FfmpegStreamInfo);
    }

    public override bool MatchType(Type type)
    {
        return typeof(FfmpegStreamInfo).IsAssignableFrom(type) || typeof(FfmpegFilterOutputInfo).IsAssignableFrom(type);
    }

    protected override void OnValueChanged(NodeSocketValueChangedArgs args)
    {
        if (args.Value is not FfmpegFilterOutputInfo filterOutputInfo)
        {
            base.OnValueChanged(args);
            return;
        }

        Value = new FfmpegStreamInfo(filterOutputInfo.PadStreamSpecifier, new List<IParameter>());
    }
}

public class FfmpegInputStreamSocketBuilder : NodeSocketBuilder<FfmpegInputStreamSocket>
{
    public override FfmpegInputStreamSocket Build()
    {
        if (Owner is not { } owner)
            throw new ArgumentException("Owner is not specified");
        if (Name is not { } name)
            throw new ArgumentException("Name is not specified");
        return new FfmpegInputStreamSocket { Owner = owner, Name = name };
    }
}