﻿namespace Avmatic.NodeModel.Ffmpeg.Filters;

public abstract class FfmpegAbstractFilterNode : SimpleNode
{
    protected FilterOutputPadStreamSpecifier GetOutputPadStreamSpecifier(int padIndex)
    {
        if (NodeIndex is not { } nodeIndex)
            throw new Exception();
        return new FilterOutputPadStreamSpecifier { PadName = $"node-{nodeIndex}-out-{padIndex}" };
    }
}