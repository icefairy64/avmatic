﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Avmatic.FfmpegModel;
using Avmatic.FfmpegRunner;
using Avmatic.Model.Nodes;
using Avmatic.NodeModel.Ffmpeg;
using Avmatic.NodeModel.Serialization;

namespace Avmatic.Eto.Node;

public partial class NodeEditor
{
    public async Task LoadFromDto(VisualNodeGraphDto visualGraphDto)
    {
        await global::Eto.Forms.Application.Instance.InvokeAsync(() =>
        {
            Clear();
            var nodes = new Dictionary<int, NodeModel.Node>();
            
            foreach (var visualNode in visualGraphDto.VisualNodes)
            {
                var nodeInstance = visualGraphDto.Graph.Nodes[visualNode.NodeIndex].Materialize();
                AddNodeLocal(nodeInstance, visualNode.X, visualNode.Y, visualNode.Width,
                    visualNode.Height, visualNode.Name);
                nodes.Add(visualNode.NodeIndex, nodeInstance);
            }
            
            foreach (var (sourceNodeIndex, sourceSocket, targetNodeIndex, targetSocket) in visualGraphDto.Graph.Connections)
            {
                var sourceNode = nodes[sourceNodeIndex];
                var sourceSocketInstance = sourceNode.Outputs.First(s => s.Name == sourceSocket);
                var targetNode = nodes[targetNodeIndex];
                var targetSocketInstance = targetNode.Inputs.First(s => s.Name == targetSocket);
                Graph.Connect(sourceSocketInstance, targetSocketInstance);
            }
            
            Invalidate();
        });
    }
    
    public VisualNodeGraphDto SaveToDto()
    {
        var graphDto = NodeGraphDto.FromGraph(Graph);
        var nodeDtos = Nodes.Select(n => new VisualNodeDto(Graph.GetNodeIndex(n.Node), (int)n.X, (int)n.Y, (int)n.Width,
            (int)n.Height, n.EffectiveName));
        return new VisualNodeGraphDto(graphDto, nodeDtos.ToList(), null);
    }

    public async Task LoadFromExample(FileInfo file)
    {
        using var probe = new FfProbeRunner(file, FfmpegPaths.FfprobePath);
        if (await probe.Run() is not {} fileInfo)
            return;

        await global::Eto.Forms.Application.Instance.InvokeAsync(() =>
        {
            Clear();
            
            var inputNode = new InputFileNode();
            AddNodeLocal(inputNode, 0, 0, 160, 100, "Input file");

            var sourceFileNode = new FfmpegSourceFileNode();
            AddNodeLocal(sourceFileNode, 200, 0, 160, 100, "Source file");
            
            Graph.Connect(inputNode.File, sourceFileNode.Filename);

            var offset = 0;
            
            foreach (var stream in fileInfo.Streams)
            {
                var node = new FfmpegInputStreamNode
                {
                    StreamType = stream.CodecType.ToStreamType(),
                    StreamIndex = fileInfo.GetTypeLocalStreamIndex(stream)
                };
                AddNodeLocal(node, 400, offset, 200, 160, $"Stream {stream.Index}");
                Graph.Connect(sourceFileNode.File, node.File);
                offset += 200;
            }
            
            Invalidate();
        });
    }
}