﻿using System.Reactive;
using System.Reactive.Linq;
using Avmatic.FfmpegModel;
using Avmatic.FfmpegRunner;
using DynamicData;
using ReactiveUI;

namespace Avmatic.Model;

public sealed class MatrixTabModel : ReactiveObject
{
    public Interaction<Unit, FileInfo?> FileOpenInteraction { get; } = new();
    public Interaction<Unit, DirectoryInfo?> OutputDirectorySelectInteraction { get; } = new();
    
    public FileInfo? FfprobePath { get; set; }

    private FileInfo? FInputFilePath;
    public FileInfo? InputFilePath
    {
        get => FInputFilePath;
        set => this.RaiseAndSetIfChanged(ref FInputFilePath, value);
    }

    private DirectoryInfo? FOutputDirectory;
    public DirectoryInfo? OutputDirectory
    {
        get => FOutputDirectory;
        set => this.RaiseAndSetIfChanged(ref FOutputDirectory, value);
    }

    private string FOutputName = "";
    public string OutputName
    {
        get => FOutputName;
        set => this.RaiseAndSetIfChanged(ref FOutputName, value);
    }

    private string FTimeOffset = "00:00";
    public string TimeOffset
    {
        get => FTimeOffset;
        set => this.RaiseAndSetIfChanged(ref FTimeOffset, value);
    }

    private int FDurationSeconds = 5;
    public int DurationSeconds
    {
        get => FDurationSeconds;
        set => this.RaiseAndSetIfChanged(ref FDurationSeconds, value);
    }

    private string FFormat = "webm";
    public string Format
    {
        get => FFormat;
        set => this.RaiseAndSetIfChanged(ref FFormat, value);
    }

    private int FJobCapacity = 8;
    public int JobCapacity
    {
        get => FJobCapacity;
        set => this.RaiseAndSetIfChanged(ref FJobCapacity, value);
    }

    private readonly SourceList<StreamMatrixPanelModel> FStreamModels = new();
    public IObservableList<StreamMatrixPanelModel> StreamModels => FStreamModels.AsObservableList();

    public ReactiveCommand<Unit, Unit> StartCommand { get; }

    private readonly ObservableAsPropertyHelper<InputFile?> FInputFile;
    public InputFile? InputFile => FInputFile.Value;

    private readonly ProgressTabModel ProgressTabModel;

    public MatrixTabModel(ProgressTabModel progressTab)
    {
        ProgressTabModel = progressTab;

        this.WhenAnyValue(vm => vm.InputFilePath)
            .SelectMany(ProbeFile)
            .ToProperty(this, vm => vm.InputFile, out FInputFile);

        StartCommand = ReactiveCommand.Create(Start,
            this.WhenAnyValue(vm => vm.InputFile, vm => vm.OutputDirectory,
                (inputFile, outputDirectory) => inputFile != null && outputDirectory != null));

        this.WhenAnyValue(vm => vm.InputFile)
            .Subscribe(ReplaceStreams);
    }

    public void SelectInputFile()
    {
        FileOpenInteraction.Handle(Unit.Default)
            .Subscribe(path => InputFilePath = path);
    }
    
    public void SelectOutputDirectory()
    {
        OutputDirectorySelectInteraction.Handle(Unit.Default)
            .Subscribe(path => OutputDirectory = path);
    }

    private void Start()
    {
        if (StreamModels.Count != 1)
            throw new Exception("Unsupported stream count");

        if (InputFilePath is null || OutputDirectory is null)
            throw new Exception();

        var configurations = StreamModels.Items.First().StreamConfigurations.ToList();

        var twoPass = !configurations.Any(c => c.Item2 is SvtAv1StreamConfiguration);
        
        ProgressTabModel.StartMatrix(InputFilePath, OutputDirectory,
            configurations.ToDictionary(c => $"{OutputName}-{c.Item1}", c => c.Item2), Format, TimeOffset,
            DurationSeconds, JobCapacity, twoPass);
    }
    
    private async Task<InputFile?> ProbeFile(FileInfo? file, CancellationToken cancellationToken)
    {
        if (file is null)
            return null;

        OutputName = Path.ChangeExtension(file.Name, null);

        using var probe = new FfProbeRunner(file, FfprobePath);
        return await probe.Run();
    }
    
    private void ReplaceStreams(InputFile? file)
    {
        FStreamModels.Clear();
        
        if (file is null)
            return;

        var panelModel = file.Streams.OfType<VideoInputStream>()
            .Select(s => new StreamMatrixPanelModel
            {
                Stream = s
            })
            .First();
        
        FStreamModels.Add(panelModel);
    }
}