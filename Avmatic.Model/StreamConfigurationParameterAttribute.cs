﻿namespace Avmatic.Model;

[AttributeUsage(AttributeTargets.Property)]
public class StreamConfigurationParameterAttribute : Attribute
{
    public string? FieldName { get; }
    public object? DefaultValue { get; }
    
    public StreamConfigurationParameterAttribute()
    {
    }
    
    public StreamConfigurationParameterAttribute(string fieldName)
    {
        FieldName = fieldName;
    }
    
    public StreamConfigurationParameterAttribute(string fieldName, object defaultValue)
    {
        FieldName = fieldName;
        DefaultValue = defaultValue;
    }
}