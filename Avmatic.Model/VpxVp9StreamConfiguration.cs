﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;
using Avmatic.FfmpegModel.Parameters.Video;
using Avmatic.FfmpegModel.Parameters.Video.Aom;

namespace Avmatic.Model;

[StreamConfiguration("vpx-vp9", StreamType.Video)]
public class VpxVp9StreamConfiguration : VideoStreamConfiguration
{
    [StreamConfigurationParameter("CRF", 28)]
    public int? Crf { get; set; }
    
    [StreamConfigurationParameter("CPU used", 4)]
    public int? CpuUsed { get; set; }
    
    [StreamConfigurationParameter("Lag in frames", 25)]
    public int? LagInFrames { get; set; }
    
    public override IList<IParameter> Parameters
    {
        get
        {
            var result = base.Parameters;
            
            result.Add(new VideoCodecParameter { Codec = VideoCodec.VpxVp9 });
            result.Add(new GenericParameter { Name = "auto-alt-ref", Value = "1" });

            if (Crf is { } crf)
            {
                result.Add(new CrfParameter { Crf = crf });
                result.Add(new BitrateParameter { Bitrate = 0, Unit = BitrateUnit.Bit });
            }

            if (CpuUsed is { } cpuUsed)
                result.Add(new CpuUsedParameter { Usage = cpuUsed });
            
            if (LagInFrames is { } lag)
                result.Add(new LagInFramesParameter { Frames = lag });
            
            if (InputStream is not VideoInputStream videoStream)
                throw new Exception("Not a video stream");
            
            if (videoStream.Framerate.Denominator > 1000000)
            {
                const int denominator = 900000;
                var numerator = (long)Math.Truncate((double)videoStream.Framerate.Numerator /
                    videoStream.Framerate.Denominator * denominator);
                result.Add(new GenericParameter { Name = "r", Value = $"{numerator}/{denominator}" });
            }

            return result;
        }
    }

    public override IStreamConfiguration Clone()
    {
        return new VpxVp9StreamConfiguration
        {
            InputStream = InputStream,
            Crf = Crf,
            CpuUsed = CpuUsed,
            LagInFrames = LagInFrames,
            Resolution = Resolution,
            StreamIndex = StreamIndex,
            CodecType = CodecType,
            PixelFormat = PixelFormat
        };
    }
}