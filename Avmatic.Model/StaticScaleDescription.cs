﻿using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.Model;

public class StaticScaleDescription : IScaleDescription
{
    public required int Width { get; set; }
    public required int Height { get; set; }

    public IParameter Parameter => new GenericParameter { Name = "vf", Value = $"scale={Width}:{Height}:flags=lanczos" };
}