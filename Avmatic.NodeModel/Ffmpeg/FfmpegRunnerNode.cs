﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;
using Avmatic.FfmpegRunner;

namespace Avmatic.NodeModel.Ffmpeg;

[Node("FFmpeg Runner", "FFmpeg")]
public class FfmpegRunnerNode : Node, ITickableNode, IDisposable
{
    public ResultNodeSocket<IReadOnlyList<IParameter>> Parameters { get; }
    public ResultNodeSocket<FileInfo> OutputFile { get; }
    public SimpleNodeSocket<FfmpegRunnerProgress> Progress { get; }
    
    [NodeParameter]
    public bool TwoPass { get; set; }

    private FfmpegRunner.FfmpegRunner? Runner;
    private CancellationTokenSource? CancellationTokenSource;

    public FfmpegRunnerNode()
    {
        Parameters = new ResultNodeSocket<IReadOnlyList<IParameter>> { Owner = this, Name = "Parameters" };
        OutputFile = new ResultNodeSocket<FileInfo> { Owner = this, Name = "Output file" };
        Progress = new SimpleNodeSocket<FfmpegRunnerProgress> { Owner = this, Name = "Progress" };
        
        FInputs.Add(Parameters);
        FInputs.Add(OutputFile);
        FOutputs.Add(Progress);
    }

    public bool Tick()
    {
        if (CancellationTokenSource is {} cts)
            cts.Cancel();

        CancellationTokenSource = new CancellationTokenSource();
        Run(CancellationTokenSource.Token);

        return true;
    }

    private async Task Run(CancellationToken token)
    {
        if (Parameters is not { Value: IList<IParameter> rawParameters } || OutputFile is not { Value: FileInfo outputFile })
            return;
        
        var parameters = ResolveParameters(rawParameters).ToList();

        var firstPassParams = new List<IParameter>(parameters);
        var passLogPath = TwoPass ? Path.GetTempFileName() : null;
        
        if (TwoPass && passLogPath is not null)
        {
            firstPassParams.Add(new PassParameter { Pass = Pass.First });
            firstPassParams.Add(new GenericParameter { Name = "passlogfile", Value = passLogPath });
        }

        using (Runner = new FfmpegRunner.FfmpegRunner(firstPassParams, outputFile, FfmpegPaths.FfmpegPath))
        {
            Runner.Progress += (_, args) => Progress.Value = new FfmpegRunnerProgress(1, TwoPass ? 2 : 1, args.OutTime, false);
            try
            {
                await Runner.Run(token);
            }
            catch (Exception e)
            {
                RaiseException(e);
            }
            Runner = null;
        }

        if (TwoPass && passLogPath is not null)
        {
            if (CancellationTokenSource?.IsCancellationRequested ?? false)
                return;
            
            var secondPassParams = new List<IParameter>(parameters)
            {
                new PassParameter { Pass = Pass.Second },
                new GenericParameter { Name = "passlogfile", Value = passLogPath }
            };

            using (Runner = new FfmpegRunner.FfmpegRunner(secondPassParams, outputFile, FfmpegPaths.FfmpegPath))
            {
                Runner.Progress += (_, args) => Progress.Value = new FfmpegRunnerProgress(2, 2, args.OutTime, false);
                try
                {
                    await Runner.Run(token);
                }
                catch (Exception e)
                {
                    RaiseException(e);
                }
                Runner = null;
            }
        }

        Progress.Value = new FfmpegRunnerProgress(1, 1, TimeSpan.Zero, true);
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
        CancellationTokenSource?.Cancel();
        CancellationTokenSource?.Dispose();
    }

    internal static IEnumerable<IParameter> ResolveParameters(IEnumerable<IParameter> parameters)
    {
        var paramList = parameters.ToList();
        var mapParams = paramList.OfType<MapParameter>().ToList();
        return paramList.Select(p =>
        {
            if (p is not StreamIndexUnresolvedParameter unresolvedParameter)
                return p;

            var mapParamIndex =
                mapParams.FindIndex(x => Equals(x.StreamSpecifier, unresolvedParameter.StreamSpecifier));
            return new ScopedParameter
            {
                Parameter = unresolvedParameter.Parameter,
                StreamSpecifier = new StreamIndexSpecifier { Index = mapParamIndex }
            };
        });
    }
}

public record FfmpegRunnerProgress(int Pass, int PassCount, TimeSpan Position, bool Complete);