﻿using System;
using Eto.Drawing;
using Eto.Forms;
using SkiaSharp;

namespace Avmatic.Eto.Node;

public partial class NodeEditor
{
    private float CameraX = 0;
    private float CameraY = 0;
    private float CameraZoom = 1;

    private PointF MousePreviousPosition;

    private void OnCameraMouseDown(MouseEventArgs e)
    {
        State = NodeEditorState.Panning;
        SelectedNodes.Clear();
        MousePreviousPosition = e.Location;
        Invalidate();
    }
    
    private void OnCameraMouseUp(MouseEventArgs e)
    {
        
    }
    
    private void OnCameraMouseMove(MouseEventArgs e)
    {
        var prevLocalPos = ScreenToLocal(MousePreviousPosition.X, MousePreviousPosition.Y);
        var curLocalPos = ScreenToLocal(e.Location.X, e.Location.Y);
        var dx = curLocalPos.X - prevLocalPos.X;
        var dy = curLocalPos.Y - prevLocalPos.Y;
        CameraX -= dx;
        CameraY -= dy;
        MousePreviousPosition = e.Location;
        Invalidate();
    }
    
    private void OnCameraMouseWheel(MouseEventArgs e)
    {
        if (Math.Sign(e.Delta.Height) > 0)
            CameraZoom *= 1.2f;
        else
            CameraZoom /= 1.2f;
        Invalidate();
    }

    private PointF ScreenToLocal(float x, float y)
    {
        var point = CameraMatrix.Invert().MapPoint(x, y);
        return new PointF(point.X, point.Y);
    }

    private SKMatrix CameraMatrix =>
        SKMatrix.CreateTranslation(Width / 2f, Height / 2f)
            .PreConcat(SKMatrix.CreateScale(CameraZoom, CameraZoom))
            .PreConcat(SKMatrix.CreateTranslation(-CameraX, -CameraY));
}