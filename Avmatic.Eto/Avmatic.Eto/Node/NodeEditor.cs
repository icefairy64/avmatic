﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Avmatic.Model.Nodes;
using Avmatic.NodeModel;
using Eto.Forms;
using Eto.SkiaDraw;

namespace Avmatic.Eto.Node;

public partial class NodeEditor : SkiaDrawable
{
    private NodeEditorState State = NodeEditorState.Idle;
    
    private readonly List<VisualNode> Nodes = new();
    private readonly NodeGraph Graph;

    private int GraphCycleCount;
    private readonly List<(VisualNode?, Exception)> ErroredNodes = new();

    public event EventHandler<NodeContextMenuEventArgs>? NodeContextMenuRequested;
    public event EventHandler<MouseEventArgs>? FieldContextMenuRequested;
    public event EventHandler<NodeEditorFileDropEventArgs>? FileDropped; 

    public NodeEditor() : this(new NodeGraph())
    {
        
    }

    public NodeEditor(NodeGraph graph)
    {
        Graph = graph;
        var offset = 0;
        foreach (var node in graph.Nodes)
        {
            RegisterNode(node, offset, 0, null, null, node.GetType().GetCustomAttribute<NodeAttribute>()?.Name);
            offset += 350;
        }
        
        Graph.PhaseChanged += OnGraphPhaseChanged;
    }

    private void OnGraphPhaseChanged(object? sender, NodeGraphPhaseChangedEventArgs e)
    {
        global::Eto.Forms.Application.Instance.AsyncInvoke(() =>
        {
            if (e.Phase is NodeGraphPhase.Idle)
            {
                ErroredNodes.Clear();
                GraphCycleCount++;
                Invalidate();
            }
            else if (e.Phase is NodeGraphPhase.Error && e.Exceptions is {} exceptions)
            {
                ErroredNodes.AddRange(exceptions.Select(x => (Nodes.Find(n => n.Node == x.Item1), x.Item2)));
                Invalidate();
            }
        });
    }

    public void AddNode(NodeModel.Node node, float x, float y, string? name)
    {
        Graph.AddNode(node);
        RegisterNode(node, x, y, null, null, name);
    }
    
    public VisualNode AddNodeLocal(NodeModel.Node node, float x, float y, float width, float height, string? name)
    {
        Graph.AddNode(node);
        return RegisterNodeLocal(node, x, y, width, height, name);
    }

    public void RemoveNode(VisualNode node)
    {
        Nodes.Remove(node);
        Graph.RemoveNode(node.Node);
        Invalidate();
    }

    protected void Clear()
    {
        Graph.Clear();
        Nodes.Clear();
        Invalidate();
    }

    private void RegisterNode(NodeModel.Node node, float x, float y, float? width, float? height, string? name)
    {
        var localPoint = ScreenToLocal(x, y);
        RegisterNodeLocal(node, localPoint.X, localPoint.Y, width, height, name);
    }
    
    private VisualNode RegisterNodeLocal(NodeModel.Node node, float x, float y, float? width, float? height, string? name)
    {
        var visualNode = new VisualNode(node)
            { X = x, Y = y, Width = width ?? 256, Height = height ?? 192, Name = name };
        Nodes.Add(visualNode);
        Invalidate();
        return visualNode;
    }

    protected override void OnMouseDown(MouseEventArgs e)
    {
        if (e.Buttons.HasFlag(MouseButtons.Alternate))
        {
            if (GetNodeAtScreenLocation(e.Location.X, e.Location.Y) is { } node)
                OnNodeAlternateMouseDown(e, node);
            else
                OnFieldAlternateMouseDown(e);
            return;
        }
        
        if (GetSocketAtScreenLocation(e.Location.X, e.Location.Y) is { Item1: {} owner, Item2: {} socket })
            OnSocketMouseDown(e, socket, owner);
        else if (GetNodeAtScreenLocation(e.Location.X, e.Location.Y) is { } node)
            OnNodeMouseDown(e, node);
        else
            OnCameraMouseDown(e);
    }

    protected override void OnMouseUp(MouseEventArgs e)
    {
        switch (State)
        {
            case NodeEditorState.Panning:
                OnCameraMouseUp(e);
                break;
            case NodeEditorState.CreatingConnection:
                OnCreatingConnectionMouseUp(e);
                break;
            case NodeEditorState.Disconnecting:
                OnDisconnectingMouseUp(e);
                break;
        }
        
        State = NodeEditorState.Idle;
        
        Invalidate();
    }

    protected override void OnMouseMove(MouseEventArgs e)
    {
        switch (State)
        {
            case NodeEditorState.Panning:
                OnCameraMouseMove(e);
                break;
            case NodeEditorState.MovingNode:
            case NodeEditorState.ResizingNode:
                OnNodeMove(e);
                break;
            case NodeEditorState.CreatingConnection:
            case NodeEditorState.Disconnecting:
                OnCreatingConnectionMouseMove(e);
                break;
        }
    }

    protected override void OnMouseWheel(MouseEventArgs e)
    {
        OnCameraMouseWheel(e);
    }

    protected override void OnDragEnter(DragEventArgs e)
    {
        base.OnDragEnter(e);
        
        if (!e.Data.ContainsUris || e.Data.Uris.Length != 1)
            return;
        
        var firstUri = e.Data.Uris[0];
        
        if (firstUri.IsFile)
            e.Effects = DragEffects.Copy;
    }
    
    protected override void OnDragDrop(DragEventArgs e)
    {
        base.OnDragDrop(e);
        
        if (!e.Data.ContainsUris)
            return;
        
        var firstUri = e.Data.Uris[0];

        if (!firstUri.IsFile)
            return;
        
        var firstFile = new FileInfo(firstUri.AbsolutePath);
        
        FileDropped?.Invoke(this, new NodeEditorFileDropEventArgs { File = firstFile });
    }

    public void Reset()
    {
        Graph.Reset();
    }
}

internal enum NodeEditorState
{
    Idle,
    Panning,
    MovingNode,
    ResizingNode,
    CreatingConnection,
    Disconnecting
}

public class NodeContextMenuEventArgs : EventArgs
{
    public required MouseEventArgs MouseEventArgs { get; init; }
    public required VisualNode Node { get; init; }
}

public class NodeEditorFileDropEventArgs : EventArgs
{
    public required FileInfo File { get; init; }
}