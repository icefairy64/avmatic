﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Avmatic.Model;
using Eto.Forms;
using ReactiveUI;

namespace Avmatic.Eto;

public class TranscodingTab : Panel
{
    private readonly CompositeDisposable CompositeDisposable = new();
    private readonly TranscodingTabModel ViewModel = new()
    {
        FfprobePath = FfmpegUtils.GetFfToolPath("ffprobe"),
        JobCapacity = 8,
        Format = "webm"
    };

    private readonly StackLayout StreamStack;

    public TranscodingTab(ProgressTabModel progressTabModel)
    {
        ViewModel.StartAction = (jobCapacity, files, directory, format, streams) =>
            progressTabModel.Start(files, directory, streams, format, jobCapacity, !streams.Any(c => c is SvtAv1StreamConfiguration));
        
        var openFolderButton = new Button(OnSelectOutputDirClick)
        {
            Text = "Select a directory"
        };
        
        var selectFilesButton = new Button(OnSelectFilesClick)
        {
            Text = "Select files"
        };

        var startButton = new Button((sender, args) => ViewModel.StartCommand.Execute().Subscribe())
        {
            Text = "Start"
        };
        ViewModel.WhenAnyObservable(vm => vm.StartCommand.CanExecute)
            .BindToControl(startButton, b => b.Enabled)
            .DisposeWith(CompositeDisposable);

        var label = new Label();
        ViewModel.WhenAnyValue(vm => vm.OutputDirectory)
            .Select(d => d?.FullName ?? "No directory selected")
            .BindToControl(label, l => l.Text)
            .DisposeWith(CompositeDisposable);

        StreamStack = new StackLayout
        {
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Spacing = 8
        };

        ViewModel.StreamPanels.CollectionChanged += StreamPanelsOnCollectionChanged;

        Content = new StackLayout
        {
            Padding = 8,
            Spacing = 8,
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Items =
            {
                selectFilesButton,
                openFolderButton,
                label,
                FieldUtil.CreateNumericField("Job capacity", ViewModel, vm => vm.JobCapacity, CompositeDisposable),
                FieldUtil.CreateSelectorField("Format", ViewModel, vm => vm.Format, CompositeDisposable, "webm", "mp4"),
                new StackLayoutItem(new Scrollable { Content = StreamStack }, expand: true),
                startButton,
            }
        };
    }

    private void StreamPanelsOnCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
    {
        switch (e.Action)
        {
            case NotifyCollectionChangedAction.Add:
                foreach (var panelModel in (e.NewItems ?? throw new InvalidOperationException()).Cast<StreamTranscodePanelModel>())
                    StreamStack.Items.Add(new StreamTranscodePanel(panelModel));
                break;
            case NotifyCollectionChangedAction.Remove:
                foreach (var panelModel in (e.OldItems ?? throw new InvalidOperationException()).Cast<StreamTranscodePanelModel>())
                    StreamStack.Items.Remove(StreamStack.Items.First(x =>
                        (x.Control as StreamTranscodePanel)?.ViewModel == panelModel));
                break;
            case NotifyCollectionChangedAction.Replace:
                foreach (var panelModel in (e.OldItems ?? throw new InvalidOperationException()).Cast<StreamTranscodePanelModel>())
                    StreamStack.Items.Remove(StreamStack.Items.First(x =>
                        (x.Control as StreamTranscodePanel)?.ViewModel == panelModel));
                foreach (var panelModel in (e.NewItems ?? throw new InvalidOperationException()).Cast<StreamTranscodePanelModel>())
                    StreamStack.Items.Add(new StreamTranscodePanel(panelModel));
                break;
            case NotifyCollectionChangedAction.Move:
                throw new NotImplementedException();
                break;
            case NotifyCollectionChangedAction.Reset:
                StreamStack.Items.Clear();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    protected override void Dispose(bool disposing)
    {
        CompositeDisposable.Dispose();
        base.Dispose(disposing);
    }
    
    private void OnSelectFilesClick(object? sender, EventArgs eventArgs)
    {
        using var dialog = new OpenFileDialog
        {
            Filters = { new FileFilter("Video files", ".mp4", ".mkv", ".webm", ".avi") },
            MultiSelect = true
        };

        if (dialog.ShowDialog(this) != DialogResult.Ok)
            return;
            
        foreach (var file in dialog.Filenames.Select(f => new FileInfo(f)))
            ViewModel.SourceFiles.Add(file);

        ViewModel.SampleFile = ViewModel.SourceFiles.Last();
    }
    
    private void OnSelectOutputDirClick(object? sender, EventArgs eventArgs)
    {
        using var dialog = new SelectFolderDialog();

        if (dialog.ShowDialog(this) != DialogResult.Ok)
            return;

        ViewModel.OutputDirectory = new DirectoryInfo(dialog.Directory);
    }
}