﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.Model;

public interface IStreamConfiguration
{
    public InputStream? InputStream { get; set; }
    public IList<IParameter> Parameters { get; }
    public int StreamIndex { get; set; }
    public string CodecType { get; set; }

    public IStreamConfiguration Clone();

    public static IReadOnlyList<Type> GetImplementations()
    {
        return typeof(IStreamConfiguration).Assembly.ExportedTypes
            .Where(t => typeof(IStreamConfiguration).IsAssignableFrom(t))
            .ToList();
    }
}