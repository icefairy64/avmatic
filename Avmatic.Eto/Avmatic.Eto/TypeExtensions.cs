﻿using System;

namespace Avmatic.Eto;

public static class TypeExtensions
{
    public static Type? GetNullableUnderlying(this Type type)
    {
        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            return Nullable.GetUnderlyingType(type);
        else
            return type;
    }
}