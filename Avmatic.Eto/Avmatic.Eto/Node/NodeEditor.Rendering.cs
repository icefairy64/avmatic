﻿using System;
using System.Linq;
using Avmatic.NodeModel;
using DynamicData;
using Eto.Drawing;
using Eto.SkiaDraw;
using SkiaSharp;

namespace Avmatic.Eto.Node;

public partial class NodeEditor
{
    private static readonly SKPaint TextPaint = new SKPaint(new SKFont(SKTypeface.Default)) { Color = SKColors.Azure };

    private static readonly SKPaint TextPaintRightAligned = new SKPaint(new SKFont(SKTypeface.Default))
        { Color = SKColors.Azure, TextAlign = SKTextAlign.Right };

    protected override void OnPaint(SKPaintEventArgs e)
    {
        var canvas = e.Surface.Canvas;
        
        canvas.Save();

        var cameraMatrix = CameraMatrix;
        canvas.Concat(ref cameraMatrix);

        DrawBackground(canvas);
        DrawNodes(canvas);
        DrawConnections(canvas);

        if (State is NodeEditorState.CreatingConnection or NodeEditorState.Disconnecting)
            DrawNewConnection(canvas);
        
        canvas.Restore();
        
        DrawOverlay(canvas);
    }

    private void DrawOverlay(SKCanvas canvas)
    {
        var textPaint = new SKPaint(new SKFont(SKTypeface.Default)) { Color = SKColors.Azure };
        canvas.DrawText($"X: {CameraX}", new SKPoint(12, 20), textPaint);
        canvas.DrawText($"Y: {CameraY}", new SKPoint(12, 40), textPaint);
        canvas.DrawText($"Zoom: {CameraZoom}", new SKPoint(12, 60), textPaint);
        canvas.DrawText($"State: {State}", new SKPoint(12, 80), textPaint);
        canvas.DrawText($"Cycles: {GraphCycleCount}", new SKPoint(12, 100), textPaint);
    }

    private void DrawBackground(SKCanvas canvas)
    {
        canvas.Clear(SKColor.FromHsv(230f, 48f, 48f));
        for (var xi = -30; xi <= 30; xi++)
        {
            canvas.DrawLine(((float)Math.Floor(CameraX / 100f) + xi) * 100f, -10000, ((float)Math.Floor(CameraX / 100f) + xi) * 100f, 10000, new SKPaint { Color = new SKColor(255, 255, 255, 48) });
        }
        
        for (var yi = -30; yi <= 30; yi++)
        {
            canvas.DrawLine(-10000, ((float)Math.Floor(CameraY / 100f) + yi) * 100f, 10000, ((float)Math.Floor(CameraY / 100f) + yi) * 100f, new SKPaint { Color = new SKColor(255, 255, 255, 48) });
        }
    }

    private void DrawNodes(SKCanvas canvas)
    {
        foreach (var node in Nodes)
            DrawNode(canvas, node);
    }

    private void DrawNode(SKCanvas canvas, VisualNode node)
    {
        var color = SelectedNodes.Contains(node) ? SKColors.DarkOrchid : SKColor.FromHsv(260, 48, 24);
        string? errorText = null;

        if (ErroredNodes.FirstOrDefault(x => x.Item1 == node) is { Item1: { } erroredNode, Item2: { } exception })
        {
            color = SKColors.Red;
            errorText = (exception as AggregateException)?.InnerExceptions[0].Message ?? exception.Message;
        }

        canvas.DrawRoundRect(node.X, node.Y, node.Width, node.Height, 12, 12, new SKPaint
        {
            Color = SKColor.FromHsv(0, 0, 0, 96),
            MaskFilter = SKMaskFilter.CreateBlur(SKBlurStyle.Outer, 8)
        });
        canvas.DrawRoundRect(node.X, node.Y, node.Width, node.Height, 12, 12, new SKPaint { Color = color });
        canvas.DrawRoundRect(node.X, node.Y, node.Width, node.Height, 12, 12, new SKPaint { Color = SKColor.FromHsv(0, 0, 100, 48), StrokeWidth = 1, Style = SKPaintStyle.Stroke, IsAntialias = true });
        canvas.DrawText(node.EffectiveName, new SKPoint(node.X + 8, node.Y + 20), TextPaint);
        canvas.DrawLine(node.X, node.Y + 32, node.X + node.Width, node.Y + 32, new SKPaint { Color = SKColor.FromHsv(0, 0, 100, 48), StrokeWidth = 1 });

        var socketOffset = 48;
        foreach (var input in node.Node.Inputs)
        {
            var socketY = node.Y + socketOffset;
            var socketColor = input.Value == null ? SKColors.DarkSlateGray : SKColors.Aqua;
            canvas.DrawCircle(node.X, socketY, 7, new SKPaint { Color = socketColor });
            canvas.DrawCircle(node.X, socketY, 7, new SKPaint { Color = SKColors.DodgerBlue, Style = SKPaintStyle.Stroke, IsAntialias = true });
            canvas.DrawText(input.Name, new SKPoint(node.X + 12, socketY + 6f), TextPaint);
            socketOffset += 24;
        }
        
        var maxOffset = socketOffset;

        socketOffset = 48;
        foreach (var output in node.Node.Outputs)
        {
            var socketY = node.Y + socketOffset;
            var socketColor = output.Value == null ? SKColors.DarkSlateGray : SKColors.Aqua;
            canvas.DrawCircle(node.X + node.Width, socketY, 7, new SKPaint { Color = socketColor });
            canvas.DrawCircle(node.X + node.Width, socketY, 7, new SKPaint { Color = SKColors.DodgerBlue, Style = SKPaintStyle.Stroke, IsAntialias = true });
            canvas.DrawText(output.Name, new SKPoint(node.X + node.Width - 12, socketY + 6f), TextPaintRightAligned);
            socketOffset += 24;
        }

        if (maxOffset > socketOffset)
            socketOffset = maxOffset;
        
        foreach (var parameterName in node.Parameters)
        {
            var paramY = node.Y + socketOffset;
            canvas.DrawText(parameterName, new SKPoint(node.X + 12, paramY + 6f), TextPaint);
            canvas.DrawText(node.GetParameterValue(parameterName)?.ToString() ?? "-", new SKPoint(node.X + node.Width - 12, paramY + 6f), TextPaintRightAligned);
            socketOffset += 24;
        }

        if (errorText is not null)
            canvas.DrawText(errorText, new SKPoint(node.X + 12, socketOffset + 6f), TextPaint);
    }

    private void DrawConnections(SKCanvas canvas)
    {
        foreach (var connection in Graph.Connections)
        {
            var source = Nodes.First(n => n.Node.Outputs.Contains(connection.Source));
            var sourceSocketIndex = source.Node.Outputs.IndexOf(connection.Source);
            var target = Nodes.First(n => n.Node.Inputs.Contains(connection.Target));
            var targetSocketIndex = target.Node.Inputs.IndexOf(connection.Target);
            if (State != NodeEditorState.Disconnecting || !ReferenceEquals(MovingConnection, connection))
                DrawConnection(canvas, source, sourceSocketIndex, target, targetSocketIndex);
        }
    }
    
    private void DrawConnection(SKCanvas canvas, VisualNode from, int fromSocketIndex, VisualNode to, int toSocketIndex)
    {
        const int socketOffset = 48;
        var fromLocation = new PointF(from.X + from.Width, from.Y + socketOffset + 24f * fromSocketIndex);
        var toLocation = new PointF(to.X, to.Y + socketOffset + 24f * toSocketIndex);
        canvas.DrawLine(fromLocation.X, fromLocation.Y, toLocation.X, toLocation.Y, new SKPaint
        {
            Color = SKColors.Bisque,
            StrokeWidth = 2,
            StrokeCap = SKStrokeCap.Round,
            IsAntialias = true
        });
    }
    
    private void DrawNewConnection(SKCanvas canvas)
    {
        if (SourceNode is not {} source || SourceSocketIndex is not {} sourceSocketIndex)
            return;
        
        const int socketOffset = 48;
        
        var fromLocation = new PointF(source.X + source.Width, source.Y + socketOffset + 24f * sourceSocketIndex);

        PointF toLocation;
        SKPaint paint;
        
        if (TargetNode is not { } target || TargetSocketIndex is not { } targetSocketIndex)
        {
            paint = new SKPaint
            {
                Color = SKColors.OrangeRed,
                StrokeWidth = 2,
                PathEffect = SKPathEffect.CreateDash(new float[] { 10, 10 }, 0),
                IsAntialias = true
            };
            toLocation = ScreenToLocal(MousePreviousPosition.X, MousePreviousPosition.Y);
        }
        else
        {
            paint = new SKPaint { Color = SKColors.GreenYellow, StrokeWidth = 2, IsAntialias = true };
            toLocation = new PointF(target.X, target.Y + socketOffset + 24f * targetSocketIndex);
        }

        canvas.DrawLine(fromLocation.X, fromLocation.Y, toLocation.X, toLocation.Y, paint);
    }
}