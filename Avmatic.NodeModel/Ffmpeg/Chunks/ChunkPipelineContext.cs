﻿using Avmatic.FfmpegModel;

namespace Avmatic.NodeModel.Ffmpeg.Chunks;

public class ChunkPipelineContext
{
    public required InputFile FileInfo { get; init; }
}