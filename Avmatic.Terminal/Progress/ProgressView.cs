﻿using System.Reactive.Disposables;
using System.Reactive.Linq;
using Avmatic.Model;
using NStack;
using ReactiveUI;
using Terminal.Gui;

namespace Avmatic.Terminal.Progress;

public class ProgressView : View
{
    private readonly CompositeDisposable Disposable;

    public ProgressView(ProgressTabModel viewModel)
    {
        Disposable = new CompositeDisposable();

        var label = new Label
        {
            Y = Pos.AnchorEnd() - 1
        };

        viewModel.WhenAnyValue(vm => vm.Elapsed, vm => vm.Eta)
            .DistinctUntilChanged(t => t.Item2)
            .Select(tuple => tuple switch
            {
                (null, _) or (_, null) => "No ETA",
                (var elapsed, { TotalMilliseconds: 0 }) => $"Finished in {elapsed:g}",
                var (elapsed, eta) => $"ETA: {eta:g}, elapsed: {elapsed:g}"
            })
            .Select(ustring.Make)
            .BindToView(label, l => l.Text)
            .DisposeWith(Disposable);

        var totalProgressBar = new ProgressBar
        {
            Y = Pos.Top(label) - 2,
            Width = Dim.Fill()
        };
        
        viewModel.WhenAnyValue(vm => vm.TotalProgress)
            .BindToView(totalProgressBar, b => b.Fraction)
            .DisposeWith(Disposable);

        var jobStack = new View
        {
            X = 1,
            Y = 1,
            Width = Dim.Fill(1),
            Height = Dim.Fill(6),
            Border = new Border
            {
                Padding = new Thickness(1),
                Background = Color.Blue,
                BorderBrush = Color.White,
                BorderStyle = BorderStyle.Single
            }
        };

        viewModel.JobsInProgress
            .Connect()
            .BindToStackView(jobStack,
                (vm, c) => c.ViewModel == vm,
                vm => new ProgressEntryView(vm))
            .DisposeWith(Disposable);

        Width = Dim.Fill();
        Height = Dim.Fill();
        
        Add(jobStack, totalProgressBar, label);
    }

    protected override void Dispose(bool disposing)
    {
        Disposable.Dispose();
        base.Dispose(disposing);
    }
}

public class ProgressEntryView : View
{
    internal readonly JobProgressBarModel ViewModel;
    private readonly CompositeDisposable Disposable;
    
    public ProgressEntryView(JobProgressBarModel viewModel)
    {
        ViewModel = viewModel;
        Disposable = new CompositeDisposable();
        
        var progressBar = new ProgressBar
        {
            Width = Dim.Fill()
        };

        viewModel.WhenAnyValue(vm => vm.Progress)
            .BindToView(progressBar, bar => bar.Fraction)
            .DisposeWith(Disposable);

        var filenameLabel = new Label
        {
            Y = Pos.Bottom(progressBar)
        };
        
        viewModel.WhenAnyValue(vm => vm.Filename)
            .Select(s => s ?? "No filename")
            .Select(ustring.Make)
            .BindToView(filenameLabel, l => l.Text)
            .DisposeWith(Disposable);
        
        var label = new Label
        {
            Y = Pos.Bottom(filenameLabel)
        };
        
        viewModel.WhenAnyValue(vm => vm.Stage)
            .Select(s => s ?? "Unknown stage")
            .Select(ustring.Make)
            .BindToView(label, l => l.Text)
            .DisposeWith(Disposable);
        
        Height = 3;
        Width = Dim.Fill();
        
        Add(progressBar, filenameLabel, label);
    }

    protected override void Dispose(bool disposing)
    {
        Disposable.Dispose();
        base.Dispose(disposing);
    }
}