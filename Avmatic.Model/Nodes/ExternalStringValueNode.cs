﻿using Avmatic.NodeModel;

namespace Avmatic.Model.Nodes;

[Node("External string value")]
public class ExternalStringValueNode : ExternalValueNode
{
    public override Type ValueType => typeof(string);
    
    protected override object? ParseValue(string value)
    {
        return value;
    }
}