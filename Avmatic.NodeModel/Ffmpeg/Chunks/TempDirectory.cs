﻿namespace Avmatic.NodeModel.Ffmpeg.Chunks;

public sealed class TempDirectory : IDisposable
{
    public DirectoryInfo Directory { get; }

    public int FileCount => Directory.EnumerateFiles().Count();

    public TempDirectory()
    {
        var tempPath = Path.GetTempPath();
        var srcDirPath = Path.Combine(tempPath, $"{DateTime.UtcNow.ToBinary():x8}");
        Directory = new DirectoryInfo(srcDirPath);
        Directory.Create();
    }

    public void Clear()
    {
        foreach (var entry in Directory.EnumerateFileSystemInfos())
        {
            if (entry is DirectoryInfo dir)
                dir.Delete(true);
            else
                entry.Delete();
        }
    }

    public void Dispose()
    {
        Directory.Delete(true);
    }
}