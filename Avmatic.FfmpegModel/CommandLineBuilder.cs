﻿using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.FfmpegModel;

public class CommandLineBuilder
{
    private Dictionary<int, InputFile> Inputs = new();
    private Dictionary<IStreamSpecifier, IStreamSpecifier> StreamMappings = new();
    private List<IParameter> Parameters = new();

    public IList<InputStream> Streams => Inputs.Values.SelectMany(i => i.Streams).ToList();

    public CommandLineBuilder WithInput(InputFile input)
    {
        if (Inputs.ContainsKey(input.Index))
            throw new ArgumentException("Input with given index is already present");
        Inputs[input.Index] = input;
        return this;
    }

    public CommandLineBuilder UseStream(InputStream stream)
    {
        var inputStreamSpecifier = GetInputStreamSpecifier(stream);
        if (StreamMappings.ContainsKey(inputStreamSpecifier))
            throw new ArgumentException("Provided stream is already in use");
            
        StreamMappings[inputStreamSpecifier] = new StreamIndexSpecifier
        {
            Index = StreamMappings.Count
        };

        return this;
    }

    public CommandLineBuilder ConfigureStream(InputStream stream, Action<StreamParametersBuilder> configureAction)
    {
        var inputStreamSpecifier = GetInputStreamSpecifier(stream);
        var streamSpecifier = StreamMappings[inputStreamSpecifier];
        
        var paramBuilder = new StreamParametersBuilder
        {
            StreamSpecifier = streamSpecifier
        };
        
        configureAction(paramBuilder);
        
        Parameters.AddRange(paramBuilder.Parameters);
        
        return this;
    }

    public IList<IParameter> BuildParameters()
    {
        var parameters = StreamMappings.Keys
            .Select(inputStream => new MapParameter { StreamSpecifier = inputStream })
            .Cast<IParameter>()
            .ToList();

        parameters.AddRange(Parameters);

        return parameters;
    }

    public IList<string> BuildCommandLineArguments()
    {
        return BuildCommandLineArguments(BuildParameters());
    }

    private IStreamSpecifier GetInputStreamSpecifier(InputStream stream)
    {
        var inputPair = Inputs.First(p => p.Value.Streams.Contains(stream));
        return inputPair.Value.GetStreamSpecifier(stream);
    }

    public static IList<string> BuildCommandLineArguments(IEnumerable<IParameter> parameters)
    {
        return parameters.SelectMany(param => new[] { $"-{param.Name}", param.Value })
            .ToList();
    }
}

public class StreamParametersBuilder
{
    public required IStreamSpecifier StreamSpecifier { get; init; }
    
    internal List<IParameter> Parameters = new();

    public StreamParametersBuilder WithParameter(IParameter parameter)
    {
        Parameters.Add(new ScopedParameter { Parameter = parameter, StreamSpecifier = StreamSpecifier });
        return this;
    }
}