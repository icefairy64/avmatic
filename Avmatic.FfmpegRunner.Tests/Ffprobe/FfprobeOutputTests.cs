using Avmatic.FfmpegModel;
using Avmatic.FfmpegRunner;

namespace AvMatic.FfmpegRunner.Tests;

public class FfprobeOutputTests
{
    private Dictionary<string, FileInfo> Dumps;
    
    [SetUp]
    public void Setup()
    {
        var dir = new DirectoryInfo(Path.Join(TestContext.CurrentContext.TestDirectory, "FfProbe", "Dumps"));
        Dumps = dir.EnumerateFiles().ToDictionary(f => f.Name);
    }

    [Test]
    public void TestSample1()
    {
        var file = Dumps["probe_SMR.txt"];
        var lines = File.ReadLines(file.FullName);
        var handler = new FfProbeOutputHandler();
        
        foreach (var line in lines) 
            handler.HandleLine(line);

        var input = handler.Input;
        Assert.That(input.Streams, Has.Count.EqualTo(5));
        
        Assert.That(input.Format.FormatName, Is.EqualTo("matroska,webm"));
        Assert.That(input.Format.Duration, Is.EqualTo(TimeSpan.FromMilliseconds(1419872)));

        var video = input.Streams[0] as VideoInputStream;
        Assert.That(video, Is.Not.Null);
        Assert.That(video.CodecName, Is.EqualTo("h264"));
        Assert.That(video.Width, Is.EqualTo(640));
        Assert.That(video.Height, Is.EqualTo(480));
        Assert.Multiple(() =>
        {
            Assert.That(video.Framerate.Numerator, Is.EqualTo(24000));
            Assert.That(video.Framerate.Denominator, Is.EqualTo(1001));
        });
        var audio = input.Streams[1] as AudioInputStream;
        Assert.That(audio, Is.Not.Null);
        Assert.That(audio.SampleRate, Is.EqualTo(48000));
    }
}