﻿namespace Avmatic.FfmpegModel;

public class InputFile
{
    public int Index { get; set; }
    public required IList<InputStream> Streams { get; set; }
    public required InputFormat Format { get; set; }

    public IStreamSpecifier GetStreamSpecifier(InputStream stream)
    {
        var streamIndex = GetTypeLocalStreamIndex(stream);

        return new InputStreamSpecifier
        {
            InputIndex = Index,
            AdditionalSpecifier = new StreamTypeSpecifier
            {
                Type = stream.CodecType.ToStreamType(),
                AdditionalSpecifier = new StreamIndexSpecifier
                {
                    Index = streamIndex
                }
            }
        };
    }

    public int GetTypeLocalStreamIndex(InputStream stream)
    {
        return Streams
            .Where(s => s.CodecType == stream.CodecType)
            .TakeWhile(s => s != stream)
            .Count();
    }

    public override string ToString()
    {
        return $"{nameof(Index)}: {Index}, {nameof(Format)}: {Format}";
    }
}