﻿using System.Text.Json.Serialization;

namespace Avmatic.FfmpegModel;

public class StreamTypeSpecifier : IStreamSpecifier
{
    public required StreamType Type { get; set; }
    public IStreamSpecifier? AdditionalSpecifier { get; set; }

    [JsonIgnore]
    public string String => AdditionalSpecifier != null
            ? $"{Type.ToStreamSelectorChar()}:{AdditionalSpecifier.String}"
            : Type.ToStreamSelectorChar();

    protected bool Equals(StreamTypeSpecifier other)
    {
        return Type == other.Type && Equals(AdditionalSpecifier, other.AdditionalSpecifier);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((StreamTypeSpecifier)obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine((int)Type, AdditionalSpecifier);
    }
}