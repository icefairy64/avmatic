﻿namespace Avmatic.FfmpegModel.Parameters;

public class SeekParameter : IParameter
{
    public required string Position { get; init; }

    public string Name => "ss";
    public string Value => Position;
}