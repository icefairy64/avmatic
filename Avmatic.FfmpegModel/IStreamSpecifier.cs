﻿namespace Avmatic.FfmpegModel;

public interface IStreamSpecifier
{
    public string String { get; }
}