﻿using Avmatic.FfmpegModel;

namespace Avmatic.FfmpegRunner;

public class FfProbeOutputHandler : IOutputHandler
{
    public event EventHandler? Finished;
    
    private PropertyOutputHandler? PropertyHandler;
    private List<InputStream> InputStreams = new();
    private InputFormat? Format;

    public void HandleLine(string line)
    {
        switch (line)
        {
            case "[STREAM]":
            case "[FORMAT]":
            {
                PropertyHandler = new();
                break;
            }
            case "[/STREAM]":
            {
                InputStreams.Add(CreateInputStream());
                break;
            }
            case "[/FORMAT]":
            {
                Format = CreateFormat();
                Finished?.Invoke(this, EventArgs.Empty);
                break;
            }
            default:
            {
                PropertyHandler?.HandleLine(line);
                break;
            }
        }
    }

    public InputFile Input
    {
        get
        {
            if (Format is null)
                throw new Exception("Format is not present");

            return new InputFile
            {
                Index = 0,
                Format = Format,
                Streams = InputStreams
            };
        }
    }

    private InputStream CreateInputStream()
    {
        if (PropertyHandler is null)
            throw new Exception("No property handler");

        var index = int.Parse(PropertyHandler.Properties["index"]);
        var codecName = PropertyHandler.Properties["codec_name"];
        var codecType = PropertyHandler.Properties["codec_type"];
        var framerate = PropertyHandler.Properties["r_frame_rate"];
        var tags = PropertyHandler.Properties
            .Where(p => p.Key.StartsWith("TAG:"))
            .Select(p => KeyValuePair.Create(p.Key["TAG:".Length..], p.Value))
            .ToDictionary(p => p.Key, p => p.Value);

        return codecType switch
        {
            "video" => new VideoInputStream(
                Index: index,
                CodecName: codecName,
                CodecType: codecType,
                Tags: tags,
                Framerate: (long.Parse(framerate[..framerate.IndexOf('/')]), long.Parse(framerate[(framerate.IndexOf('/') + 1)..])),
                Width: int.Parse(PropertyHandler.Properties["width"]),
                Height: int.Parse(PropertyHandler.Properties["height"])),
            
            "audio" => new AudioInputStream(
                Index: index,
                CodecName: codecName,
                CodecType: codecType,
                Tags: tags,
                SampleRate: int.Parse(PropertyHandler.Properties["sample_rate"])),
            
            _ => new InputStream(
                Index: index,
                CodecName: codecName,
                CodecType: codecType,
                Tags: tags)
        };
    }
    
    private InputFormat CreateFormat()
    {
        if (PropertyHandler is null)
            throw new Exception("No property handler");

        double? durationSec = null;
        if (double.TryParse(PropertyHandler.Properties["duration"], out var tmpSec))
            durationSec = tmpSec;
        
        var name = PropertyHandler.Properties["format_name"];

        return new InputFormat
        {
            Duration = durationSec is {} sec ? TimeSpan.FromSeconds(sec) : null,
            FormatName = name
        };
    }
}