﻿namespace Avmatic.NodeModel.Ffmpeg.Filters;

public record FfmpegFilterInfo(string Name, IReadOnlyDictionary<string, string> Parameters, IReadOnlyList<IFfmpegFilterInputInfo> Inputs);
public record FfmpegFilterOutputInfo(FfmpegFilterInfo Filter, int Index, FilterOutputPadStreamSpecifier PadStreamSpecifier);

public interface IFfmpegFilterInputInfo
{
    
}

public record FfmpegFilterStreamInputInfo(FfmpegStreamInfo StreamInfo) : IFfmpegFilterInputInfo;
public record FfmpegFilterChainInputInfo(FfmpegFilterOutputInfo SourceInfo) : IFfmpegFilterInputInfo;
