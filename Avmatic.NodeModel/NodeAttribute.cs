﻿namespace Avmatic.NodeModel;

[AttributeUsage(AttributeTargets.Class)]
public class NodeAttribute : Attribute
{
    public string? Name { get; }
    public string? Category { get; }

    public NodeAttribute(string? name, string? category = null)
    {
        Name = name;
        Category = category;
    }
}