﻿namespace Avmatic.NodeModel;

public class NodeGraph
{
    private List<Node> FNodes = new();
    private List<NodeSocketConnection> FConnections = new();

    public IReadOnlyList<Node> Nodes => FNodes;
    public IReadOnlyList<NodeSocketConnection> Connections => FConnections;

    private NodeSocket? PropagatingSource;
    private readonly List<NodeSocket> UpdatedInputs = new();
    
    private readonly List<(Node?, Exception)> FRaisedExceptions = new();
    public IReadOnlyList<(Node?, Exception)> RaisedExceptions => FRaisedExceptions;

    private NodeGraphPhase FPhase = NodeGraphPhase.Idle;
    public NodeGraphPhase Phase
    {
        get => FPhase;
        private set
        {
            var curPhase = FPhase;
            FPhase = value;
            if (curPhase != value)
                PhaseChanged?.Invoke(this, new NodeGraphPhaseChangedEventArgs
                {
                    Phase = value,
                    Exceptions = value is NodeGraphPhase.Error ? FRaisedExceptions : null
                });
        }
    }

    public event EventHandler<NodeGraphPhaseChangedEventArgs>? PhaseChanged;
    public event EventHandler<NodeGraphNodeEventArgs>? NodeAdded;
    public event EventHandler<NodeGraphNodeEventArgs>? NodeRemoved;

    private bool Batching;

    public void AddNode(Node node)
    {
        FNodes.Add(node);
        
        node.Graph = this;
        
        foreach (var nodeOutput in node.Outputs)
        {
            nodeOutput.ValueChanging += OnNodeOutputValueChanging;
            nodeOutput.ValueChanged += OnNodeOutputValueChanged;
            nodeOutput.Disposing += OnNodeSocketDisposing;
        }
        
        foreach (var nodeInput in node.Inputs)
        {
            nodeInput.ValueChanging += OnNodeInputValueChanging;
            nodeInput.Disposing += OnNodeSocketDisposing;
        }
        
        NodeAdded?.Invoke(this, new NodeGraphNodeEventArgs { Node = node });
    }

    public void RemoveNode(Node node)
    {
        if (!FNodes.Remove(node))
            throw new ArgumentException("Node does not belong to this graph");

        node.Graph = null;
        
        foreach (var nodeOutput in node.Outputs)
        {
            nodeOutput.ValueChanging -= OnNodeOutputValueChanging;
            nodeOutput.ValueChanged -= OnNodeOutputValueChanged;
            nodeOutput.Disposing -= OnNodeSocketDisposing;
        }
        
        foreach (var nodeInput in node.Inputs)
        {
            nodeInput.ValueChanging -= OnNodeInputValueChanging;
            nodeInput.Disposing -= OnNodeSocketDisposing;
        }

        foreach (var connection in FConnections.Where(c => c.Source.Owner == node || c.Target.Owner == node).ToList())
            Disconnect(connection);
        
        NodeRemoved?.Invoke(this, new NodeGraphNodeEventArgs { Node = node });
    }

    public void Connect(NodeSocket source, NodeSocket target)
    {
        if (!FNodes.Contains(source.Owner))
            throw new ArgumentException("Source does not belong to this graph");
        
        if (!FNodes.Contains(target.Owner))
            throw new ArgumentException("Target does not belong to this graph");

        if (FConnections.Any(c => c.Source == source && c.Target == target))
            throw new ArgumentException("Connection already exists");

        source.Disposing += OnNodeSocketDisposing;
        target.Disposing += OnNodeSocketDisposing;

        var connection = new NodeSocketConnection(source, target);
        FConnections.Add(connection);
    }

    public void Disconnect(NodeSocketConnection connection)
    {
        if (!FConnections.Contains(connection))
            throw new ArgumentException("Connection does not belong to this graph");

        FConnections.Remove(connection);
        connection.Dispose();
    }

    public void Clear()
    {
        foreach (var connection in Connections.ToList())
            Disconnect(connection);
        foreach (var node in Nodes.ToList())
            RemoveNode(node);
    }

    public void Setup(IEnumerable<(NodeSocket source, NodeSocket target)> connections)
    {
        foreach (var (source, target) in connections)
        {
            if (!FNodes.Contains(source.Owner))
                AddNode(source.Owner);
            
            if (!FNodes.Contains(target.Owner))
                AddNode(target.Owner);
            
            Connect(source, target);
        }
    }
    
    private void OnNodeOutputValueChanging(object? sender, NodeSocketValueChangedArgs e)
    {
        if (Phase == NodeGraphPhase.Idle)
        {
            PropagatingSource = (NodeSocket?)sender;
            FRaisedExceptions.Clear();
            Phase = NodeGraphPhase.Propagation;
        }

        if (Phase == NodeGraphPhase.Execution)
            throw new InvalidOperationException("Node outputs cannot be updated in Execution phase");
    }
    
    private void OnNodeOutputValueChanged(object? sender, NodeSocketValueChangedArgs e)
    {
        if (Phase == NodeGraphPhase.Propagation && sender == PropagatingSource && !Batching)
            Execute();
    }
    
    private void OnNodeInputValueChanging(object? sender, NodeSocketValueChangedArgs e)
    {
        if (Phase == NodeGraphPhase.Propagation && sender is NodeSocket socket)
            UpdatedInputs.Add(socket);
    }
    
    private void OnNodeSocketDisposing(object? sender, EventArgs e)
    {
        var affectedConnections = FConnections.Where(c => c.Source == sender || c.Target == sender)
            .ToList();
        foreach (var connection in affectedConnections)
            Disconnect(connection);
    }

    private void Execute()
    {
        if (Phase != NodeGraphPhase.Propagation)
            throw new InvalidOperationException("Execution can only start after Propagation phase");

        if (!FRaisedExceptions.Any())
        {
            Phase = NodeGraphPhase.Execution;

            foreach (var node in UpdatedInputs.Select(s => s.Owner).Distinct().OfType<ITickableNode>())
            {
                try
                {
                    node.Tick();
                }
                catch (Exception e)
                {
                    MarkException(node as Node, e);
                }
            }
        }

        PropagatingSource = null;
        UpdatedInputs.Clear();
        Phase = FRaisedExceptions.Any() ? NodeGraphPhase.Error : NodeGraphPhase.Idle;
    }

    public void RunInTransaction(Action action)
    {
        Batching = true;
        try
        {
            action();
            if (Phase != NodeGraphPhase.Idle)
                Execute();
        }
        finally
        {
            Batching = false;
        }
    }

    public void MarkException(Node? node, Exception e)
    {
        FRaisedExceptions.Add((node, e));
        if (Phase is NodeGraphPhase.Idle)
            Phase = NodeGraphPhase.Error;
    }

    public void Reset()
    {
        Phase = NodeGraphPhase.Idle;
    }

    public int GetNodeIndex(Node node)
    {
        return FNodes.IndexOf(node);
    }
}

public enum NodeGraphPhase
{
    Idle,
    Propagation,
    Execution,
    Error
}

public class NodeGraphPhaseChangedEventArgs : EventArgs
{
    public required NodeGraphPhase Phase { get; init; }
    public IReadOnlyList<(Node?, Exception)>? Exceptions { get; init; }
}

public class NodeGraphNodeEventArgs : EventArgs
{
    public required Node Node { get; init; }
}