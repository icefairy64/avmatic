﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.NodeModel.Ffmpeg;

[Node("Stream mapping", "FFmpeg")]
public class FfmpegStreamMappingNode : Node
{
    public FfmpegInputStreamSocket Stream { get; }
    public SimpleNodeSocket<IReadOnlyList<IParameter>> Parameters { get; }

    public FfmpegStreamMappingNode()
    {
        Stream = new FfmpegInputStreamSocket { Owner = this, Name = "Stream" };
        Parameters = new SimpleNodeSocket<IReadOnlyList<IParameter>> { Owner = this, Name = "Parameters" };

        Stream.ValueChanged += (_, _) => Update();
        
        FInputs.Add(Stream);
        FOutputs.Add(Parameters);
    }

    public void Update()
    {
        if (Stream.Value is not FfmpegStreamInfo stream)
        {
            Parameters.Value = null;
            return;
        }

        var result = new List<IParameter>();
        var streamSpecifier = stream.StreamSpecifier;
        
        result.Add(new MapParameter
        {
            StreamSpecifier = streamSpecifier
        });
        
        result.AddRange(stream.Parameters.Select(p => new StreamIndexUnresolvedParameter
        {
            Parameter = p,
            StreamSpecifier = streamSpecifier
        }));
        
        Parameters.Value = result;
    }
}