﻿using System.ComponentModel;
using System.Linq.Expressions;
using System.Reactive.Linq;
using System.Reflection;
using DynamicData;
using ReactiveUI;
using Terminal.Gui;

namespace Avmatic.Terminal;

public static class ReactiveViewExtensions
{
    public static IDisposable BindToView<TC, TV>(
        this IObservable<TV> observable,
        TC view,
        Expression<Func<TC, TV>> expression) where TC : View
    {
        if (expression is not LambdaExpression { Body: MemberExpression memberExpression })
            throw new Exception("Not a member expression");
        
        if (memberExpression.Member is not PropertyInfo property)
            throw new Exception("Not a property");

        return observable
            .ObserveOn(RxApp.MainThreadScheduler)
            .Subscribe(value =>
            {
                property.SetValue(view, value);
                view.Redraw(view.Bounds);
            });
    }

    public static IDisposable BindToStackView<TC, TVm>(
        this IObservable<IChangeSet<TVm>> observable,
        View stackView,
        Func<TVm, TC, bool> viewMatcher,
        Func<TVm, TC> viewFactory) where TC : View where TVm : INotifyPropertyChanged
    {
        return observable
            .ObserveOn(RxApp.MainThreadScheduler)
            .OnItemAdded(v =>
            {
                var control = viewFactory(v);
                if (stackView.Subviews.LastOrDefault() is { } lastView)
                {
                    control.Y = Pos.Bottom(lastView) + 1;
                }
                stackView.Add(control);
                stackView.Redraw(stackView.Bounds);
            })
            .OnItemRemoved(v =>
            {
                var item = stackView.Subviews.First(i =>
                    viewMatcher(v, i as TC ?? throw new InvalidOperationException()));
                var index = stackView.Subviews.IndexOf(item);
                if (index != stackView.Subviews.Count - 1)
                {
                    if (index > 1)
                        stackView.Subviews[index + 1].Y = Pos.Bottom(stackView.Subviews[index - 1]) + 1;
                    else
                        stackView.Subviews[index + 1].Y = 0;
                }
                stackView.Remove(item);
                item.Dispose();
                stackView.Redraw(stackView.Bounds);
            }, false)
            .Subscribe();
    }
}