﻿using System.Reflection;
using Avmatic.Terminal;
using CommandLine;

Type[] GetVerbs()
{
    return Assembly.GetExecutingAssembly().GetTypes()
        .Where(t => t.GetCustomAttribute<VerbAttribute>() != null)
        .ToArray();
}

Parser.Default.ParseArguments(args, GetVerbs())
    .WithParsed(Startup.Run);