﻿using System.Reflection;

namespace Avmatic.NodeModel;

internal class ParameterNodeSocket : NodeSocket
{
    internal required PropertyInfo NodeProperty { get; init; }

    public override bool MatchType(Type type)
    {
        return NodeProperty.PropertyType.IsAssignableFrom(type);
    }
    
    protected override void OnValueChanged(NodeSocketValueChangedArgs args)
    {
        base.OnValueChanged(args);
        
        if (args.Value is null)
            NodeProperty.SetValue(Owner, null);
        else if (MatchType(args.Value.GetType()))
            NodeProperty.SetValue(Owner, args.Value);
        else
            NodeProperty.SetValue(Owner, null);
    }
}