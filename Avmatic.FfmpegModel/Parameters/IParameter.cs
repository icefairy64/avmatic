﻿namespace Avmatic.FfmpegModel.Parameters;

public interface IParameter
{
    public string Name { get; }
    public string Value { get; }
}