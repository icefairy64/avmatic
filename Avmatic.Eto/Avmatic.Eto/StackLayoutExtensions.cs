﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using DynamicData;
using Eto.Forms;
using ReactiveUI;

namespace Avmatic.Eto;

public static class StackLayoutExtensions
{
    public static IDisposable BindToChangeSet<T, TControl>(this StackLayout stack, IObservable<IChangeSet<T>> list, Func<T, TControl, bool> controlMatcher, Func<T, TControl> controlFactory) where TControl : Control where T : INotifyPropertyChanged
    {
        return list.ObserveOn(RxApp.MainThreadScheduler)
            .OnItemAdded(v =>
            {
                var control = controlFactory(v);
                stack.Items.Add(control);
            })
            .OnItemRemoved(v =>
            {
                var item = stack.Items.First(i =>
                    controlMatcher(v, i.Control as TControl ?? throw new InvalidOperationException()));
                stack.Items.Remove(item);
                item.Control.Dispose();
            })
            .Subscribe();
    }

    public static IDisposable BindToStackLayout<T, TControl>(
        this IObservable<IChangeSet<T>> list,
        StackLayout stack,
        Func<T, TControl, bool> controlMatcher,
        Func<T, TControl> controlFactory) where TControl : Control where T : INotifyPropertyChanged
    {
        return stack.BindToChangeSet(list, controlMatcher, controlFactory);
    }
}