﻿using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.Model;

public class ShortSideScaleDescription : IScaleDescription
{
    public required int SideLength { get; set; }

    public IParameter Parameter => new GenericParameter { Name = "vf", Value = $"scale=w='if(gt(iw\\,ih)\\,-2\\,{SideLength})':h='if(gt(iw\\,ih)\\,{SideLength}\\,-2):flags=lanczos" };
}