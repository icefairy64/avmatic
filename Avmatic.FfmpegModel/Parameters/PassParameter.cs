﻿namespace Avmatic.FfmpegModel.Parameters;

public class PassParameter : IParameter
{
    public string Name => "pass";
    
    public required Pass Pass { get; set; }

    public string Value => Pass == Pass.First ? "1" : "2";
}

public enum Pass
{
    First,
    Second
}