﻿using Avmatic.FfmpegModel;
using Avmatic.FfmpegModel.Parameters;

namespace Avmatic.NodeModel.Ffmpeg;

[Node("Input stream", "FFmpeg")]
public class FfmpegInputStreamNode : Node
{
    public ResultNodeSocket<InputFile> File { get; }
    public SimpleNodeSocket<FfmpegStreamInfo> Stream { get; }

    private StreamType? FStreamType;
    
    [NodeParameter]
    public StreamType? StreamType
    {
        get => FStreamType;
        set
        {
            FStreamType = value;
            UpdateStream();
        }
    }
    
    private int? FStreamIndex;
    
    [NodeParameter]
    public int? StreamIndex
    {
        get => FStreamIndex;
        set
        {
            FStreamIndex = value;
            UpdateStream();
        }
    }

    public FfmpegInputStreamNode()
    {
        File = new ResultNodeSocket<InputFile> { Owner = this, Name = "File" };
        Stream = new SimpleNodeSocket<FfmpegStreamInfo> { Owner = this, Name = "Stream" };
        
        File.ValueChanged += OnFileValueChanged;
        
        FInputs.Add(File);
        FOutputs.Add(Stream);
    }

    private void OnFileValueChanged(object? sender, NodeSocketValueChangedArgs e)
    {
        switch (e.Value)
        {
            case null:
                Stream.Value = null;
                break;
            case InputFile:
                UpdateStream();
                break;
        }
    }

    private void UpdateStream()
    {
        Stream.Value = File.Value switch
        {
            InputFile file => file.Streams
                .Where(s => StreamType == null || s.CodecType.ToStreamType() == StreamType)
                .Skip(StreamIndex ?? 0)
                .Select(s => new FfmpegStreamInfo(file.GetStreamSpecifier(s), new List<IParameter>()))
                .FirstOrDefault(),
            _ => null
        };
    }
}