﻿using Avmatic.Model;
using Avmatic.Terminal.Progress;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Terminal.Gui;

namespace Avmatic.Terminal;

public class MainWindow : Window
{
    public MainWindow(IHost host)
    {
        Title = "Avmatic";

        var progressTabModel = host.Services.GetService<ProgressTabModel>() ?? throw new Exception("No progress tab model found");
        
        var tabView = new TabView
        {
            Width = Dim.Fill(),
            Height = Dim.Fill()
        };
        tabView.AddTab(new TabView.Tab("Progress", new ProgressView(progressTabModel)), true);
        
        Add(tabView);
    }
}