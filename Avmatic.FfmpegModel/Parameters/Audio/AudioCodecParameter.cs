﻿namespace Avmatic.FfmpegModel.Parameters.Audio;

public class AudioCodecParameter : IParameter
{
    public string Name => "c";

    public required AudioCodec Codec;

    private string FormattedCodec => Codec switch
    {
        AudioCodec.Opus => "libopus",
        AudioCodec.Copy => "copy",
        _ => throw new ArgumentOutOfRangeException()
    };

    public string Value => FormattedCodec;
}

public enum AudioCodec
{
    Opus,
    Copy
}