﻿using System.Reflection;
using System.Text.Json;

namespace Avmatic.NodeModel.Serialization;

public record NodeDto(string Type, IDictionary<string, string> Parameters, ISet<string>? ExposedParameters)
{
    public Node Materialize()
    {
        var type = NodeImplementations.GetByName(Type);
        
        if (Activator.CreateInstance(type) is not Node node)
            throw new InvalidOperationException("Not a node type");
        
        foreach (var (key, value) in Parameters)
        {
            if (type.GetProperty(key) is not { } property)
                throw new InvalidOperationException($"Unknown property {key}");
            
            property.SetValue(node, JsonSerializer.Deserialize(value, property.PropertyType));
        }

        if (ExposedParameters is not null)
        {
            foreach (var parameter in ExposedParameters)
                node.ExposeParameterAsSocket(parameter);
        }

        return node;
    }

    public static NodeDto FromNode(Node node)
    {
        var type = node.GetType();
        var parameters = new Dictionary<string, string>();
        
        var props = type.GetProperties().SelectMany(p =>
        {
            if (p.GetCustomAttribute<NodeParameterAttribute>() is { } attribute)
                return new[] { (p, attribute) };
            return Array.Empty<(PropertyInfo, NodeParameterAttribute)>();
        });
        
        foreach (var (property, _) in props)
            parameters[property.Name] = JsonSerializer.Serialize(property.GetValue(node));

        return new NodeDto(type.Name, parameters, node.SocketizedParameters.ToHashSet());
    }
}