﻿namespace Avmatic.Jobs;

public abstract class Job
{
    public event EventHandler<JobProgressArgs>? Progress;

    protected readonly CancellationTokenSource CancellationTokenSource = new();

    protected CancellationToken CancellationToken => CancellationTokenSource.Token;

    protected virtual void EmitProgress(JobProgressArgs e)
    {
        Progress?.Invoke(this, e);
    }

    protected void ThrowIfCancelled()
    {
        CancellationToken.ThrowIfCancellationRequested();
    }

    public abstract Task Start();

    public virtual void Stop()
    {
        CancellationTokenSource.Cancel();
    }
}

public class JobProgressArgs : EventArgs
{
    public required Job Job { get; set; }
    public string? Stage { get; set; }
    public double? StageProgress { get; set; }
    public double? TotalProgress { get; set; }
}