﻿using System.Reactive.Disposables;
using Avmatic.Model;
using Eto.Forms;

namespace Avmatic.Eto;

public sealed class StreamMatrixConfigurationPanel : Panel
{
    public StreamMatrixPanelModel ViewModel { get; }

    private readonly CompositeDisposable CompositeDisposable = new();

    public StreamMatrixConfigurationPanel(StreamMatrixPanelModel viewModel)
    {
        ViewModel = viewModel;

        var fieldStack = new StackLayout
        {
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Spacing = 8
        };

        var stack = new StackLayout
        {
            Orientation = Orientation.Vertical,
            HorizontalContentAlignment = HorizontalAlignment.Stretch,
            Spacing = 8,
            Items =
            {
                FieldUtil.CreateSelectorField("Format", ViewModel, vm => vm.SelectedTarget, CompositeDisposable, viewModel.AvailableTargets),
                fieldStack
            }
        };
        
        Content = stack;

        viewModel.ConfigurationFields
            .Connect()
            .BindToStackLayout(fieldStack, (vm, control) => control.ViewModel == vm, vm => new ReactiveTextField(vm))
            .DisposeWith(CompositeDisposable);
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            CompositeDisposable.Dispose();
        }

        base.Dispose(disposing);
    }
}