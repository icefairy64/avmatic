﻿using System.Text.RegularExpressions;
using Avmatic.FfmpegModel.Parameters;
using Avmatic.FfmpegModel.Parameters.Audio;

namespace Avmatic.NodeModel.Ffmpeg.Encoders;

[Node("Opus encoder", "FFmpeg/Encoders")]
public partial class FfmpegOpusEncoderNode : FfmpegAbstractStreamNode
{
    private static readonly Regex BitrateRegex = BitrateStringRegex();
    
    private string FBitrate = "112k";

    [NodeParameter]
    public string Bitrate
    {
        get => FBitrate;
        set => SetAndUpdateIfChanged(ref FBitrate, value);
    }
    
    protected override FfmpegStreamInfo? GetOutputStream()
    {
        return InputStream.Value switch
        {
            FfmpegStreamInfo info => info with
            {
                Parameters = info.Parameters
                    .Concat(Parameters)
                    .ToList()
            },
            null => null,
            _ => throw new Exception("Unsupported value")
        };
    }

    private IEnumerable<IParameter> Parameters
    {
        get
        {
            yield return new AudioCodecParameter { Codec = AudioCodec.Opus };
            if (BitrateRegex.Match(Bitrate) is { Success: true, Groups: var groups })
            {
                var bitrate = int.Parse(groups[1].Value);
                var bitrateUnit = groups[2].Value.ToLowerInvariant().LastOrDefault('-') switch
                {
                    'k' => BitrateUnit.Kilobit,
                    'm' => BitrateUnit.Megabit,
                    'g' => BitrateUnit.Gigabit,
                    _ => BitrateUnit.Bit
                };
                yield return new BitrateParameter { Bitrate = bitrate, Unit = bitrateUnit };
            }
        }
    }

    [GeneratedRegex("(\\d+)([kmg])")]
    private static partial Regex BitrateStringRegex();
}