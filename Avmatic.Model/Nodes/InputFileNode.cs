﻿using Avmatic.NodeModel;

namespace Avmatic.Model.Nodes;

[Node("Input file")]
public class InputFileNode : Node
{
    public SimpleNodeSocket<FileInfo> File { get; }

    public InputFileNode()
    {
        File = new SimpleNodeSocket<FileInfo> { Owner = this, Name = "File" };
        FOutputs.Add(File);
    }
}