﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Numerics;
using System.Reactive.Disposables;
using System.Reflection;
using Eto.Forms;
using ReactiveUI;

namespace Avmatic.Eto;

public static class FieldUtil
{
    public static StackLayout CreateNumericField<T>(string label, DirectBinding<T> binding) where T : INumber<T>
    {
        var field = new NumericStepper();
        field.ValueBinding.Bind(binding.Convert(double.CreateTruncating, T.CreateTruncating));
        return CreateField(label, field);
    }
    
    public static StackLayout CreateNumericField<T, TVM>(string label, TVM viewModel, Expression<Func<TVM, T>> accessorExpression, CompositeDisposable disposable) where T : INumber<T> where TVM : ReactiveObject
    {
        var field = new NumericStepper();
        return CreateField(label, field, field.ValueBinding.Convert(T.CreateTruncating, double.CreateTruncating), viewModel, accessorExpression, disposable);
    }

    public static StackLayout CreateTextField(string label, DirectBinding<string> binding)
    {
        var field = new TextBox();
        field.TextBinding.Bind(binding);
        return CreateField(label, field);
    }
    
    public static StackLayout CreateTextField<TVM>(string label, TVM viewModel, Expression<Func<TVM, string>> accessorExpression, CompositeDisposable disposable) where TVM : ReactiveObject
    {
        var field = new TextBox();
        return CreateField(label, field, field.TextBinding, viewModel, accessorExpression, disposable);
    }
    
    public static StackLayout CreateTextField<TVM>(Expression<Func<TVM, string>> labelExpression, TVM viewModel, Expression<Func<TVM, string>> accessorExpression, CompositeDisposable disposable) where TVM : ReactiveObject
    {
        var field = new TextBox();
        return CreateField(labelExpression, field, field.TextBinding, viewModel, accessorExpression, disposable);
    }
    
    public static StackLayout CreateSelectorField(string label, DirectBinding<string> binding, params string[] options)
    {
        var field = new ComboBox();
        field.Items.AddRange(options.Select(x => new ListItem { Text = x, Key = x }));
        field.SelectedKeyBinding.Bind(binding);
        return CreateField(label, field);
    }
    
    public static StackLayout CreateSelectorField<TVM>(string label, TVM viewModel, Expression<Func<TVM, string>> accessorExpression, CompositeDisposable disposable, params string[] options) where TVM : ReactiveObject
    {
        var field = new ComboBox();
        field.Items.AddRange(options.Select(x => new ListItem { Text = x, Key = x }));
        return CreateField(label, field, field.SelectedKeyBinding, viewModel, accessorExpression, disposable);
    }
    
    public static StackLayout CreateCheckboxField(string label, DirectBinding<bool> binding)
    {
        var field = new CheckBox();
        field.CheckedBinding.Convert(from => from ?? false, to => to).Bind(binding);
        return CreateField(label, field);
    }

    public static StackLayout CreateFileSelectorField(string label, DirectBinding<FileInfo?> binding)
    {
        var field = new Button();
        field.TextBinding.Bind(binding.Convert(from => from?.Name ?? "No file", to => new FileInfo(to)), DualBindingMode.OneWay);
        field.Command = new Command((_, _) =>
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog(field) is DialogResult.Ok)
                binding.DataValue = new FileInfo(dialog.FileName);
        });
        return CreateField(label, field);
    }

    private static StackLayout CreateField(string label, Control field)
    {
        return new StackLayout
        {
            Orientation = Orientation.Horizontal,
            Items =
            {
                new Label { Text = label + ":", Width = 100 },
                new StackLayoutItem(field, true)
            }
        };
    }
    
    private static StackLayout CreateField<TVM, TO, TV>(string label, Control field, BindableBinding<TO, TV> binding, TVM viewModel, Expression<Func<TVM, TV>> accessorExpression, CompositeDisposable disposable) where TO : IBindable where TVM : ReactiveObject
    {
        if (accessorExpression is not LambdaExpression { Body: MemberExpression { Member: PropertyInfo property } })
            throw new Exception("Invalid expression");
        
        binding.BindToObservable(viewModel.WhenAnyValue(accessorExpression), v => property.SetValue(viewModel, v))
            .DisposeWith(disposable);
        
        return new StackLayout
        {
            Orientation = Orientation.Horizontal,
            Items =
            {
                new Label { Text = label + ":", Width = 100 },
                new StackLayoutItem(field, true)
            }
        };
    }
    
    private static StackLayout CreateField<TVM, TO, TV>(Expression<Func<TVM, string>> labelExpression, Control field, BindableBinding<TO, TV> binding, TVM viewModel, Expression<Func<TVM, TV>> accessorExpression, CompositeDisposable disposable) where TO : IBindable where TVM : ReactiveObject
    {
        if (accessorExpression is not LambdaExpression { Body: MemberExpression { Member: PropertyInfo property } })
            throw new Exception("Invalid expression");
        
        binding.BindToObservable(viewModel.WhenAnyValue(accessorExpression), v => property.SetValue(viewModel, v))
            .DisposeWith(disposable);

        var label = new Label { Text = labelExpression.Compile().Invoke(viewModel) + ":", Width = 100 };

        viewModel.WhenAnyValue(labelExpression)
            .BindToControl(label, l => l.Text)
            .DisposeWith(disposable);

        return new StackLayout
        {
            Orientation = Orientation.Horizontal,
            Items =
            {
                label,
                new StackLayoutItem(field, true)
            }
        };
    }
}