﻿using System.Reflection;

namespace Avmatic.NodeModel;

public abstract class Node
{
    protected List<NodeSocket> FInputs = new();
    protected List<NodeSocket> FOutputs = new();

    public IReadOnlyList<NodeSocket> Inputs => FInputs;
    public IReadOnlyList<NodeSocket> Outputs => FOutputs;

    private NodeGraph? FGraph;

    public NodeGraph? Graph
    {
        get => FGraph;
        set
        {
            if (FGraph == value)
                return;
            FGraph = value;
            OnGraphChanged(value);
        }
    }

    private HashSet<string> FSocketizedParameters = new();

    public IReadOnlySet<string> SocketizedParameters
    {
        get => FSocketizedParameters;
        set
        {
            var newParams = value.Except(FSocketizedParameters).ToList();
            var oldParams = FSocketizedParameters.Except(value).ToList();
            foreach (var newParam in newParams)
                ExposeParameterAsSocket(newParam);
            foreach (var oldParam in oldParams)
                RemoveExposedParameter(oldParam);
        }
    }

    public int? NodeIndex => Graph?.GetNodeIndex(this);

    public void ExposeParameterAsSocket(string parameterName)
    {
        if (FSocketizedParameters.Contains(parameterName))
            throw new ArgumentException($"{parameterName} is already exposed");
        if (GetType().GetProperty(parameterName) is not {} prop)
            throw new ArgumentException($"{parameterName} is not a parameter on this node");
        FInputs.Add(new ParameterNodeSocket { Name = parameterName, Owner = this, NodeProperty = prop });
        FSocketizedParameters.Add(parameterName);
    }

    public void RemoveExposedParameter(string parameterName)
    {
        if (!FSocketizedParameters.Contains(parameterName))
            throw new ArgumentException($"{parameterName} is not exposed");
        var removedInputs = FInputs.RemoveAll(s =>
            s is ParameterNodeSocket { NodeProperty.Name: var socketParam } && socketParam == parameterName);
        if (removedInputs == 0)
            throw new ArgumentException($"{parameterName} was not removed");
        FSocketizedParameters.Remove(parameterName);
    }
    
    protected void RaiseException(Exception e)
    {
        Graph?.MarkException(this, e);
    }

    protected virtual void OnGraphChanged(NodeGraph? graph)
    {
        
    }
}

public abstract class NodeSocket : IDisposable
{
    public required Node Owner { get; init; }
    public required string Name { get; init; }
    
    public abstract bool MatchType(Type type);

    private Type FType;
    public Type Type
    {
        get => FType;
        protected set
        {
            var prevType = FType;
            FType = value;
            if (prevType != value)
                OnTypeChanged(new NodeSocketTypeChangedArgs { Type = value });
        }
    }

    private object? FValue;
    public object? Value
    {
        get => FValue;
        set
        {
            var prevValue = FValue;
            
            if (prevValue == value)
                return;
            
            OnValueChanging(new NodeSocketValueChangedArgs { Value = value });
            FValue = value;
            OnValueChanged(new NodeSocketValueChangedArgs { Value = value });
        }
    }

    public event EventHandler<NodeSocketValueChangedArgs>? ValueChanging;
    public event EventHandler<NodeSocketValueChangedArgs>? ValueChanged;
    public event EventHandler<NodeSocketTypeChangedArgs>? TypeChanged;
    public event EventHandler? Disposing; 

    protected virtual void OnValueChanging(NodeSocketValueChangedArgs args)
    {
        ValueChanging?.Invoke(this, args);
    }
    
    protected virtual void OnValueChanged(NodeSocketValueChangedArgs args)
    {
        ValueChanged?.Invoke(this, args);
    }
    
    protected virtual void OnTypeChanged(NodeSocketTypeChangedArgs args)
    {
        TypeChanged?.Invoke(this, args);
    }

    public virtual void Dispose()
    {
        GC.SuppressFinalize(this);
        Disposing?.Invoke(this, EventArgs.Empty);
    }
}

public class NodeSocketConnection : IDisposable, IEquatable<NodeSocketConnection>
{
    public NodeSocket Source { get; }
    public NodeSocket Target { get; }

    public event EventHandler<NodeSocketConnectionValidityChangedArgs>? ValidityChanged; 

    private bool FIsValid;
    public bool IsValid
    {
        get => FIsValid;
        set
        {
            var prevValid = FIsValid;
            FIsValid = value;
            if (prevValid != value)
                ValidityChanged?.Invoke(this, new NodeSocketConnectionValidityChangedArgs { IsConnectionValid = value });
        }
    }

    public NodeSocketConnection(NodeSocket source, NodeSocket target)
    {
        Source = source;
        Target = target;
        UpdateValidity();
        Source.ValueChanging += OnSourceValueChanging;
        PropagateValue(Source.Value);
    }

    private void OnSourceValueChanging(object? sender, NodeSocketValueChangedArgs e)
    {
        PropagateValue(e.Value);
    }

    private void UpdateValidity()
    {
        IsValid = Target.MatchType(Source.Type);
    }

    private void PropagateValue(object? value)
    {
        try
        {
            if (IsValid)
                Target.Value = value;
        }
        catch (Exception e)
        {
            Target.Owner.Graph?.MarkException(Target.Owner, e);
        }
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this);
        Source.ValueChanging -= OnSourceValueChanging;
    }

    public bool Equals(NodeSocketConnection? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Source.Equals(other.Source) && Target.Equals(other.Target);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        return obj.GetType() == GetType() && Equals((NodeSocketConnection)obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Source, Target);
    }
}

public class NodeSocketValueChangedArgs : EventArgs
{
    public required object? Value { get; init; }
}

public class NodeSocketTypeChangedArgs : EventArgs
{
    public required Type Type { get; init; }
}

public class NodeSocketConnectionValidityChangedArgs : EventArgs
{
    public required bool IsConnectionValid { get; init; }
}