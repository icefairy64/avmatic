﻿namespace Avmatic.NodeModel.Ffmpeg.Chunks;

public abstract class AbstractChunkProcessorNode : SimpleNode, IDisposable
{
    public ResultNodeSocket<ChunkProcessingContext> InputContext { get; }
    public SimpleNodeSocket<ChunkProcessingContext> OutputContext { get; }

    private ChunkProcessingContext? OutContextValue;
    private TempDirectory? TempDirectory;

    protected AbstractChunkProcessorNode()
    {
        InputContext = AddInput("Input context", () => new ResultNodeSocketBuilder<ChunkProcessingContext>());
        OutputContext = AddOutput("Output context", () => new SimpleNodeSocketBuilder<ChunkProcessingContext>());
        InputContext.ValueChanging += OnInputContextValueChanging;
    }

    protected override void Update()
    {
        if (InputContext.Value is not ChunkProcessingContext inContext)
        {
            OutputContext.Value = null;
            return;
        }
        
        inContext.ChunkStarted += OnChunkStarted;
        
        if (TempDirectory is {} prevTempDirectory)
            prevTempDirectory.Dispose();
        TempDirectory = new TempDirectory();

        OutContextValue = new ChunkProcessingContext
        {
            Pipeline = inContext.Pipeline,
            SourceDirectory = TempDirectory.Directory
        };

        OutputContext.Value = OutContextValue;
    }

    private void OnChunkStarted(object? sender, ChunkStartedEventArgs e)
    {
        if (sender is not ChunkProcessingContext inContext)
            throw new Exception();

        ProcessChunk(inContext, e.CancellationToken).ContinueWith(task =>
        {
            if (task is { Exception: {} exception })
                RaiseException(exception);
        });
    }

    private async Task ProcessChunk(ChunkProcessingContext inContext, CancellationToken cancellationToken)
    {
        if (TempDirectory is not {} targetDirectory)
            throw new Exception("Target directory is not set");
        
        if (OutContextValue is not {} outContext)
            throw new Exception("Output context is not set");
        
        await Process(inContext.SourceDirectory, targetDirectory.Directory, cancellationToken);
        var result = await outContext.BeginChunk(cancellationToken);
        inContext.EndChunk(result);
        
        targetDirectory.Clear();
    }
    
    private void OnInputContextValueChanging(object? sender, NodeSocketValueChangedArgs e)
    {
        if (InputContext.Value is ChunkProcessingContext context)
        {
            // TODO: cancel running processing
            context.ChunkStarted -= OnChunkStarted;
        }
    }

    protected abstract Task Process(DirectoryInfo sourceDirectory, DirectoryInfo targetDirectory, CancellationToken cancellationToken);

    public void Dispose()
    {
        GC.SuppressFinalize(this);
        TempDirectory?.Dispose();
    }
}