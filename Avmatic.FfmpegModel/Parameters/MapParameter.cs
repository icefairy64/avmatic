﻿namespace Avmatic.FfmpegModel.Parameters;

public class MapParameter : IParameter
{
    public string Name => "map";
    
    public required IStreamSpecifier StreamSpecifier { get; set; }

    public string Value => StreamSpecifier.String;
}