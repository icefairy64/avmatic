﻿using Microsoft.Extensions.Logging;

namespace Avmatic.Jobs;

public interface ILogAwareJob<in T>
{
    public ILogger<T> Logger { set; }
}