﻿using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;

namespace Avmatic.Jobs;

public sealed class JobExecutor : IDisposable
{
    public required int JobCapacity { get; set; }

    public event EventHandler<JobStateChangeArgs>? JobStateChanged;
    public event EventHandler<JobFailureArgs>? JobFailed;
    public event EventHandler<JobProgressArgs>? JobProgress; 

    private readonly List<Job> ActiveJobs = new();
    private readonly ConcurrentQueue<Job> JobQueue = new();

    private readonly ILogger<JobExecutor> Logger;
    private readonly ILoggerFactory LoggerFactory;
    
    public JobExecutor(ILoggerFactory loggerFactory)
    {
        LoggerFactory = loggerFactory;
        Logger = loggerFactory.CreateLogger<JobExecutor>();
    }

    public void Enqueue<T>(T job) where T : Job
    {
        if (job is ILogAwareJob<T> logAwareJob)
            logAwareJob.Logger = LoggerFactory.CreateLogger<T>();
        
        JobQueue.Enqueue(job);
        OnJobStateChanged(new()
        {
            Job = job,
            State = JobState.Queued
        });
        
        StartNext();
    }

    public void Cancel(Job job, bool removeFromQueue = false)
    {
        if (JobQueue.Contains(job))
        {
            Logger.LogInformation("Cancelling pending job {Job}", job.ToString());
            OnJobStateChanged(new JobStateChangeArgs
            {
                Job = job,
                State = JobState.Canceled
            });
            if (removeFromQueue)
                throw new NotImplementedException();
        }
        else if (ActiveJobs.Contains(job))
        {
            Logger.LogInformation("Cancelling active job {Job}", job.ToString());
            job.Stop();
        }
        else
        {
            throw new ArgumentException("Provided job does not belong to this executor");
        }
    }

    public void CancelAll()
    {
        Logger.LogInformation("Cancelling all jobs");
        
        foreach (var job in JobQueue)
            Cancel(job);
        
        JobQueue.Clear();
        
        foreach (var job in ActiveJobs)
            Cancel(job);
    }

    private void StartNext()
    {
        if (ActiveJobs.Count >= JobCapacity)
            return;

        if (!JobQueue.TryDequeue(out var job))
            return;
        job.Progress += OnJobProgress;
        
        ActiveJobs.Add(job);
        OnJobStateChanged(new()
        {
            Job = job,
            State = JobState.Running
        });

        try
        {
            var syncContext = TaskScheduler.FromCurrentSynchronizationContext();
        
            job.Start().ContinueWith(task =>
            {
                job.Progress -= OnJobProgress;
                ActiveJobs.Remove(job);
            
                if (task.IsFaulted)
                    OnJobFailed(job, task.Exception);
                else if (task.IsCanceled)
                    OnJobCanceled(job);
                else
                    OnJobComplete(job);
            
                StartNext();
            }, syncContext);
        }
        catch (Exception e)
        {
            job.Start().ContinueWith(task =>
            {
                job.Progress -= OnJobProgress;
                ActiveJobs.Remove(job);
            
                if (task.IsFaulted)
                    OnJobFailed(job, task.Exception);
                else
                    OnJobComplete(job);
            
                StartNext();
            });
        }
    }

    private void OnJobComplete(Job job)
    {
        OnJobStateChanged(new()
        {
            Job = job,
            State = JobState.Complete
        });
    }
    
    private void OnJobCanceled(Job job)
    {
        OnJobStateChanged(new JobStateChangeArgs
        {
            Job = job,
            State = JobState.Canceled
        });
    }

    private void OnJobFailed(Job job, AggregateException? exception)
    {
        var state = JobState.Failed;

        if (exception?.InnerExceptions.OfType<OperationCanceledException>().FirstOrDefault() is { })
            state = JobState.Canceled;
        
        OnJobStateChanged(new JobStateChangeArgs
        {
            Job = job,
            State = state
        });

        if (state is JobState.Failed)
        {
            JobFailed?.Invoke(this, new()
            {
                Job = job,
                Exception = exception
            });
        }
    }

    private void OnJobStateChanged(JobStateChangeArgs args)
    {
        JobStateChanged?.Invoke(this, args);
    }

    private void OnJobProgress(object? sender, JobProgressArgs e)
    {
        JobProgress?.Invoke(this, e);
    }

    public void Dispose()
    {
        CancelAll();
    }
}

public enum JobState
{
    Queued,
    Running,
    Complete,
    Failed,
    Canceled
}

public class JobStateChangeArgs : EventArgs
{
    public required Job Job { get; set; }
    public required JobState State { get; set; }
}

public class JobFailureArgs : EventArgs
{
    public required Job Job { get; set; }
    public Exception? Exception { get; set; }
}