﻿namespace Avmatic.FfmpegRunner;

public class AccumulatorOutputHandler : IOutputHandler
{
    public List<string> Lines { get; } = new();

    public void HandleLine(string line)
    {
        Lines.Add(line);
    }
}