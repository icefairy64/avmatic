﻿namespace Avmatic.NodeModel;

[Node("Time math")]
public class TimeMathNode : SimpleNode
{
    public ResultNodeSocket<TimeSpan> InputA { get; }
    public ResultNodeSocket<TimeSpan> InputB { get; }
    
    public SimpleNodeSocket<TimeSpan> Result { get; }

    private TimeMathOperation FOperation = TimeMathOperation.Add;

    [NodeParameter]
    public TimeMathOperation Operation
    {
        get => FOperation;
        set => SetAndUpdateIfChanged(ref FOperation, value);
    }

    public TimeMathNode()
    {
        InputA = AddInput("Input A", () => new ResultNodeSocketBuilder<TimeSpan>());
        InputB = AddInput("Input B", () => new ResultNodeSocketBuilder<TimeSpan>());
        Result = AddOutput("Result", () => new SimpleNodeSocketBuilder<TimeSpan>());
    }

    protected override void Update()
    {
        Result.Value = Operation switch
        {
            TimeMathOperation.Add => InputA.Value is TimeSpan a && InputB.Value is TimeSpan b ? a + b : null,
            TimeMathOperation.Subtract => InputA.Value is TimeSpan a && InputB.Value is TimeSpan b ? a - b : null,
            TimeMathOperation.Max => Max(InputA.Value, InputB.Value),
            TimeMathOperation.Min => Min(InputA.Value, InputB.Value),
            _ => null
        };
    }

    private static TimeSpan? Min(object? a, object? b)
    {
        return a switch
        {
            TimeSpan ta when b is null => ta,
            null when b is TimeSpan tb => tb,
            TimeSpan ta when b is TimeSpan tb => ta < tb ? ta : tb,
            _ => null
        };
    }
    
    private static TimeSpan? Max(object? a, object? b)
    {
        return a switch
        {
            TimeSpan ta when b is null => ta,
            null when b is TimeSpan tb => tb,
            TimeSpan ta when b is TimeSpan tb => ta > tb ? ta : tb,
            _ => null
        };
    }
}

public enum TimeMathOperation
{
    Add,
    Subtract,
    Min,
    Max
}