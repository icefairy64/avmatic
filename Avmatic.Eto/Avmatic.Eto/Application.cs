﻿using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Avmatic.Eto;

public class Application : global::Eto.Forms.Application, IHostedService
{
    private readonly IHost Host;
    private readonly ILogger<Application> Logger;

    private readonly TaskCompletionSource RunTask = new TaskCompletionSource();

    internal new static Application Instance;

    public Application(IHost host, ILogger<Application> logger)
    {
        Host = host;
        Logger = logger;
        var configurator = host.Services.GetService<IApplicationConfirurator>();
        configurator?.ConfigureApplication(this);
    }

    public override void Run()
    {
        var form = new MainForm(Host.Services.GetRequiredService<ILoggerFactory>());
        Logger.LogInformation("Running application");
        base.Run(form);
    }

    protected override void OnTerminating(CancelEventArgs e)
    {
        base.OnTerminating(e);
        RunTask.SetResult();
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        Logger.LogInformation("Starting application");
        Instance = this;
        cancellationToken.Register(Quit);
        await RunTask.Task;
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        Logger.LogInformation("Stopping application");
        Quit();
        Logger.LogInformation("Post-quit");
        return Task.CompletedTask;
    }
}