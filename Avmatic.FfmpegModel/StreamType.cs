﻿namespace Avmatic.FfmpegModel;

public enum StreamType
{
    Video,
    VideoNonAttachment,
    Audio,
    Subtitle,
    Data,
    Attachment
}

public static class StreamTypeExtension
{
    public static string ToStreamSelectorChar(this StreamType type)
    {
        return type switch
        {
            StreamType.Video => "v",
            StreamType.VideoNonAttachment => "V",
            StreamType.Audio => "a",
            StreamType.Subtitle => "s",
            StreamType.Data => "d",
            StreamType.Attachment => "t",
            _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
        };
    }

    public static StreamType ToStreamType(this string str)
    {
        return str switch
        {
            "video" => StreamType.Video,
            "audio" => StreamType.Audio,
            "subtitle" => StreamType.Subtitle,
            "data" => StreamType.Data,
            "attachment" => StreamType.Attachment,
            _ => throw new ArgumentOutOfRangeException(nameof(str), str, null)
        };
    }
}