﻿using System.Text.RegularExpressions;

namespace Avmatic.NodeModel;

[Node("Find file neighbor", "Utils")]
public class FileNeighborNode : Node
{
    public ResultNodeSocket<FileInfo> File { get; }
    public SimpleNodeSocket<FileInfo> NeighborFile { get; }

    private string? FMatch;
    
    [NodeParameter]
    public string? Match
    {
        get => FMatch;
        set
        {
            var curMatch = FMatch;
            FMatch = value;
            if (curMatch != value)
                Update();
        }
    }
    
    private string? FReplace;
    
    [NodeParameter]
    public string? Replace
    {
        get => FReplace;
        set
        {
            var curReplace = FReplace;
            FReplace = value;
            if (curReplace != value)
                Update();
        }
    }

    public FileNeighborNode()
    {
        File = new ResultNodeSocket<FileInfo> { Owner = this, Name = "File" };
        NeighborFile = new SimpleNodeSocket<FileInfo> { Owner = this, Name = "Neighbor" };
        
        File.ValueChanged += OnFileValueChanged;
        
        FInputs.Add(File);
        FOutputs.Add(NeighborFile);
    }

    private void OnFileValueChanged(object? sender, NodeSocketValueChangedArgs e)
    {
        if (e.Value is FileInfo or null)
            Update();
    }

    private void Update()
    {
        if (Match is not { } matchPattern || Replace is not { } replacePattern || File.Value is not FileInfo file)
        {
            NeighborFile.Value = null;
            return;
        }

        var matchRegex = new Regex(matchPattern);
        var match = matchRegex.Match(file.Name);

        if (!match.Success)
        {
            NeighborFile.Value = null;
            return;
        }

        var newName = match.Groups.Keys
            .Aggregate(replacePattern, (current, key) => current.Replace($"${key}", match.Groups[key].Value));

        NeighborFile.Value = new FileInfo(Path.Combine(file.Directory?.FullName ?? "", newName));
    }
}