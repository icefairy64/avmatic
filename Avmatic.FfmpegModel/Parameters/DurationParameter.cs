﻿namespace Avmatic.FfmpegModel.Parameters;

public class DurationParameter : IParameter
{
    public required string Duration { get; init; }
    
    public string Name => "t";
    public string Value => Duration;
}