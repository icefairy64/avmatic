﻿using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reflection;
using Avmatic.FfmpegModel;
using ReactiveUI;

namespace Avmatic.Model;

public sealed class StreamTranscodePanelModel : ReactiveObject, IDisposable
{
    public required InputStream Stream { get; init; }

    private string FSelectedTarget = "none";
    public string SelectedTarget
    {
        get => FSelectedTarget;
        set => this.RaiseAndSetIfChanged(ref FSelectedTarget, value);
    }

    private IStreamConfiguration? FStreamConfiguration;
    public IStreamConfiguration? StreamConfiguration
    {
        get => FStreamConfiguration;
        set => this.RaiseAndSetIfChanged(ref FStreamConfiguration, value);
    }
    
    public string[] AvailableTargets => StreamConfigurationImplementations.Values
        .Select(t => t.Attribute)
        .OfType<StreamConfigurationAttribute>()
        .Where(a => a.StreamType is null || a.StreamType == Stream.CodecType.ToStreamType())
        .Select(a => a.Name)
        .Prepend("none")
        .ToArray();
    
    private readonly CompositeDisposable CompositeDisposable;
    
    private static readonly Dictionary<string, (Type Type, StreamConfigurationAttribute? Attribute)> StreamConfigurationImplementations;

    static StreamTranscodePanelModel()
    {
        StreamConfigurationImplementations = IStreamConfiguration.GetImplementations()
            .Select(t => (Type: t, Attribute: t.GetCustomAttribute(typeof(StreamConfigurationAttribute)) as StreamConfigurationAttribute))
            .Where(t => t.Attribute is not null)
            .ToDictionary(x => x.Attribute?.Name ?? throw new Exception("No attribute"));
    }
    
    public StreamTranscodePanelModel()
    {
        CompositeDisposable = new CompositeDisposable();

        this.WhenAnyValue(vm => vm.SelectedTarget)
            .Select(t => t switch
            {
                "none" => null,
                _ => CreateStreamConfiguration(t)
            })
            .Subscribe(x => StreamConfiguration = x)
            .DisposeWith(CompositeDisposable);
    }

    public void Dispose()
    {
        CompositeDisposable.Dispose();
    }

    private IStreamConfiguration CreateStreamConfiguration(string target)
    {
        var tuple = StreamConfigurationImplementations[target];
        var instance = Activator.CreateInstance(tuple.Type) as IStreamConfiguration ?? throw new Exception();
        instance.InputStream = Stream;
        instance.StreamIndex = Stream.Index;
        instance.CodecType = Stream.CodecType;
        return instance;
    }
}