﻿namespace Avmatic.FfmpegModel.Parameters.Video;

public class KeyframeIntervalParameter : IParameter
{
    public string Name => "g";
    
    public required int Interval { get; set; }
    
    public string Value => Interval.ToString();
}